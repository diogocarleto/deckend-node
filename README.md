# README #

### What is this repository for? ###

* A new backend as a service (baas). 

To access API documentation use localhost:8081/resources/index.htm or http://localhost:8081/swagger-ui.html

* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
* Configuration


1. Start the mongodb.
2. Change application.properties and set 'spring.data.mongodb' properties.

```
#!java

spring.data.mongodb.host=
spring.data.mongodb.port=
spring.data.mongodb.database=
spring.data.mongodb.username=
spring.data.mongodb.password=
```



3. execute gradle bootrun.

* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Diogo Carleto <diogocarleto@gmail.com>
* Other community or team contact