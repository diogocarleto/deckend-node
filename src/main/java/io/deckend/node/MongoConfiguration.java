package io.deckend.node;

import static java.util.Collections.singletonList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.mongo.MongoProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.domain.AuditorAware;
import org.springframework.data.mongodb.config.AbstractMongoConfiguration;
import org.springframework.data.mongodb.config.EnableMongoAuditing;
import org.springframework.data.mongodb.gridfs.GridFsTemplate;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;
import org.springframework.security.core.context.SecurityContextHolder;

import com.mongodb.Mongo;
import com.mongodb.MongoClient;
import com.mongodb.ServerAddress;
import com.mongodb.WriteConcern;

import io.deckend.node.infra.model.DBObjectUtils;
import io.deckend.node.model.User;

//TODO - deixar o spring se resolver quanto a conexão.
@Configuration
@ComponentScan
@EnableMongoRepositories
@EnableMongoAuditing
@EnableConfigurationProperties(MongoProperties.class)
/**
 * 
 * @author Diogo Carleto - diogocarleto@gmail.com
 *
 */
public class MongoConfiguration extends AbstractMongoConfiguration {
	
	@Autowired
	private MongoProperties properties;
	
	@Override
	protected String getDatabaseName() {
		return properties.getDatabase();
	}

	@Override
	public Mongo mongo() throws Exception {
//		singletonList(MongoCredential.createCredential(properties.getUsername(), properties.getDatabase(), properties.getPassword()))
		Mongo mongo = new MongoClient(singletonList(new ServerAddress(properties.getHost(), properties.getPort())));
		mongo.setWriteConcern(WriteConcern.SAFE);

		return mongo;
	}
	
	@Bean
	public GridFsTemplate gridFsTemplate() throws Exception {
		return new GridFsTemplate(mongoDbFactory(), mappingMongoConverter());
	}
	
	@Bean
	AuditorAware<String> auditorAware() {
		return () -> {
			String system = "system";
			if(SecurityContextHolder.getContext().getAuthentication() != null && 
					SecurityContextHolder.getContext().getAuthentication().getPrincipal() instanceof User) {
				User loggedUser = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
				if (loggedUser != null) {
					return loggedUser.getUsername();
				} else {
					return system;
				}
			}
			return system;
		};
	}
	
	@Bean
	public DBObjectUtils dbObjectUtils() {
		return new DBObjectUtils(auditorAware());
	}
}
