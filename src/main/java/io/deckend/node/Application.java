package io.deckend.node;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
//@EnableZuulProxy
/**
 * 
 * @author Diogo Carleto - diogocarleto@gmail.com
 *
 */
public class Application {
	
	public static void main(final String[] args) {
		SpringApplication.run(Application.class, args);
	}	
}
