package io.deckend.node.repository;

import io.deckend.node.model.User;

/**
 * @author Diogo Carleto - diogocarleto@gmail.com
 *
 */
public interface UserRepositoryCustom {

	void evictTokenAfter30Minutes();	
	User findByUserNameAndAppKey(String username, String appKey);
	void removeAppClientFromUser(String appKey);
}
