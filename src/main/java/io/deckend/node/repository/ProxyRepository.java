package io.deckend.node.repository;

import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.mongodb.repository.MongoRepository;

import io.deckend.node.config.HazelcastConfiguration;
import io.deckend.node.model.Proxy;

/**
 * 
 * @author Diogo Carleto - diogocarleto@gmail.com
 *
 */
public interface ProxyRepository extends MongoRepository<Proxy, String> {

	@Cacheable(HazelcastConfiguration.FILTERS_CACHE)
	Proxy findByName(String name);
	void deleteByName(String name);
}
