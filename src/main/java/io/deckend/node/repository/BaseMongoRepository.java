package io.deckend.node.repository;

import static io.deckend.node.exception.ErrorMessageEnum.COLLECTION_NOT_FOUND;
import static io.deckend.node.exception.ErrorMessageEnum.DOCUMENT_DELETED_SUCCESSFULLY;
import static io.deckend.node.exception.ErrorMessageEnum.DOCUMENT_NOT_FOUND;
import static io.deckend.node.exception.ErrorMessageEnum.*;
import static io.deckend.node.exception.ErrorMessageInfo.messageInfo;
import static io.deckend.node.infra.ACLPermissionsEnum.WRITE;
import static io.deckend.node.infra.model.DBObjectUtils.ACL;
import static io.deckend.node.infra.model.DBObjectUtils.ADD_DATE;
import static io.deckend.node.infra.model.DBObjectUtils.ADD_USER;
import static io.deckend.node.infra.model.DBObjectUtils.ID_FIELD;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.annotation.Resource;

import org.bson.types.ObjectId;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.stereotype.Repository;

import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.WriteResult;
import com.mongodb.util.JSON;

import io.deckend.node.exception.DeckendException;
import io.deckend.node.exception.ErrorMessageInfo;
import io.deckend.node.infra.ACLPermissionsEnum;
import io.deckend.node.infra.model.DBObjectUtils;

/**
 * @author Diogo Carleto - diogocarleto@gmail.com
 *
 */
@Repository
public class BaseMongoRepository {

	private static final Logger logger = LoggerFactory.getLogger(BaseMongoRepository.class);
	
	@Resource
	private MongoTemplate mongoTemplate;
	
	@Resource
	private DBObjectUtils dbObjectUtils;
	
	/**
	 * Perform insert/update for a Json.
	 * @param collection
	 * @param json
	 * @return
	 */
	public DBObject save(final String collection, final String json) {		
		logger.info("trying to save Document: {}", json);
		verifyCollectionExists(collection);
		
		DBObject objToSave = dbObjectUtils.parse(json);
		Optional<String> id = dbObjectUtils.getId(objToSave);
		
		if(!id.isPresent()) {
			insert(collection, objToSave);
		} else {
			update(collection, objToSave);
		}
		
		return objToSave;
	}
	
	/**
	 * Perfom insert/update for a {@link com.mongodb.DBObject}
	 * @param collection
	 * @param objToSave
	 * @return
	 */
	public DBObject save(final String collection, final DBObject objToSave) {		
		logger.info("trying to save Document: {}", objToSave);
		verifyCollectionExists(collection);
		
		Optional<String> id = dbObjectUtils.getId(objToSave);
		
		if(!id.isPresent()) {
			insert(collection, objToSave);
		} else {
			update(collection, objToSave);
		}
		
		return objToSave;
	}
	
	private void insert(final String collection, final DBObject objToSave) {
		dbObjectUtils.toAuditableDBObject(collection, objToSave);
		mongoTemplate.getCollection(collection).insert(objToSave);
		
		dbObjectUtils.adjustIdToResponse(objToSave, (ObjectId) objToSave.get(ID_FIELD));
	}
	
	private void update(final String collection, final DBObject objToSave) {
		Optional<Integer> version = dbObjectUtils.getVersion(objToSave); //get version right before toAuditableDBObject increments version
		ObjectId id = dbObjectUtils.buidObjectId(dbObjectUtils.getId(objToSave).get());
		
		dbObjectUtils.toAuditableDBObject(collection, objToSave);
		DBCollection dbCollection = mongoTemplate.getCollection(collection);
				
		DBObject savedObject = findById(collection, id.toString(), WRITE);
		if (savedObject==null) {
			throw new DeckendException(DOCUMENT_NOT_FOUND, objToSave.toMap());
		}
		
		if(!version.get().equals(dbObjectUtils.getVersion(savedObject).get())) {
			throw new DeckendException(OPTIMISTIC_LOCK_EXCEPTION, objToSave.toMap());
		}
		
		//maintains addUser,changeUser and acl unmodifiable
		objToSave.put(ADD_USER, savedObject.get(ADD_USER));
		objToSave.put(ADD_DATE, savedObject.get(ADD_DATE));
		objToSave.put(ACL, savedObject.get(ACL));
		
		WriteResult writeResult = dbCollection.update(
										dbObjectUtils.buildQuery(
											id.toString(), 
											version, 
											ACLPermissionsEnum.WRITE).getQueryObject(), 
										objToSave);
		if (writeResult.getN() == 0 ) {
			throw new DeckendException(OPTIMISTIC_LOCK_EXCEPTION, objToSave.toMap());
		}
		dbObjectUtils.adjustIdToResponse(objToSave, id);
	}
	
	/**
	 * Update only part of document, appending data or updating some existent field.
	 * @param collection
	 * @param id
	 * @param json
	 * @return
	 */
	public DBObject updatePartial(final String collection, 
							final String id,
							final String json) {
		logger.info("trying to update partial Document: {}", json);

		ObjectId objectId = dbObjectUtils.buidObjectId(id);
		DBObject partialObj = (DBObject) JSON.parse(json);
		
		DBCollection dbCollection = mongoTemplate.getCollection(collection);		
		DBObject objToSave = dbCollection.findOne(dbObjectUtils.buildQuery(id, 
																	Optional.empty(),
																	ACLPermissionsEnum.WRITE)
																.getQueryObject());
		
		if(objToSave == null) {
			return null;
		}
		
		objToSave.putAll(partialObj);
		dbObjectUtils.adjustIdToResponse(objToSave, objectId);
		
		objToSave = dbObjectUtils.toAuditableDBObject(collection, objToSave.toString());
		
		dbCollection.save(objToSave);
		dbObjectUtils.adjustIdToResponse(objToSave, objectId);
		
		return objToSave;
	}
	
	public ErrorMessageInfo delete(final String collection, final String id) {
		logger.info("trying to delete Document: {}", id);
		verifyCollectionExists(collection);
		
		DBObject removedObj = mongoTemplate.getCollection(collection)
				.findAndRemove(dbObjectUtils.buildQuery(id, Optional.empty(), ACLPermissionsEnum.WRITE).getQueryObject());
		
		if(removedObj == null) {
			throw new DeckendException(DOCUMENT_NOT_FOUND, id);
		}
		return messageInfo(DOCUMENT_DELETED_SUCCESSFULLY);
	}
	
	public List<DBObject> find(final String collection, final String query) {		
		logger.info("trying to find Documents: {}", query);
		List<DBObject> dbObjects = new ArrayList<>();
		
		DBObject queryObj = (DBObject) JSON.parse(query);		
		DBCursor cursor = mongoTemplate.getCollection(collection).find(dbObjectUtils.buildQueryToRead(queryObj));
		
		while(cursor.hasNext()) {
			DBObject dbObject = cursor.next();
			dbObjectUtils.adjustIdToResponse(dbObject, dbObjectUtils.buidObjectId(dbObject.get(ID_FIELD).toString()));
			dbObjects.add(dbObject);
		}
		
		return dbObjects;
	}
	
	public DBObject findById(final String collection, final String id, ACLPermissionsEnum... aclPermission) {	
		logger.info("trying to find Document by id: {}", id);
		return mongoTemplate
				.getCollection(collection)
				.findOne(
						dbObjectUtils.buildQuery(
												id,
												Optional.empty(),
												aclPermission
												
						).getQueryObject()
				);
	}
	
	
	public void verifyCollectionExists(final String collection) {
		if(!mongoTemplate.collectionExists(collection)) {
			throw new DeckendException(COLLECTION_NOT_FOUND, collection);
		}
	}
}
