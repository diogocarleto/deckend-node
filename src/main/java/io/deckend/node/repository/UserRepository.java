package io.deckend.node.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;

import io.deckend.node.model.User;

/**
 * 
 * @author Diogo Carleto - diogocarleto@gmail.com
 *
 */
public interface UserRepository extends MongoRepository<User, String>, UserRepositoryCustom {

	 public User findByFirstName(String firstName);
	 public User findByUsername(String username);
	 public List<User> findByLastName(String lastName);
}
