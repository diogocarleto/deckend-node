package io.deckend.node.repository;

import static org.springframework.data.mongodb.core.query.Criteria.where;
import static org.springframework.data.mongodb.core.query.Query.query;
import static org.springframework.data.mongodb.core.query.Update.update;

import java.time.LocalDateTime;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.stereotype.Repository;

import io.deckend.node.model.User;

/**
 * @author Diogo Carleto - diogocarleto@gmail.com
 *
 */
@Repository
public class UserRepositoryImpl implements UserRepositoryCustom {

	@Autowired 
	private MongoOperations operations;
	
	@Override
	public void evictTokenAfter30Minutes() {
		operations.updateMulti(query(where("token").ne(null)
										.and("changeDate")
										.gte(LocalDateTime.now().minusMinutes(30))), 
								update("token", null), 
								User.class);
	}

	@Override
	public User findByUserNameAndAppKey(final String username, final String appKey) {
		User user = operations.findOne(query(where("username").is(username)),User.class);
		
//		Map<AppClient, String> roleAppClients = user.getRoleAppClients().entrySet()	
//												.stream()
//												.filter(p -> p.getKey().equals(appKey))
//												.collect(Collectors.toMap(p -> p.getKey(), p -> p.getValue()));
//		user.setRoleAppClients(roleAppClients);
		return user;
	}

	@Override
	public void removeAppClientFromUser(final String appKey) {
		//operations.updateMulti(query(where("").), update, entityClass)
		
	}
}
