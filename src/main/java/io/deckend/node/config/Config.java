package io.deckend.node.config;

import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.context.request.RequestContextListener;

/**
 * @author Diogo Carleto - diogocarleto@gmail.com
 *
 */
@Configuration
public class Config {
	
	@Bean
	@ConditionalOnMissingBean
	public RequestContextListener requestContextListener() {
		return new RequestContextListener();
	}
}
