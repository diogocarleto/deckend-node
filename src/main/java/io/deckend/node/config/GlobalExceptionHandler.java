package io.deckend.node.config;

import static io.deckend.node.exception.ErrorMessageInfo.messageInfo;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.fasterxml.jackson.databind.ObjectMapper;

import io.deckend.node.exception.DeckendException;
import io.deckend.node.exception.ErrorMessageEnum;
import io.deckend.node.exception.ErrorMessageInfo;

/**
 * Customize response errors to client.
 * @author Diogo Carleto - diogocarleto@gmail.com
 *
 */
@ControllerAdvice
public class GlobalExceptionHandler {

	private final Logger logger = LoggerFactory.getLogger(GlobalExceptionHandler.class);
	
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	@ExceptionHandler(DeckendException.class)
	@ResponseBody
	public ErrorMessageInfo handleDeckendException(final HttpServletRequest req, final DeckendException ex) throws Exception {
		logger.info("handleDeckendException: {}", ex.getErrorEnum().code);
		return messageInfo(ex.getErrorEnum().code, ex.getFormattedErrorEnumDescription());
	}
	
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	@ExceptionHandler(DuplicateKeyException.class)
	@ResponseBody
	public ErrorMessageInfo handleDuplicateKeyException(final HttpServletRequest req, final DuplicateKeyException ex) throws Exception {
		ObjectMapper mapper = new ObjectMapper();
//		Map<String,Object> map = mapper.readValue(ex.getMessage(), Map.class);		
		logger.info("handleDuplicateKeyException: {}", ex.getMessage());
		return messageInfo(ErrorMessageEnum.DUPLICATED_KEY_ERROR.code, ex.getMessage());
	}
}
