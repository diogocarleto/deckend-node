package io.deckend.node.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.embedded.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import io.deckend.node.filter.ProxyFilter;

/**
 * @author Diogo Carleto - diogocarleto@gmail.com
 *
 */
@Configuration
public class FiltersConfiguration {

	@Autowired
	private ProxyFilter proxyFilter;

	@Bean
	public FilterRegistrationBean proxyFilterRegistration() {
		FilterRegistrationBean registration = new FilterRegistrationBean();
		registration.setFilter(proxyFilter);
		registration.addUrlPatterns("/proxies/*");
		registration.setName("proxyFilter");
		return registration;
	}
}
