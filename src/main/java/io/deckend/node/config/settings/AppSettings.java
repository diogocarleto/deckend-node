package io.deckend.node.config.settings;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * @author Diogo Carleto - diogocarleto@gmail.com
 *
 */
@Component
@ConfigurationProperties(prefix="app")
public class AppSettings {
	
	private String key;
	private String name;
	
	public String getKey() {
		return key;
	}
	public void setKey(final String key) {
		this.key = key;
	}
	public String getName() {
		return name;
	}
	public void setName(final String name) {
		this.name = name;
	}
}
