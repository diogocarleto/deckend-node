package io.deckend.node.config.settings;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * @author Diogo Carleto - diogocarleto@gmail.com
 *
 */
@Component
@ConfigurationProperties(prefix="token")
public class TokenSettings {
	
	private Integer timeout;
	private Integer maxTokens;
	
	public Integer getTimeout() {
		return timeout;
	}

	public void setTimeout(final Integer timeout) {
		this.timeout = timeout;
	}

	public Integer getMaxTokens() {
		return maxTokens;
	}

	public void setMaxTokens(final Integer maxTokens) {
		this.maxTokens = maxTokens;
	}
}
