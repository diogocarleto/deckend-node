package io.deckend.node.config.settings;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import io.deckend.node.infra.RoleEnum;

/**
 * @author Diogo Carleto - diogocarleto@gmail.com
 *
 */
@Component
@ConfigurationProperties(prefix="app.administrator")
public class UserAdministratorSettings {

	private String name;
	private String username;
	private String password;
	private String email;
	private final String role = RoleEnum.ROLE_APPADMIN.value;
	
	public String getUsername() {
		return username;
	}
	public void setUsername(final String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(final String password) {
		this.password = password;
	}
	public String getRole() {
		return role;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(final String email) {
		this.email = email;
	}
	public String getName() {
		return name;
	}
	public void setName(final String name) {
		this.name = name;
	}
}
