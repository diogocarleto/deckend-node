package io.deckend.node.config;

import static com.google.common.base.Predicates.or;
import static io.deckend.node.utils.AuthEnum.X_AUTH_APP_KEY;
import static io.deckend.node.utils.AuthEnum.X_AUTH_TOKEN;
import static springfox.documentation.builders.PathSelectors.regex;

import java.time.LocalDate;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.google.common.base.Predicate;

import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger.web.UiConfiguration;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
/**
 * 
 * @author Diogo Carleto - diogocarleto@gmail.com
 *
 */
public class SwaggerConfig {
    
    @Bean
    public Docket deckendApi() {
      return new Docket(DocumentationType.SWAGGER_2)
          .select()
            .apis(RequestHandlerSelectors.any())
            .paths(paths())
            .build()
          .pathMapping("/")
          .apiInfo(apiInfo())
          .directModelSubstitute(LocalDate.class,
                  String.class)
          .enableUrlTemplating(true);
    }
    
    @SuppressWarnings("unchecked")
	private Predicate<String> paths() {
        return or(
        	regex("/user.*"),
        	regex("/file.*"),
        	regex("/collection.*"),
        	regex("/document.*"));
    }
    
    private ApiInfo apiInfo() {
    	StringBuilder description = new StringBuilder()
    			.append("To signIn/signUp use the header: </br>")
    			.append(X_AUTH_APP_KEY.value).append(". </br></br>")
    			.append("To all other operations you should be authenticated and use the headers: </br>")
    			.append(X_AUTH_APP_KEY.value).append(", ")
    			.append(X_AUTH_TOKEN.value).append(".");
    	
        return new ApiInfoBuilder()
                .title("deckend-node API")
                .description(description.toString())
                .termsOfServiceUrl("http://deckend.io")
                .contact("nada")
                .license("Apache License Version 2.0")
                .licenseUrl("https://github.com/springfox/springfox/blob/master/LICENSE")
                .version("2.0")
                .build();
    }
    
    @Bean
    UiConfiguration uiConfig() {
      return new UiConfiguration(
          "validatorUrl");
    }
}
