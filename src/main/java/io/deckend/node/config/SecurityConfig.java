package io.deckend.node.config;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;

import io.deckend.node.security.authentication.SocialTokenAuthProvider;
import io.deckend.node.security.authentication.TokenAuthProvider;
import io.deckend.node.security.authentication.UsernamePasswordAuthProvider;
import io.deckend.node.security.filter.AppFilter;
import io.deckend.node.security.filter.AuthenticationFilter;

/**
 * @author Diogo Carleto - diogocarleto@gmail.com
 *
 */
@Configuration
@EnableGlobalMethodSecurity(jsr250Enabled=true, prePostEnabled=true)
public class SecurityConfig extends WebSecurityConfigurerAdapter {
	
	@Autowired
	private TokenAuthProvider tokenAuthenticationprovider;
	
	@Autowired
	private SocialTokenAuthProvider socialTokenAuthenticatorProvider;
		
	@Autowired
	private UsernamePasswordAuthProvider usernamePasswordAuthenticationProvider;
	
	@Autowired
	private AuthenticationFilter authenticationFilter;
	
	@Autowired
	private AppFilter appFilter;
	
	@Override
	protected void configure(final AuthenticationManagerBuilder auth) throws Exception {
		auth.authenticationProvider(tokenAuthenticationprovider)
			.authenticationProvider(socialTokenAuthenticatorProvider)
			.authenticationProvider(usernamePasswordAuthenticationProvider);
	}
	
	@Override
	protected void configure(final HttpSecurity http) throws Exception {
		http
			.csrf().disable()
			.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
				.and()
			.authorizeRequests()
				.regexMatchers("/user/sign(Up|In).*{1,}")
					.permitAll()
				.regexMatchers(publicResources())
//						"/v2/api-docs", "/swagger.*", "/webjars/.*", "/configuration/.*") //related to swagger
//				.antMatchers("/signUp/**", "/singIn/**") //the line above replace this
					.permitAll()
            	.anyRequest().authenticated()
            	.and()
            .exceptionHandling()
            	.authenticationEntryPoint(unauthorizedEntryPoint());
			
		http
			.addFilterBefore(appFilter, BasicAuthenticationFilter.class)
			.addFilterBefore(authenticationFilter, BasicAuthenticationFilter.class);
	}
	
	@Bean
	public PasswordEncoder passwordEncoder() {
		return new BCryptPasswordEncoder();
	}	
	
	@Bean
	public AuthenticationEntryPoint unauthorizedEntryPoint() {
		return (request, response, authException) -> response.sendError(HttpServletResponse.SC_UNAUTHORIZED);
	}	
	
	@Bean
	@Override
	public AuthenticationManager authenticationManagerBean() throws Exception {
		return super.authenticationManagerBean();
	}
	
	public static String[] publicResources() {
		return new String[]{"/resources/.*", "/v2/api-docs", "/swagger.*", "/webjars/.*", "/configuration/.*"};
	}
}
