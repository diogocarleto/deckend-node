package io.deckend.node.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.hazelcast.config.Config;
import com.hazelcast.config.EvictionPolicy;
import com.hazelcast.config.MaxSizeConfig;
import com.hazelcast.config.MaxSizeConfig.MaxSizePolicy;
import com.hazelcast.config.XmlConfigBuilder;
import com.hazelcast.core.Hazelcast;
import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.spring.cache.HazelcastCacheManager;

import io.deckend.node.config.settings.TokenSettings;

/**
 * @author Diogo Carleto - diogocarleto@gmail.com
 *
 */
@Configuration
@EnableCaching
public class HazelcastConfiguration {

	public static final String AUTHENTICATION_TOKENS_CACHE = "authenticationTokens";
	public static final String FILTERS_CACHE = "filtersCache";
	
	
	@Autowired
	private TokenSettings tokenSettings;
	
	@Bean
	HazelcastCacheManager hazelcastcacheManager() throws Exception {
		return new HazelcastCacheManager(hazelcastInstance());
	}

	@Bean
	HazelcastInstance hazelcastInstance() throws Exception {
		Config config = new Config();		
		config = new XmlConfigBuilder().build();
		config.getMapConfig(AUTHENTICATION_TOKENS_CACHE)
//			.setTimeToLiveSeconds(calculatedTimeouInSeconds())
			.setMaxIdleSeconds(calculatedTimeouInSeconds())
			.setEvictionPolicy(EvictionPolicy.LRU)
			.setMaxSizeConfig(new MaxSizeConfig(tokenSettings.getMaxTokens(), MaxSizePolicy.PER_NODE));
		 
		config.getMapConfig(FILTERS_CACHE)
			.setEvictionPolicy(EvictionPolicy.LRU)
			.setMaxSizeConfig(new MaxSizeConfig(tokenSettings.getMaxTokens(), MaxSizePolicy.PER_NODE));
		
		return Hazelcast.newHazelcastInstance(config);
	}

	private Integer calculatedTimeouInSeconds() {
		return tokenSettings.getTimeout() * 60;
	}
}
