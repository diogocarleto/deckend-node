package io.deckend.node.config;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.social.FacebookProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.social.config.annotation.EnableSocial;
import org.springframework.social.connect.Connection;
import org.springframework.social.connect.ConnectionFactoryLocator;
import org.springframework.social.connect.ConnectionRepository;
import org.springframework.social.connect.support.ConnectionFactoryRegistry;
import org.springframework.social.connect.web.ConnectController;
import org.springframework.social.facebook.api.Facebook;
import org.springframework.social.facebook.connect.FacebookConnectionFactory;


/**
 * @author Diogo Carleto - diogocarleto@gmail.com
 *
 */
@Configuration
@EnableSocial
@EnableConfigurationProperties(FacebookProperties.class)
public class SocialConfig {
	
	@Resource
	ConnectionRepository connectionRepository;
	
	@Autowired
	private FacebookProperties properties;
	
	@Bean
    public ConnectController connectController(final ConnectionFactoryLocator connectionFactoryLocator, final ConnectionRepository connectionRepository) {
		ConnectController controller = new ConnectController(connectionFactoryLocator, connectionRepository);
        controller.setViewPath("/");
//        controller.setApplicationUrl("http://localhost:8081/social/facebook");
        return controller;
    }
	
	@Bean
	@Scope(value="request", proxyMode=ScopedProxyMode.INTERFACES)
	public Facebook facebook() {
		Connection<Facebook> connection = connectionRepository.findPrimaryConnection(Facebook.class);
		return connection != null ? connection.getApi() : null;
	}

	@Bean
	public ConnectionFactoryLocator connectionFactoryLocator() {
		ConnectionFactoryRegistry registry = new ConnectionFactoryRegistry();
		registry.addConnectionFactory(new FacebookConnectionFactory(properties.getAppId(), properties.getAppSecret()));
		return registry;
	}
	
}
