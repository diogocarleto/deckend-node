package io.deckend.node.config;

import org.apache.camel.spring.boot.FatJarRouter;
import org.springframework.context.annotation.Configuration;

/**
 * @author Diogo Carleto - diogocarleto@gmail.com
 *
 */
@Configuration
public class MySpringBootRouter extends FatJarRouter {

	@Override
	public void configure() throws Exception {
//		from("")
		super.configure();
	}

	
}
