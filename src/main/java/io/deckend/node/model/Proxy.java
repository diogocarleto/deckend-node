package io.deckend.node.model;

import java.util.Objects;

import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * @author Diogo Carleto - diogocarleto@gmail.com
 *
 */
@Document
public class Proxy extends BaseDocument {

	private static final long serialVersionUID = -3945295237709255604L;
	
	/**
	 * if false, pass the {@link io.deckend.node.model.User} to third part service.
	 */
	private boolean useToken;
	@Indexed(unique=true)
	private String name;
	@Indexed(unique=true)
	private String url;
	private Integer port = 80;
	
	public Proxy() {
	}
	
	private Proxy(final boolean useToken, final String name, final String url, final Integer port) {
		super();
		this.useToken = useToken;
		this.name = name;
		this.url = url;
		if (port != null && port > 0) {
			this.port = port;
		}
	}
	
	public static Proxy proxy(final boolean useToken, final String name, final String url) {
		return new Proxy(useToken, name, url, null);
	}

	public static Proxy proxy(final boolean useToken, final String name, final String url, final Integer port) {
		return new Proxy(useToken, name, url, port);
	}

	public boolean isUseToken() {
		return useToken;
	}
	public void setUseToken(final boolean useToken) {
		this.useToken = useToken;
	}
	public String getName() {
		return name;
	}
	public void setName(final String name) {
		this.name = name;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(final String url) {
		this.url = url;
	}
	public Integer getPort() {
		return port;
	}
	public void setPort(final Integer port) {
		this.port = port;
	}

	@Override
	public int hashCode() {
		return Objects.hash(addDate, addUser, changeDate, changeUser, 
							id, name, url, useToken, port);
	}
	@Override
	public boolean equals(final Object obj) {
		if(!(obj instanceof Proxy)) {
			return false;
		}
		
		Proxy other = (Proxy) obj;
		return Objects.equals(addDate, other.getAddDate()) &&
				Objects.equals(addUser, other.getAddUser()) &&
				Objects.equals(changeDate, other.getChangeDate()) &&
				Objects.equals(changeUser, other.getChangeUser()) &&
				Objects.equals(id, other.getId()) &&
				Objects.equals(name, other.getName()) &&
				Objects.equals(url, other.getUrl()) &&
				Objects.equals(useToken, other.isUseToken()) &&
				Objects.equals(port, other.getPort());
	}

	@Override
	public String toString() {
		return "Proxy [useToken=" + useToken + ", name=" + name + ", url=" + url + ", port=" + port + ", toString()="
				+ super.toString() + "]";
	}
}
