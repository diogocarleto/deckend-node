package io.deckend.node.model;

import java.util.Objects;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.annotation.JsonIgnore;

import io.deckend.node.utils.IgnoreToMerge;

/**
 * @author Diogo Carleto - diogocarleto@gmail.com
 *
 */
@Document
public class User extends BaseDocument implements Cloneable {

	private static final long serialVersionUID = -3945295723770927604L;
	private static final Logger logger = LoggerFactory.getLogger(User.class);
		
	private String firstName;
	private String lastName;	
	
	@Indexed(unique=true)
	@IgnoreToMerge
	private String username;
	private String password;
	
	@Indexed(unique=true)	
	private String email;
	private String role;
	
	@JsonIgnore
	private String authData;
	@JsonIgnore
	private String data;
		
	public User() {
	}
	
	private User(final String firstName, final String lastName, final String username, final String password, final String role) {		
		this.firstName = firstName;
		this.lastName = lastName;
		this.username = username;
		this.password = password;
		this.role = role;
	}
	
	private User(final String firstName, final String lastName, final String username, final String password, final String role, final String email) {
		this(firstName, lastName, username, password, role);
		this.email = email;
	}
	
	public static User user(final String firstName, final String lastName, final String username, final String password, final String role) {
		return new User(firstName, lastName, username, password, role);
	}
	
	public static User user(final String firstName, final String lastName, final String username, final String password, final String role, final String email) {
		return new User(firstName, lastName, username, password, role, email);
	}
	
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(final String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(final String lastName) {
		this.lastName = lastName;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(final String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(final String password) {
		this.password = password;
	}
	public String getRole() {
		return role;
	}
	public void setRole(final String role) {
		this.role = role;
	}
	
	public String getEmail() {
		return email;
	}

	public void setEmail(final String email) {
		this.email = email;
	}
	
	public String getData() {
		return data;
	}

	public void setData(final String data) {
		this.data = data;
	}
	
	public String getAuthData() {
		return authData;
	}

	public void setAuthData(final String authData) {
		this.authData = authData;
	}
	
	public User email(final String email) {
		this.email = email;
		return this;
	}
	
	public User role(final String role) {
		this.role = role;
		return this;
	}
	
	@Override
	public int hashCode() {
		return Objects.hash(id, version, email, username);
	}

	@Override
	public boolean equals(final Object obj) {
		if(!(obj instanceof User)) 
			return false;
		
		User other = (User)obj;
		
		return Objects.equals(id, other.getId()) &&
				Objects.equals(version, other.getVersion()) &&
				Objects.equals(email, other.getEmail()) &&
				Objects.equals(username, other.getUsername());
	}

	@Override
	public String toString() {
		return "User [firstName=" + firstName + ", lastName=" + lastName + ", username=" + username + ", password="
				+ password + ", email=" + email + ", role=" + role + ", authData=" + authData
				+ ", data=" + data + ", id=" + id + ", addUser=" + addUser
				+ ", changeUser=" + changeUser + ", addDate=" + addDate + ", changeDate=" + changeDate + ", version="
				+ version + "]";
	}

	@Override
	protected User clone() throws CloneNotSupportedException {
		return (User) super.clone();
	}
}
