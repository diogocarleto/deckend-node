package io.deckend.node.model;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.UUID;

/**
 * @author Diogo Carleto - diogocarleto@gmail.com
 *
 */
public class Token implements Serializable {

	private static final long serialVersionUID = 8240078572534578312L;
	
	private String value;
	private User user;
	private LocalDateTime expiresDate;
	
	public Token() {
	}

	private Token(final User user, final LocalDateTime expiresDate) {
		super();
		value = generateNewToken();
		this.user = user;
		this.expiresDate = expiresDate;
	}
	
	public static Token token(final User user, final LocalDateTime expiresDate) {		
		Token token = new Token(user, expiresDate);
		return token;
	}

	public String getValue() {
		return value;
	}

	public void setValue(final String value) {
		this.value = value;
	}

	public User getUser() {
		return user;
	}

	public void setUser(final User user) {
		this.user = user;
	}

	public LocalDateTime getExpiresDate() {
		return expiresDate;
	}

	public void setExpiresDate(final LocalDateTime expiresDate) {
		this.expiresDate = expiresDate;
	}

	@Override
	public String toString() {
		return "Token [value=" + value + ", user=" + user + ", expiresDate="
				+ expiresDate + "]";
	}

	private static String generateNewToken() {
		return UUID.randomUUID().toString();
	}
}
