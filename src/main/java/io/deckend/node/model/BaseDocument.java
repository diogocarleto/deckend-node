package io.deckend.node.model;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.time.LocalDateTime;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.annotation.Version;
import org.springframework.util.StringUtils;

import com.fasterxml.jackson.annotation.JsonIgnore;

import io.deckend.node.utils.IgnoreToMerge;

/**
 * @author Diogo Carleto - diogocarleto@gmail.com
 *
 */
public abstract class BaseDocument implements Serializable {

	private static final long serialVersionUID = -192374510471856376L;
	
	private static final Logger logger = LoggerFactory.getLogger(BaseDocument.class);

	@Id
	protected String id;
	
	@CreatedBy
	protected String addUser;
	
	@LastModifiedBy
	protected String changeUser;
	
	@CreatedDate
	protected LocalDateTime addDate;
	
	@LastModifiedDate
	protected LocalDateTime changeDate;
	
	@Version
	protected Long version;
	
	public String getId() {
		return id;
	}

	public void setId(final String id) {
		this.id = id;
	}

	@JsonIgnore
	public boolean isNew() {
		return getId()!=null;
	}

	public String getAddUser() {
		return addUser;
	}

	public void setAddUser(final String addUser) {
		this.addUser = addUser;
	}

	public String getChangeUser() {
		return changeUser;
	}

	public void setChangeUser(final String changeUser) {
		this.changeUser = changeUser;
	}

	public LocalDateTime getAddDate() {
		return addDate;
	}

	public void setAddDate(final LocalDateTime addDate) {
		this.addDate = addDate;
	}

	public LocalDateTime getChangeDate() {
		return changeDate;
	}

	public void setChangeDate(final LocalDateTime changeDate) {
		this.changeDate = changeDate;
	}

	public Long getVersion() {
		return version;
	}

	public void setVersion(final Long version) {
		this.version = version;
	}
	
	@Override
	public String toString() {
		return "BaseEntity [id=" + id + ", addUser=" + addUser + ", changeUser=" + changeUser + ", addDate=" + addDate
				+ ", changeDate=" + changeDate + ", version=" + version + "]";
	}

	/**
	 * Adjust data escaping [PROTECTED] and other fields we don't want update.
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public <E extends BaseDocument> E mergeToUpdate(final E from) {
		try {			
			Field[] fields = this.getClass().getDeclaredFields();
			for(Field field: fields) {
				if(Modifier.isFinal(field.getModifiers()) || isProtected(field) 
						|| field.isAnnotationPresent(IgnoreToMerge.class)) {
					continue;
				}
				
				this.updateField(field, null, from);				
			}
			
			//maintains the Version from userFrom
			final String VERSION = "version";
			this.updateField(this.getClass().getSuperclass().getDeclaredField(VERSION), 
							from.getClass().getSuperclass().getDeclaredField(VERSION),
							from);			
			return (E) this;
		} catch(Exception ex) {
			logger.error(ex.getMessage(), ex);
			throw new RuntimeException(ex);
		}
	}
	
	private <E extends BaseDocument> void updateField(final Field fieldTo, final Field fieldFrom, final E from) throws Exception {
		logger.debug("updating field: "+fieldTo);				
		fieldTo.setAccessible(true);		
		
		if(fieldFrom!=null) {//used only for version
			fieldTo.set(this, fieldFrom.get(from));
		} else {
			Object valueFrom = from.getClass().getDeclaredMethod("get"+StringUtils.capitalize(fieldTo.getName())).invoke(from);
			if(valueFrom !=null) {			
				fieldTo.set(this, valueFrom);
			}
		}
		fieldTo.setAccessible(false);	
	}
	
	/**
	 * Verify if can update field value.
	 * @param field
	 * @return
	 */
	private boolean isProtected(final Field field) {
		String fieldName = field.getName();
		if (fieldName.equals("password") || fieldName.equals("id")) {
			return true;
		}
		
		return false;
	}
}
