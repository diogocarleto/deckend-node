package io.deckend.node.model;

import org.springframework.data.mongodb.core.mapping.Document;

/**
 * @author Diogo Carleto - diogocarleto@gmail.com
 *
 */
@Document
public class CollectionName extends BaseDocument {

	private static final long serialVersionUID = 1938696229821703323L;
	private String name;
	
	public CollectionName() {
	}
	
	private CollectionName(final String name) {
		super();
		this.name = name;
	}
	
	public static CollectionName collectionName(final String name) {
		return new CollectionName(name);
	}

	public String getName() {
		return name;
	}
}
