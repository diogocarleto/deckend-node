package io.deckend.node.exception;

/**
 * //TODO - ver uma maneira de ler as mensagens de um properties/json.
 * Enum that contains all mappedErrors/Messages, useful to rest clients.
 * @author Diogo Carleto - diogocarleto@gmail.com
 *
 */
public enum ErrorMessageEnum {

	//validation messages
	INVALID_LOGIN_ATTEMPT_USING_PROVIDER_ID(10000, "invalid login attempt using providerId: {0}"), //remover
	PROVIDER_ID_NOT_SUPPORTED(10001, "ProviderId not supported"), //remover
	INVALID_TOKEN(10002, "invalid {0}"),
	INVALID_APP_KEY(10003, "invalid appKey"),
	INVALID_USERNAME(10004, "invalid username"),
	INVALID_PASSWORD(10005, "invalid password"), 
	NOT_AUTHORIZED_SOCIAL_NETWORK(10006, "not authorized on {0}"),
	INVALID_EMAIL(10007,"invalid email {0}"),
	INSUFFICIENT_CREDENTIALS_ADD_USER_CUSTOM_ROLE(10008, "Insufficient credentials to add user with custom role: {0}"),
	INSUFFICIENT_CREDENTIALS_TO_SIGNIN_ANOTHER_APP(10009, "Insufficiet credentials to signIn in another app: {0}"),
	INVALID_USERNAME_PASSWORD(10010, "Invalid usernam/password"),
	INSUFFICIENT_CREDENTIALS_TO_REMOVE_USER(10011, "Insufficient credentials to remove user: {0}"),
	INVALID_APP_NAME(10012, "Invalid App name: {0}"),
	APP_NOT_FOUND(10013, "App not found: {0}"),
	CANNOT_UPDATE_DIFERENT_USER(10014, "Logged User is not the same the informed User to update"),
	COLLECTION_ALREADY_EXISTS(10015, "Collection already exists: {0}"),
	COLLECTION_NOT_FOUND(10016, "Collection not found: {0}"),
	DOCUMENT_NOT_FOUND(10017, "Document not found to id: {0}"),
	OPTIMISTIC_LOCK_EXCEPTION(10018, "Optimistic lock exception on saving entity: {0}"),
	FILE_NOT_FOUND(10019, "File not found"),
	VERSION_NOT_FOUND_IN_OBJECT	(10020, "VERSION not found in Object: {0}"),
	INVALID_INDEXES(10021, "Invalid indexes: {0}"),
	INVALID_PROXY(10022, "Invalid proxy: {0}"),
	INVALID_PROXY_ID(10023, "Invalid proxy id: {0}"),
	PROXY_NOT_FOUND(10024, "Proxy not found: {0}"),
	INVALID_ACL_PERMISSION(10025, "Invalid permission: {0}"),
	INVALID_COLLECTION(10026, "Invalid collection: {0}"),
	
	//error thrown by system.
	DUPLICATED_KEY_ERROR(11000, "duplicate key error: {0}"),
	INVALID_JSON(11001, "Invalid Json: {0}"),
	INVALID_ID(11002, "Invalid Id: {0}"),
	
	//info messages
	PASSWORD_CHANGED_SUCCESSFULLY(70000, "password changed."), 
	USER_DELETED_SUCCESSFULLY(70001, "user deleted."),
	APP_DELETED_SUCCESSFULLY(70002, "app deleted"), 
	COLLECTION_CREATED_SUCCESSFULLY(70003, "Collection created successfully"), 
	COLLECTION_DELETED_SUCCESSFULLY(70004, "Collection deleted successfully"), 
	DOCUMENT_DELETED_SUCCESSFULLY(70005, "Document deleted successfully"),  
	FILE_DELETED_SUCCESFULLY(70006, "File deleted successfully"),
	INDEXES_CREATED_SUCCESSFULLY(70007, "Indexes created successfully"),
	INDEXES_DROPPED_SUCCESSFULLY(70008, "Indexes dropped successfully"),
	PROXY_DELETED_SUCCESSFULLY(70009, "Proxy deleted successfully"), 
	PERMISSION_GRANTED_SUCCESSFULLY(70010, "Permission granted successfully"), 
	PERMISSION_REVOKED_SUCCESSFULLY(70011, "Permission revoked successfully"),
	;
	
	public final Integer code;
	public final String description;
	
	private ErrorMessageEnum(final Integer code, final String description) {
		this.code = code;
		this.description = description;
	}

	@Deprecated
	public Integer getCode() {
		return code;
	}

	@Deprecated
	public String getDescription() {
		return description;
	}
}
