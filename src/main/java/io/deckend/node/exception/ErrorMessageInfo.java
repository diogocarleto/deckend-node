package io.deckend.node.exception;

/**
 * @author Diogo Carleto - diogocarleto@gmail.com
 *
 */
public final class ErrorMessageInfo {

	public Integer code;
	public String message;
	
	public ErrorMessageInfo() {
	}
	
	private ErrorMessageInfo(final Integer code, final String message) {
		super();
		this.code = code;
		this.message = message;
	}
	
	public static ErrorMessageInfo messageInfo(final Integer code, final String message){
		return new ErrorMessageInfo(code, message);
	}
	
	public static ErrorMessageInfo messageInfo(final ErrorMessageEnum errorMessageEnum){
		return new ErrorMessageInfo(errorMessageEnum.getCode(), errorMessageEnum.getDescription());
	}
}
