package io.deckend.node.exception;

import java.text.MessageFormat;

/**
 * @author Diogo Carleto - diogocarleto@gmail.com
 *
 */
public class DeckendException extends RuntimeException {

	private static final long serialVersionUID = -2175508687343160554L;
	private final ErrorMessageEnum errorEnum;
	private final Object[] params;
	
	public DeckendException(final String message, final Throwable cause, final ErrorMessageEnum errorEnum) {
		super(message, cause);
		this.errorEnum = errorEnum;
		params = null;
	}

	public DeckendException(final String message, final ErrorMessageEnum errorEnum) {
		super(message);
		this.errorEnum = errorEnum;
		params = null;
	}

	public DeckendException(final Throwable cause, final ErrorMessageEnum errorEnum) {
		super(cause);
		this.errorEnum = errorEnum;
		params = null;
	}

	public DeckendException(final ErrorMessageEnum errorEnum) {
		super();
		this.errorEnum = errorEnum;
		params = null;
	}
	
	public DeckendException(final ErrorMessageEnum errorEnum, final Object... params) {
		super();
		this.errorEnum = errorEnum;
		this.params = params;
	}

	public ErrorMessageEnum getErrorEnum() {
		return errorEnum;
	}

	public Object[] getParams() {
		return params;
	}
	
	public String getFormattedErrorEnumDescription() {
		return MessageFormat.format(errorEnum.getDescription(), params);
	}
}
