package io.deckend.node.security.utils;

import static java.util.Arrays.asList;

import java.util.List;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import io.deckend.node.infra.RoleEnum;

/**
 * @author Diogo Carleto - diogocarleto@gmail.com
 *
 */
public final class GrantedAuthorityUtils {
	
	public static List<GrantedAuthority> authority(final String role) {		
		return asList(new SimpleGrantedAuthority(role));
    }
	
	public static List<GrantedAuthority> authorityAnonymous() {		
		return asList(new SimpleGrantedAuthority(RoleEnum.ROLE_ANONYMOUS.value));
    }
}
