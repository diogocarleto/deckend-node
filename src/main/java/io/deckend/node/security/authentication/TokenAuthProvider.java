package io.deckend.node.security.authentication;

import static io.deckend.node.security.utils.GrantedAuthorityUtils.authority;

import java.util.Optional;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.preauth.PreAuthenticatedAuthenticationToken;
import org.springframework.stereotype.Component;

import io.deckend.node.infra.controller.DeckendThreadLocal;
import io.deckend.node.model.Token;
import io.deckend.node.model.User;
import io.deckend.node.repository.UserRepository;
import io.deckend.node.service.TokenService;


/**
 * @author Diogo Carleto - diogocarleto@gmail.com
 *
 */
@Component
public class TokenAuthProvider implements AuthenticationProvider {

	@Resource
	private UserRepository userRepository;
	
	@Resource
	private TokenService tokenService;
	
	@Resource
	private PasswordEncoder passwordEncoder;
	
	@Resource
	private HttpServletRequest request;
		
	@Override
	@SuppressWarnings("unchecked")	
	public Authentication authenticate(final Authentication authentication) throws AuthenticationException {		
		Optional<String> tokenAuth = (Optional<String>) authentication.getPrincipal();
			
		Token token = tokenService.findByValue(tokenAuth.get());
		
		if(token==null) {
			throw new BadCredentialsException("invalid token"); 
		}
		
		User user = token.getUser();
		if(user!=null) {
			UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken = new UsernamePasswordAuthenticationToken(user, user.getPassword(), authority(user.getRole()));
			DeckendThreadLocal.setToken(token);
			return usernamePasswordAuthenticationToken;
		}
		
		throw new BadCredentialsException("invalid token");
	}	
	
	@Override
	public boolean supports(final Class<?> authentication) {
		return authentication.equals(PreAuthenticatedAuthenticationToken.class);
	}

}
