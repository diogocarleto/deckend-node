package io.deckend.node.security.authentication;

import static io.deckend.node.infra.RoleEnum.ROLE_USER;

import java.util.UUID;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.social.twitter.api.TwitterProfile;
import org.springframework.social.twitter.api.impl.TwitterTemplate;
import org.springframework.stereotype.Component;

import io.deckend.node.exception.DeckendException;
import io.deckend.node.exception.ErrorMessageEnum;
import io.deckend.node.model.User;
import io.deckend.node.utils.SocialProvidersEnum;

/**
 * @author Diogo Carleto - diogocarleto@gmail.com
 *
 */
@Component
public class TwitterTokenAuthProvider implements AuthenticationProvider {

	private static final Logger logger = LoggerFactory.getLogger(TwitterTokenAuthProvider.class);
	
	@Override
	public Authentication authenticate(final Authentication authentication) throws AuthenticationException {
		SocialPreAuthenticatedAuthToken preAuthentication = (SocialPreAuthenticatedAuthToken) authentication;
		String accessToken = (String) preAuthentication.getCredentials();
		
		TwitterTemplate ttTemplate = new TwitterTemplate(accessToken);
		if(!ttTemplate.isAuthorized()) {
			logger.warn("twitter user not Authorized");
			throw new DeckendException(ErrorMessageEnum.NOT_AUTHORIZED_SOCIAL_NETWORK, "providerId");
		}
		
		return new SocialPreAuthenticatedAuthToken(fromTwitterUser(ttTemplate, preAuthentication.getSocialProviderEnum()));
	}

	@Override
	public boolean supports(final Class<?> authentication) {
		return authentication.equals(SocialPreAuthenticatedAuthToken.class);
	}
	
	private User fromTwitterUser(final TwitterTemplate ttTemplate, final SocialProvidersEnum spe) {
		logger.info("fetching user from twitter");
		TwitterProfile ttProfile = ttTemplate.userOperations().getUserProfile();
		
		String username = buildUsername(spe.getPrefix(), String.valueOf(ttProfile.getId()));
		User user = User.user(ttProfile.getName(), 
						"", 
						username, 
						UUID.randomUUID().toString(), 
						ROLE_USER.value, 
						"");
		user.setAuthData("");//TODO implementar
		user.setData(JSONObject.wrap(ttProfile).toString());
		
		return user;
	}
	
	private String buildUsername(final String prefix, final String username) {
		return new StringBuilder(prefix)
				.append("_")
				.append(username).toString();
	}

}
