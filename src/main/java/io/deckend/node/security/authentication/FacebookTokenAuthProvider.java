package io.deckend.node.security.authentication;

import static io.deckend.node.infra.RoleEnum.ROLE_USER;

import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.social.InvalidAuthorizationException;
import org.springframework.social.facebook.api.impl.FacebookTemplate;
import org.springframework.stereotype.Component;

import io.deckend.node.exception.DeckendException;
import io.deckend.node.exception.ErrorMessageEnum;
import io.deckend.node.model.User;
import io.deckend.node.utils.SocialProvidersEnum;

/**
 * @author Diogo Carleto - diogocarleto@gmail.com
 *
 */
@Component
public class FacebookTokenAuthProvider implements AuthenticationProvider {

	private static final Logger logger = LoggerFactory.getLogger(FacebookTokenAuthProvider.class);

	@Override
	public Authentication authenticate(final Authentication authentication) throws AuthenticationException {
		SocialPreAuthenticatedAuthToken preAuthentication = (SocialPreAuthenticatedAuthToken) authentication;
		try {
			String accessToken = (String) preAuthentication.getCredentials();
			FacebookTemplate fbTemplate = new FacebookTemplate(accessToken);
			if(!fbTemplate.isAuthorized()) {
				logger.warn("facebook user not Authorized");
				throw new DeckendException(ErrorMessageEnum.NOT_AUTHORIZED_SOCIAL_NETWORK, "providerId");
			}
			
			return new SocialPreAuthenticatedAuthToken(fromFacebookUser(fbTemplate, preAuthentication.getSocialProviderEnum()));
		} catch (final InvalidAuthorizationException ex) {
			logger.warn(ex.getMessage());
			throw new DeckendException(ErrorMessageEnum.NOT_AUTHORIZED_SOCIAL_NETWORK, "providerId");
		}
	}

	@Override
	public boolean supports(final Class<?> authentication) {
		return authentication.equals(SocialPreAuthenticatedAuthToken.class);
	}

	private User fromFacebookUser(final FacebookTemplate fbTemplate, final SocialProvidersEnum spe) {
		logger.info("fetching user from facebook");
		final org.springframework.social.facebook.api.User fbUser = fbTemplate.userOperations().getUserProfile();
		
		final String username = buildUsername(spe.getPrefix(), fbUser.getEmail());
		final User user = User.user(fbUser.getFirstName(), 
						fbUser.getLastName(), 
						username, 
						UUID.randomUUID().toString(), 
						ROLE_USER.value, fbUser.getEmail());
		user.setAuthData("");//TODO implementar
//		user.setData(JSONObject.wrap(fbUser).toString());
		
		return user;
	}
	
	private String buildUsername(final String prefix, final String username) {
		return new StringBuilder(prefix)
				.append("_")
				.append(username).toString();
	}
}
