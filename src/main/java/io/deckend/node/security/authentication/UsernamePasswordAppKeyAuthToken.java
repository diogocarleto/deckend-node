package io.deckend.node.security.authentication;

import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;

/**
 * @author Diogo Carleto - diogocarleto@gmail.com
 *
 */
public class UsernamePasswordAppKeyAuthToken extends UsernamePasswordAuthenticationToken {

	private static final long serialVersionUID = -908657647304785755L;
	private String appKey;
	
	public UsernamePasswordAppKeyAuthToken(final Object principal, final Object credentials, final String appKey) {
		super(principal, credentials);
		this.appKey = appKey;
	}

	public String getAppKey() {
		return appKey;
	}

	public void setAppKey(final String appKey) {
		this.appKey = appKey;
	}
}
