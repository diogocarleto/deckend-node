package io.deckend.node.security.authentication;

import java.io.Serializable;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

import io.deckend.node.model.User;
import io.deckend.node.repository.UserRepository;
import io.deckend.node.service.TokenService;


/**
 * @author Diogo Carleto - diogocarleto@gmail.com
 *
 */
@Component
public class SocialTokenAuthProvider implements AuthenticationProvider, Serializable {

	private static final long serialVersionUID = -8201405228876909417L;

	private static final Logger logger = LoggerFactory.getLogger(SocialTokenAuthProvider.class);
	String UNDERLINE = "_";
	
	@Inject
	private FacebookTokenAuthProvider facebookTokenAuthenticator;
	
	@Inject
	private TwitterTokenAuthProvider twitterTokenAuthenticator;
	
	@Inject
	private UserRepository userRepository;
	
	@Inject
	private TokenService tokenService;

	@Override
	public Authentication authenticate(final Authentication authentication) throws AuthenticationException {
		SocialPreAuthenticatedAuthToken authToken = (SocialPreAuthenticatedAuthToken)authentication;
		String accessToken = (String) authToken.getCredentials();
		
		logger.info("trying to authenticate by {}", authToken.getSocialProviderEnum());
		Authentication resultOfAuthentication = null;
		
		switch (authToken.getSocialProviderEnum()) {
		case FACEBOOK:
			resultOfAuthentication = facebookTokenAuthenticator
					.authenticate(new SocialPreAuthenticatedAuthToken(authToken.getSocialProviderEnum(), accessToken));
			break;
		case TWITTER:
			resultOfAuthentication = twitterTokenAuthenticator
				.authenticate(new SocialPreAuthenticatedAuthToken(authToken.getSocialProviderEnum(), accessToken));
			break;
		}
		
		User user = (User) resultOfAuthentication.getPrincipal();
		User dbUser = userRepository.findByUsername(user.getUsername());		
		user = dbUser==null ? userRepository.save(user) : dbUser;
		
		tokenService.buildToken(user);
		SecurityContextHolder.getContext().setAuthentication(resultOfAuthentication);
		return resultOfAuthentication;
	}

	@Override
	public boolean supports(final Class<?> authentication) {
		return authentication.equals(SocialPreAuthenticatedAuthToken.class);
	}
}
