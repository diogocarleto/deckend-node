package io.deckend.node.security.authentication;

import static io.deckend.node.security.utils.GrantedAuthorityUtils.authority;

import javax.annotation.Resource;
import javax.inject.Inject;

import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import io.deckend.node.exception.DeckendException;
import io.deckend.node.exception.ErrorMessageEnum;
import io.deckend.node.model.User;
import io.deckend.node.repository.UserRepository;
import io.deckend.node.service.TokenService;

/**
 * @author Diogo Carleto - diogocarleto@gmail.com
 *
 */
@Component
public class UsernamePasswordAuthProvider implements AuthenticationProvider {

	@Resource
	private UserRepository userRepository;
	
	@Resource
	private PasswordEncoder passwordEncoder;
	
	@Inject
	private TokenService tokenService;
	
	@Override
	public Authentication authenticate(final Authentication authentication) throws AuthenticationException {
		User user = userRepository.findByUsername((String) authentication.getPrincipal());
		
		if(user==null || !passwordEncoder.matches((String)authentication.getCredentials(), user.getPassword())) {//passwordEncoder.matches used to verify authenticy of password
			throw new DeckendException(ErrorMessageEnum.INVALID_USERNAME_PASSWORD);
		}
		
		Authentication resultOfAuthentication = new UsernamePasswordAuthenticationToken(user, user.getPassword(), authority(user.getRole()));
		SecurityContextHolder.getContext().setAuthentication(resultOfAuthentication);
		tokenService.buildToken(user);

		return resultOfAuthentication;
	}

	@Override
	public boolean supports(final Class<?> authentication) {
		return authentication.equals(UsernamePasswordAuthenticationToken.class);
	}

}
