package io.deckend.node.security.authentication;

import static io.deckend.node.exception.ErrorMessageEnum.INVALID_TOKEN;
import static io.deckend.node.exception.ErrorMessageEnum.PROVIDER_ID_NOT_SUPPORTED;
import static io.deckend.node.utils.AuthEnum.X_AUTH_EXTERNAL_TOKEN;
import static io.deckend.node.utils.DeckendAssert.isNotEmpty;

import org.springframework.security.web.authentication.preauth.PreAuthenticatedAuthenticationToken;

import io.deckend.node.model.User;
import io.deckend.node.security.utils.GrantedAuthorityUtils;
import io.deckend.node.utils.SocialProvidersEnum;

/**
 * @author Diogo Carleto - diogocarleto@gmail.com
 *
 */
public class SocialPreAuthenticatedAuthToken extends PreAuthenticatedAuthenticationToken {

	private static final long serialVersionUID = 5972928531788393673L;
	private SocialProvidersEnum socialProviderEnum;

	public SocialPreAuthenticatedAuthToken(final SocialProvidersEnum socialProviderEnum, final String socialToken) {
		super(null, socialToken);
		
		isNotEmpty(socialProviderEnum, PROVIDER_ID_NOT_SUPPORTED);
		isNotEmpty(socialToken, INVALID_TOKEN, X_AUTH_EXTERNAL_TOKEN.value);
		
		this.socialProviderEnum = socialProviderEnum;
	}
	
	public SocialPreAuthenticatedAuthToken(final User user) {
		super(user, null, GrantedAuthorityUtils.authority(user.getRole()));
	}
	
	public SocialProvidersEnum getSocialProviderEnum() {
		return socialProviderEnum;
	}

	public void setSocialProviderEnum(final SocialProvidersEnum socialProviderEnum) {
		this.socialProviderEnum = socialProviderEnum;
	}	
}
