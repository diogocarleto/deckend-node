package io.deckend.node.security.filter;

import static io.deckend.node.exception.ErrorMessageEnum.INVALID_PASSWORD;
import static io.deckend.node.exception.ErrorMessageEnum.INVALID_USERNAME;
import static io.deckend.node.infra.controller.HttpUtils.fromHeader;
import static io.deckend.node.infra.controller.HttpUtils.handleAuthenticatioTokenResponse;
import static io.deckend.node.utils.AuthEnum.X_AUTH_EXTERNAL_TOKEN;
import static io.deckend.node.utils.AuthEnum.X_AUTH_PASSWORD;
import static io.deckend.node.utils.AuthEnum.X_AUTH_TOKEN;
import static io.deckend.node.utils.AuthEnum.X_AUTH_USERNAME;
import static io.deckend.node.utils.DeckendAssert.isNotEmpty;

import java.io.IOException;
import java.util.Optional;

import javax.annotation.Resource;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.InternalAuthenticationServiceException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.preauth.PreAuthenticatedAuthenticationToken;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import io.deckend.node.exception.DeckendException;
import io.deckend.node.exception.ErrorMessageEnum;
import io.deckend.node.security.authentication.SocialPreAuthenticatedAuthToken;
import io.deckend.node.utils.SocialProvidersEnum;

/**
 * Filter Used to authenticateByToken.
 * @author Diogo Carleto - diogocarleto@gmail.com
 *
 */
@Component
public class AuthenticationFilter extends OncePerRequestFilter {

	private static final Logger logger = LoggerFactory.getLogger(AuthenticationFilter.class);
	
	@Resource
	private AuthenticationManager authenticationManager;
	
	private Optional<String> token;
	
	@Override
	protected void doFilterInternal(final HttpServletRequest httpRequest, final HttpServletResponse httpResponse, final FilterChain chain) throws ServletException, IOException {
		
		logger.debug("getting AuthEnum values from request");
        token = Optional.ofNullable(httpRequest.getHeader(X_AUTH_TOKEN.value));
		
        try {
        	if (isAttemptToSignIn(httpRequest)) {
        		processUsernamePasswordAuthentication(httpRequest, httpResponse);
        	} else if(isAttemptToSignInSocial(httpRequest)) {
        		processSocialAuthentication(httpRequest, httpResponse);
        	} else if(token.isPresent() && !token.get().isEmpty()) {
	        	processTokenAuthentication(httpResponse, token);	        	
	        }
	        
	        chain.doFilter(httpRequest, httpResponse);
        } catch (InternalAuthenticationServiceException internalAuthenticationServiceException) {
            SecurityContextHolder.clearContext();
            logger.error("Internal authentication service exception", internalAuthenticationServiceException);
            httpResponse.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
        } catch (AuthenticationException ex) {
        	SecurityContextHolder.clearContext();
        	httpResponse.sendError(HttpServletResponse.SC_UNAUTHORIZED, ex.getMessage());
        }
	}
	
	private boolean isAttemptToSignIn(final HttpServletRequest httpRequest) {
		return httpRequest.getMethod().equals(HttpMethod.POST.name()) && httpRequest.getRequestURI().toString().endsWith("/user/signIn");
	}
	
	private boolean isAttemptToSignInSocial(final HttpServletRequest httpRequest) {
		return httpRequest.getMethod().equals(HttpMethod.POST.name()) && httpRequest.getRequestURI().toString().matches("/user/signIn/.*");
	}	
	
	private void processUsernamePasswordAuthentication(final HttpServletRequest httpRequest, final HttpServletResponse httpResponse) throws IOException {
		String username = fromHeader(httpRequest, X_AUTH_USERNAME);
		String password = fromHeader(httpRequest, X_AUTH_PASSWORD);
		
		logger.debug("Trying to singIn using {}", username);
		
		isNotEmpty(username, INVALID_USERNAME, X_AUTH_USERNAME.value);
		isNotEmpty(password, INVALID_PASSWORD, X_AUTH_PASSWORD.value);
		
		tryToAuthenticate(new UsernamePasswordAuthenticationToken(username, password));
		handleAuthenticationResponse(httpResponse);
	}
	
	private void processSocialAuthentication(final HttpServletRequest httpRequest, final HttpServletResponse httpResponse) {
		String providerId = httpRequest.getRequestURI().toString().split("/")[3];
		String accessToken = fromHeader(httpRequest, X_AUTH_EXTERNAL_TOKEN);
		
		logger.info("signIn to providerId: {}", providerId);
		if(!SocialProvidersEnum.isSupported(providerId)) {
			throw new DeckendException(ErrorMessageEnum.PROVIDER_ID_NOT_SUPPORTED, providerId);
		}
		
		tryToAuthenticate(new SocialPreAuthenticatedAuthToken(SocialProvidersEnum.valueOf(providerId.toUpperCase()), accessToken));
		handleAuthenticatioTokenResponse(httpResponse);
	}
	
	private void processTokenAuthentication(final HttpServletResponse httpResponse, final Optional<String> token) throws IOException {
		logger.debug("Trying to authenticate by token using {}", token.get());
		
		PreAuthenticatedAuthenticationToken authenticationToken = new PreAuthenticatedAuthenticationToken(token, null);
		tryToAuthenticate(authenticationToken);
		handleAuthenticationResponse(httpResponse);
	}
	
	private void tryToAuthenticate(final Authentication authentication) {
		Authentication resultOfAuthentication = authenticationManager.authenticate(authentication);
		SecurityContextHolder.getContext().setAuthentication(resultOfAuthentication);
	}
	
	private void handleAuthenticationResponse(final HttpServletResponse httpResponse) throws IOException {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		
		if (!authentication.isAuthenticated()) {
			throw new InternalAuthenticationServiceException("Unable to authenticate for provided credentials");
		}
		
		logger.debug("Sending response to client", SecurityContextHolder.getContext().getAuthentication().getPrincipal());
		handleAuthenticatioTokenResponse(httpResponse);
	}
}
