package io.deckend.node.security.filter;

import static io.deckend.node.utils.AuthEnum.X_AUTH_APP_KEY;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import io.deckend.node.config.SecurityConfig;
import io.deckend.node.config.settings.AppSettings;

/**
 * 
 * @author Diogo Carleto - diogocarleto@gmail.com
 *
 */
@Component
public class AppFilter extends OncePerRequestFilter {
	
	@Autowired
	private AppSettings appSettings;
	
	@Override
	protected void doFilterInternal(final HttpServletRequest request, final HttpServletResponse response, final FilterChain chain) throws ServletException, IOException {
		String appKey = request.getHeader(X_AUTH_APP_KEY.value);		
		
		if(!isAttemptToSeePublicContent(request) && (appKey==null || !appSettings.getKey().equals(appKey))) {
			response.sendError(HttpServletResponse.SC_UNAUTHORIZED, "invalid "+X_AUTH_APP_KEY.value);
			return;
		}
		
		chain.doFilter(request, response);
	}
	
	private boolean isAttemptToSeePublicContent(final HttpServletRequest httpRequest) {
		for(String publicResource: SecurityConfig.publicResources()) {
			if (httpRequest.getRequestURI().toString().matches(publicResource)) {
				return true;
			}
		}
		return false;
	}
}
