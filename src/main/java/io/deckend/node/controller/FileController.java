package io.deckend.node.controller;

import static io.deckend.node.infra.model.DBObjectUtils.ID_FIELD;
import static org.springframework.data.mongodb.core.query.Criteria.where;
import static org.springframework.data.mongodb.core.query.Query.query;
import static org.springframework.web.bind.annotation.RequestMethod.DELETE;
import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;
import static org.springframework.web.bind.annotation.RequestMethod.PUT;

import java.io.IOException;
import java.util.Optional;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;

import org.bson.types.ObjectId;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.mongodb.gridfs.GridFsOperations;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.mongodb.DBObject;
import com.mongodb.gridfs.GridFSDBFile;
import com.mongodb.gridfs.GridFSFile;

import io.deckend.node.exception.DeckendException;
import io.deckend.node.exception.ErrorMessageEnum;
import io.deckend.node.exception.ErrorMessageInfo;
import io.deckend.node.infra.ACLPermissionsEnum;
import io.deckend.node.infra.model.DBObjectUtils;

/**
 * @author Diogo Carleto - diogocarleto@gmail.com
 *
 */
@RestController
@RequestMapping("/file")
public class FileController {
		
	private static final Logger logger = LoggerFactory.getLogger(FileController.class);
	
	@Resource
	private GridFsOperations gridOperations;
	
	@Resource
	private DBObjectUtils dbObjectUtils;
	
	@RequestMapping(value="", method=POST)
	public DBObject upload(@RequestParam final String collectionName, 
							@RequestParam final MultipartFile file, 
							@RequestParam(required=false, defaultValue="{}") final String metadata) {		
		try {
			logger.info("trying to save a file: {}", file.getOriginalFilename());
			
			DBObject metadataObj = dbObjectUtils.toAuditableDBObject(collectionName, metadata);	
			
			GridFSFile gridFSFile = gridOperations.store(file.getInputStream(), file.getOriginalFilename(), file.getContentType(), metadataObj);			
			dbObjectUtils.adjustIdToResponse(gridFSFile, (ObjectId) gridFSFile.get(ID_FIELD));
			
			return dbObjectUtils.toDBObject(gridFSFile);
		} catch (IOException e) {
			logger.error(e.getMessage(), e);
			throw new RuntimeException(e);
		}
	}
	
	@RequestMapping(value="/{id}", method=GET)
	public void download(@PathVariable final String id, final HttpServletResponse httpResponse) {
		try {
			logger.info("trying to find by id: {}", id);
			GridFSDBFile gridFSDBFile = gridOperations.findOne(query(where(ID_FIELD).is(dbObjectUtils.buidObjectId(id)))); 
			
			if(gridFSDBFile == null) {
				throw new DeckendException(ErrorMessageEnum.FILE_NOT_FOUND);
			}
			
			gridFSDBFile.writeTo(httpResponse.getOutputStream());
			httpResponse.setContentType(gridFSDBFile.getContentType());
			httpResponse.flushBuffer();
		} catch (IOException e) {
			throw new DeckendException(ErrorMessageEnum.FILE_NOT_FOUND);
		}
	}
	
	@RequestMapping(value="/", method=PUT)
	public DBObject update() {
		return null;
	}
	
	@RequestMapping(value="/{id}", method=DELETE)
	public ErrorMessageInfo delete(@PathVariable final String id) {
		logger.info("trying to delete by id: {}", id);
		
		gridOperations.delete(dbObjectUtils.buildQuery(id, Optional.empty(), ACLPermissionsEnum.WRITE));
		return ErrorMessageInfo.messageInfo(ErrorMessageEnum.FILE_DELETED_SUCCESFULLY);
	}
	
}
