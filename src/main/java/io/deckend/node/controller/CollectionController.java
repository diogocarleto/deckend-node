package io.deckend.node.controller;

import static io.deckend.node.exception.ErrorMessageEnum.COLLECTION_ALREADY_EXISTS;
import static io.deckend.node.exception.ErrorMessageEnum.COLLECTION_CREATED_SUCCESSFULLY;
import static io.deckend.node.exception.ErrorMessageEnum.COLLECTION_DELETED_SUCCESSFULLY;
import static io.deckend.node.exception.ErrorMessageEnum.INDEXES_CREATED_SUCCESSFULLY;
import static io.deckend.node.exception.ErrorMessageEnum.INDEXES_DROPPED_SUCCESSFULLY;
import static io.deckend.node.exception.ErrorMessageInfo.messageInfo;
import static org.springframework.web.bind.annotation.RequestMethod.DELETE;
import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

import java.util.List;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mongodb.DBObject;

import io.deckend.node.exception.DeckendException;
import io.deckend.node.exception.ErrorMessageEnum;
import io.deckend.node.exception.ErrorMessageInfo;
import io.deckend.node.infra.model.DBObjectUtils;
import io.deckend.node.repository.BaseMongoRepository;

/**
 * 
 * @author Diogo Carleto - diogocarleto@gmail.com
 *
 */
@RestController
@RequestMapping("/collection")
public class CollectionController {
	
	private static final Logger logger = LoggerFactory.getLogger(CollectionController.class);
	
	@Resource
	private MongoTemplate mongoTemplate;
	
	@Resource
	private BaseMongoRepository baseMongoRepository;
	
	@Resource
	private DBObjectUtils dbObjectUtils;
	
	@RequestMapping(value="/{collectionName}",method=POST)
	public ErrorMessageInfo createCollection(@PathVariable final String collectionName) {
		logger.info("trying to create collection: {}", collectionName);
		if(mongoTemplate.collectionExists(collectionName)) {
			throw new DeckendException(COLLECTION_ALREADY_EXISTS, collectionName);
		}
		mongoTemplate.createCollection(collectionName).getName();
		return messageInfo(COLLECTION_CREATED_SUCCESSFULLY); 
	}
	
	@RequestMapping(value="/{collectionName}",method=DELETE)
	public ErrorMessageInfo deleteCollection(@PathVariable final String collectionName) {
		logger.info("trying to drop collection: {}", collectionName);
		baseMongoRepository.verifyCollectionExists(collectionName);
		mongoTemplate.dropCollection(collectionName);
		return messageInfo(COLLECTION_DELETED_SUCCESSFULLY);
	}
	
	@RequestMapping(value="/{collectionName}/index",method=POST)
	public ErrorMessageInfo createIndex(@PathVariable final String collectionName, @RequestBody final String jsonIndex) {
		try {
			logger.info("trying to add index: {}, to collection: {}", jsonIndex, collectionName);
			baseMongoRepository.verifyCollectionExists(collectionName);
			DBObject keys = dbObjectUtils.parse(jsonIndex);
			mongoTemplate.getCollection(collectionName).createIndex(keys);
		} catch (Exception ex) {
			throw new DeckendException(ErrorMessageEnum.INVALID_INDEXES, jsonIndex);
		}
		
		return messageInfo(INDEXES_CREATED_SUCCESSFULLY);
	}
	
	@RequestMapping(value="/{collectionName}/index/{index}",method=DELETE)
	public ErrorMessageInfo dropIndex(@PathVariable final String collectionName, @PathVariable final String index) {
		logger.info("trying to drop index: {}, on collection: {}", index, collectionName);
		try {
			baseMongoRepository.verifyCollectionExists(collectionName);
			mongoTemplate.getCollection(collectionName).dropIndex(index);
		} catch (Exception ex) {
			throw new DeckendException(ErrorMessageEnum.INVALID_INDEXES, index);
		}
		
		return messageInfo(INDEXES_DROPPED_SUCCESSFULLY);
	}
	
	@RequestMapping(value="/{collectionName}/index",method=GET)
	public List<DBObject> showIndexes(@PathVariable final String collectionName) {
		logger.info("trying to get indexes for collection: {} ", collectionName);
		baseMongoRepository.verifyCollectionExists(collectionName);
		return mongoTemplate.getCollection(collectionName).getIndexInfo();
	}
}
