package io.deckend.node.controller;

import static io.deckend.node.exception.ErrorMessageEnum.INVALID_APP_KEY;
import static io.deckend.node.exception.ErrorMessageInfo.messageInfo;
import static io.deckend.node.infra.controller.HttpUtils.fromHeader;
import static io.deckend.node.infra.controller.HttpUtils.getLoggedUser;
import static io.deckend.node.utils.AuthEnum.X_AUTH_APP_KEY;
import static io.deckend.node.utils.AuthEnum.X_AUTH_APP_SYSTEM;
import static io.deckend.node.utils.AuthEnum.X_AUTH_EXTERNAL_TOKEN;
import static io.deckend.node.utils.DeckendAssert.isNotEmpty;
import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;
import static org.springframework.web.bind.annotation.RequestMethod.PUT;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import io.deckend.node.exception.ErrorMessageEnum;
import io.deckend.node.exception.ErrorMessageInfo;
import io.deckend.node.model.User;
import io.deckend.node.security.filter.AuthenticationFilter;
import io.deckend.node.service.UserService;

/**
 * @author Diogo Carleto - diogocarleto@gmail.com
 *
 */
@RestController
@RequestMapping("/user")
public class UserController {
	
	private static Logger logger = LoggerFactory.getLogger(UserController.class);
		
	@Resource
	private UserService userService;
	
	@Resource
	private HttpServletRequest httpRequest;
	
	@Resource
	private HttpServletResponse httpResponse;
		
	@RequestMapping(value="/signUp/{providerId}", method=POST)
	public User signUpSocial(@PathVariable final String providerId) {
		userService.signUp(providerId, fromHeader(httpRequest, X_AUTH_EXTERNAL_TOKEN));
		return getLoggedUser();
	}
	
	@RequestMapping(value="/signUp", method=POST)
	public User signUp(@RequestBody final User user) {
		logger.info("signUp internal");
		
		isNotEmpty(fromHeader(httpRequest, X_AUTH_APP_KEY), INVALID_APP_KEY, X_AUTH_APP_KEY.value);
		userService.signUp(user);
		
		return getLoggedUser();
	}
	
	/**
	 * Managed by {@link AuthenticationFilter}
	 * @param providerId
	 * @return
	 */
	@RequestMapping(value="/signIn/{providerId}", method=POST)
	public User signInSocial(@PathVariable final String providerId) {
		logger.info("signIn social");
		return getLoggedUser();
	}
	
	/**
	 * Managed by {@link AuthenticationFilter}
	 * @return
	 */
	@RequestMapping(value="/signIn", method=POST)
	public User signIn() {
		logger.info("signIn internal");
		String sysAppSystem = fromHeader(httpRequest, X_AUTH_APP_SYSTEM);
		
		
		User user;
		
//		if(sysAppSystem!=null && sysAppSystem.trim().length()>0) {
//			user = userService.signIn(username, password, sysAppSystem);
//		} else {				
//			user = userService.signIn(username, password);
//		}
			
		return getLoggedUser();
	}
	
	@RequestMapping(value="", method=PUT)
	public User update(@RequestBody final User user) {
		userService.update(user);
		return getLoggedUser();
	}
	
	@RequestMapping(value="", method=RequestMethod.DELETE)
	public ErrorMessageInfo remove() {
		logger.info("trying to remove user : "+getLoggedUser());
		userService.deleteLoggedUser();
		return messageInfo(ErrorMessageEnum.USER_DELETED_SUCCESSFULLY);
	}
	
	@RequestMapping(value="/changePassword", method=POST)
	public ErrorMessageInfo changePassword(@RequestParam final String newPassword) {		
		userService.changePassword(newPassword);
		return messageInfo(ErrorMessageEnum.PASSWORD_CHANGED_SUCCESSFULLY);
	}
	
	@RequestMapping(value="/linkUser/{providerId}", method=POST)
	public String linkUser() {
		return null;
	}
	
	@RequestMapping(value="/unlikUser", method=POST)
	public String unlinkUser() {
		return null;
	}
	
	@RequestMapping(value="", method=GET)
	public User me() {
		return getLoggedUser();
	}
}
