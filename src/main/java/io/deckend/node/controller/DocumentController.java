package io.deckend.node.controller;

import static org.springframework.web.bind.annotation.RequestMethod.DELETE;
import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;
import static org.springframework.web.bind.annotation.RequestMethod.PUT;

import java.util.List;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mongodb.DBObject;

import io.deckend.node.exception.ErrorMessageInfo;
import io.deckend.node.service.DocumentService;

/**
 * @author Diogo Carleto - diogocarleto@gmail.com
 *
 */
@RestController
@RequestMapping("/document")
public class DocumentController {
	
	private static final Logger logger = LoggerFactory.getLogger(DocumentController.class);
	
	@Resource
	private DocumentService documentService;
	
	@RequestMapping(value="/{collectionName}", method=POST)
	public DBObject createDocument(@PathVariable final String collectionName, @RequestBody final String json) {
		return documentService.createDocument(collectionName, json);
	}
	
	@RequestMapping(value="/{collectionName}/{id}", method=DELETE)
	public ErrorMessageInfo delete(@PathVariable final String collectionName, @PathVariable final String id) {
		return documentService.delete(collectionName, id);
	}
	
	@RequestMapping(value="/{collectionName}", method=PUT)
	public DBObject update(@PathVariable final String collectionName, @RequestBody final String json) {
		return documentService.update(collectionName, json);
	}
		
	@RequestMapping(value="/{collectionName}/{id}", method=PUT)
	public DBObject update(@PathVariable final String collectionName, @PathVariable final String id,
							@RequestBody final String json) {
		return documentService.updatePartial(collectionName, id, json);
	}
	
	@RequestMapping(value="/{collectionName}/find", method=POST)
	public List<DBObject> find(@PathVariable final String collectionName, @RequestBody final String query) {
		return documentService.find(collectionName, query);
	}
	
	@RequestMapping(value="/{collectionName}/find/{id}", method=GET)
	public DBObject findById(@PathVariable final String collectionName, @PathVariable final String id) {
		return documentService.findById(collectionName, id);
	}
	
	/**
	 * Grant permissions to User
	 * @param collectionName
	 * @param id
	 * @param action
	 * @param username
	 * @return
	 */
	@RequestMapping(value="/{collectionName}/{id}/{permission}/user/{username}", method=PUT)
	public ErrorMessageInfo grantToUser(@PathVariable final String collectionName, 
							@PathVariable final String id,
							@PathVariable final String permission,
							@PathVariable final String username) {
		return documentService.grantPermissionToUser(collectionName, id, permission, username);
	}
	
	@RequestMapping(value="/{collectionName}/{id}/{permission}/role/{role}", method=PUT)
	public ErrorMessageInfo grantToRole(@PathVariable final String collectionName, 
							@PathVariable final String id,
							@PathVariable final String permission,
							@PathVariable final String role) {
		
		return documentService.grantPermissionToRole(collectionName, id, permission, role);
	}
	
	@RequestMapping(value="/{collectionName}/{id}/{permission}", method=PUT)
	public ErrorMessageInfo grantToPublic(@PathVariable final String collectionName, 
							@PathVariable final String id,
							@PathVariable final String permission) {
		
		return documentService.grantPermissionToPublic(collectionName, id, permission);
	}
	
	@RequestMapping(value="/{collectionName}/{id}/{permission}/user/{username}", method=DELETE)
	public ErrorMessageInfo revokeForUser(@PathVariable final String collectionName, 
							@PathVariable final String id,
							@PathVariable final String permission,
							@PathVariable final String username) {
		
		return documentService.revokePermission(collectionName, id, permission, username);
	}
	
	@RequestMapping(value="/{collectionName}/{id}/{permission}/role/{role}", method=DELETE)
	public ErrorMessageInfo revokeToRole(@PathVariable final String collectionName, 
							@PathVariable final String id,
							@PathVariable final String permission,
							@PathVariable final String role) {
		
		return documentService.revokePermission(collectionName, id, permission, role);
	}
	
	@RequestMapping(value="/{collectionName}/{id}/{permission}", method=DELETE)
	public ErrorMessageInfo revokeToPublic(@PathVariable final String collectionName, 
							@PathVariable final String id,
							@PathVariable final String permission) {
		
		return documentService.revokePermission(collectionName, id, permission, "public");
	}
}
