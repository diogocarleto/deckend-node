package io.deckend.node.controller;

import static io.deckend.node.exception.ErrorMessageEnum.INVALID_PROXY;
import static io.deckend.node.exception.ErrorMessageEnum.INVALID_PROXY_ID;
import static io.deckend.node.exception.ErrorMessageEnum.PROXY_DELETED_SUCCESSFULLY;
import static io.deckend.node.exception.ErrorMessageInfo.messageInfo;
import static io.deckend.node.utils.DeckendAssert.isNotEmpty;
import static io.deckend.node.utils.DeckendAssert.isNotNull;
import static org.springframework.web.bind.annotation.RequestMethod.DELETE;
import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;
import static org.springframework.web.bind.annotation.RequestMethod.PUT;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.deckend.node.exception.ErrorMessageInfo;
import io.deckend.node.model.Proxy;
import io.deckend.node.repository.ProxyRepository;

/**
 * TODO - generate api documentation
 * Responsible for integration with third parties apps
 * @author Diogo Carleto - diogocarleto@gmail.com
 *
 */
@RestController
@PreAuthorize("hasAnyAuthority('ROLE_ADMIN','ROLE_SYSADMIN','ROLE_APPADMIN')")
@RequestMapping("/proxy")
public class ProxyController {
	
	private static final Logger logger = LoggerFactory.getLogger(ProxyController.class);
	
	@Resource
	private ProxyRepository proxyRepository;

	@RequestMapping(value="", method=POST)
	public Proxy create(@RequestBody final Proxy proxy) {
		logger.debug("Adding proxy {}", proxy);
		validateProxy(proxy);
		
		return proxyRepository.save(proxy);
	}
	
	@RequestMapping(value="", method=PUT)
	public Proxy update(@RequestBody final Proxy proxy) {
		logger.debug("Updating proxy {}", proxy);
		validateProxy(proxy);
		isNotEmpty(proxy.getId(), INVALID_PROXY_ID, proxy.getId());
		
		return proxyRepository.save(proxy);
	}
	
	@RequestMapping(value="/{name}", method=DELETE)
	public ErrorMessageInfo delete(@PathVariable final String name) {
		logger.debug("Deleting proxy by id {}", name);
		isNotEmpty(name, INVALID_PROXY_ID, name);
		
		proxyRepository.deleteByName(name);
		return messageInfo(PROXY_DELETED_SUCCESSFULLY);
	}
	
	@RequestMapping(value="/{id}", method=GET)
	public Proxy findById(@PathVariable final String id) {
		logger.debug("Finding proxy by id {}", id);
		
		isNotEmpty(id, INVALID_PROXY_ID, id);
		return proxyRepository.findOne(id);
	}
	
	private void validateProxy(final Proxy proxy) {
		isNotNull(proxy, INVALID_PROXY, proxy);
		isNotEmpty(proxy.getUrl(), INVALID_PROXY, proxy);
		isNotEmpty(proxy.getName(), INVALID_PROXY, proxy);
	}
}
