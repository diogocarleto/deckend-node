package io.deckend.node.infra.model;

import static io.deckend.node.infra.controller.HttpUtils.getLoggedUser;
import static org.springframework.data.mongodb.core.query.Criteria.where;
import static org.springframework.data.mongodb.core.query.Query.query;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.bson.types.ObjectId;
import org.springframework.data.domain.AuditorAware;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.util.Assert;

import com.mongodb.DBObject;
import com.mongodb.gridfs.GridFSFile;
import com.mongodb.util.JSON;
import com.mongodb.util.JSONParseException;

import io.deckend.node.exception.DeckendException;
import io.deckend.node.exception.ErrorMessageEnum;
import io.deckend.node.infra.ACLPermissionsEnum;
import io.deckend.node.infra.RoleEnum;
import io.deckend.node.utils.DeckendAssert;

/**
 * @author Diogo Carleto - diogocarleto@gmail.com
 *
 */
public final class DBObjectUtils {

	private final AuditorAware<String> auditorAware;
	
	public static final String ID_FIELD = "_id";
	public static final String ADD_USER = "addUser";
	public static final String CHANGE_USER = "changeUser";
	public static final String ADD_DATE = "addDate";
	public static final String CHANGE_DATE = "changeDate";
	public static final String VERSION = "version";
	public static final String CLASS = "_class";
	public static final String ACL = "ACL";
	
	public DBObjectUtils(final AuditorAware<String> auditorAware) {
		super();
		this.auditorAware = auditorAware;
	}

	/**
	 * Convert a given Json to DBObject, setting auditing properties to object.
	 * @param collection
	 * @param jsonString
	 * @return
	 */
	public DBObject toAuditableDBObject(final String collection, final String jsonString) {
		Assert.notNull(collection);
		Assert.notNull(jsonString);
		
		DBObject dbObject = parse(jsonString);
		return toAuditableDBObject(collection, dbObject);
	}
	
	/**
	 * TODO - add aspectJ to intercept before save objects.
	 * Do not call from any place instead of BaseMongoRepository
	 * Get the dbObject and set auditing properties.
	 * @param collection
	 * @param dbObject
	 * @return
	 */
	public DBObject toAuditableDBObject(final String collection, final DBObject dbObject) {
		Assert.notNull(collection);
		Assert.notNull(dbObject);
		
		LocalDateTime ldt = LocalDateTime.now();
		
		//adjust date creation and change
		if(isNew(dbObject)) {
			dbObject.put(ADD_USER, auditorAware.getCurrentAuditor());
			dbObject.put(ADD_DATE, ldt.toString());
			dbObject.put(CHANGE_USER, null);
			dbObject.put(CHANGE_DATE, null);
			dbObject.put(VERSION, 0);		
			dbObject.put(CLASS, collection);
			dbObject.put(ACL, null);
		} else {
			DeckendAssert.isNotNull(dbObject.get(VERSION), ErrorMessageEnum.VERSION_NOT_FOUND_IN_OBJECT, dbObject.toMap());
			
			dbObject.put(CHANGE_USER, auditorAware.getCurrentAuditor());
			dbObject.put(CHANGE_DATE, ldt.toString());
			dbObject.put(VERSION, Integer.valueOf(dbObject.get(VERSION).toString())+1);
			dbObject.removeField(ID_FIELD);//cleaned to perform update commands
		}		
		
		return dbObject;
	}
	
	public DBObject toDBObject(final GridFSFile gridFSFile) {
		DBObject dbObject = gridFSFile.getMetaData();
		dbObject.put(ID_FIELD, gridFSFile.getId());
		dbObject.put("filename", gridFSFile.getFilename());
		dbObject.put("chunkSize", gridFSFile.getChunkSize());
		dbObject.put("contentType", gridFSFile.getContentType());
		dbObject.put("length", gridFSFile.getLength());
		dbObject.put("md5", gridFSFile.getMD5());
		return dbObject;
	}
	
	public ObjectId buidObjectId(final String id) {
		try {
			return new ObjectId(id);
		} catch (Exception ex) {
			throw new DeckendException(ErrorMessageEnum.INVALID_ID, id);
		}
	}
	
	/**
	 * Adjust id to response.
	 * @param dbObject
	 * @param objectId
	 * @return
	 */
	public DBObject adjustIdToResponse(final DBObject dbObject, final ObjectId objectId) {
		dbObject.put(ID_FIELD, objectId.toString());//put id again, the id is removed on dbObjectUtils.toDBObject to resolve update problems.
		return dbObject;
	}
	
	public DBObject parse(final String json) {
		try {
			return (DBObject) JSON.parse(json);
		} catch(JSONParseException ex) {
			throw new DeckendException(ErrorMessageEnum.INVALID_JSON, ex.getMessage());
		}
	}
	
	/**
	 * Build query verifying if the loggedUser can handle the DBObject
	 * Only who created or with ROLE_APPADMIN/ROLE_SYSADMIN or with permissions can handle.
	 * @param id
	 * @return
	 */
	public Query buildQuery(final String id, final Optional<Integer> version, final ACLPermissionsEnum... aclEnum) {
		Query query = query(where(ID_FIELD).is(buidObjectId(id)));	
		
		if(version.isPresent()) {
			query.addCriteria(new Criteria().and(VERSION).is(version.get()));
		}
		
		if (!RoleEnum.isTypeAppAdminOrSysAdmin(getLoggedUser().getRole())) {
			query.addCriteria(criteriaBasedOnACL(aclEnum));
		}
		
		return query;
	}
	
	public DBObject buildQueryToRead(final DBObject preQuery) {
		Query query = new Query();		
		if (!RoleEnum.isTypeAppAdminOrSysAdmin(getLoggedUser().getRole())) {
			query.addCriteria(criteriaBasedOnACL(ACLPermissionsEnum.READ));
		}
		
		if(preQuery!=null) {
			preQuery.putAll(query.getQueryObject());
		}
		return preQuery;
	}
	
	
	public Criteria criteriaBasedOnACL(final ACLPermissionsEnum... aclPermissions) {
		String ACL = "ACL.";
		String aclUsername = ACL+getLoggedUser().getUsername();
		String aclRole = ACL+getLoggedUser().getRole();
		
		List<Criteria> listCriteriaOr = new ArrayList<>();
		for(ACLPermissionsEnum aclPermission: aclPermissions) {
			listCriteriaOr.add(where(aclUsername).is(aclPermission.aclValue));
			listCriteriaOr.add(where(aclRole).is(aclPermission.aclValue));
			listCriteriaOr.add(where(ACL+"public").is(aclPermission.aclValue));
		}
		listCriteriaOr.add(where(ADD_USER).is(getLoggedUser().getUsername()));
		
		Criteria criteria = new Criteria();		
		criteria.orOperator((Criteria[]) listCriteriaOr.toArray(new Criteria[listCriteriaOr.size()]));
		
		return criteria;
	}
	
	public Optional<Integer> getVersion(final DBObject dbObject) {
		return Optional.ofNullable((Integer) dbObject.get(VERSION));
	}
	
	public Optional<String> getId(final DBObject dbObject) {
		if(dbObject.get(ID_FIELD) instanceof ObjectId) {
			return Optional.ofNullable(((ObjectId) dbObject.get(ID_FIELD)).toString());
		} 
		
		return Optional.ofNullable((String) dbObject.get(ID_FIELD));
	}
	
	private boolean isNew(final DBObject dbObject) {
		return !getId(dbObject).isPresent();
	}
}
