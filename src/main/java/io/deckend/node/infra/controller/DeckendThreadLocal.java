package io.deckend.node.infra.controller;

import io.deckend.node.model.Token;

/**
 * @author Diogo Carleto - diogocarleto@gmail.com
 *
 */
public final class DeckendThreadLocal {

	private DeckendThreadLocal() {}
	
	private static final ThreadLocal<Token> tokenThreadLocal = new ThreadLocal<>();
		
	public static void setToken(final Token token) {
		tokenThreadLocal.set(token);
	}
	
	public static Token getToken() {
		return tokenThreadLocal.get();
	}
	
}
