package io.deckend.node.infra.controller;

import static io.deckend.node.utils.AuthEnum.X_AUTH_TOKEN;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.context.SecurityContextHolder;

import io.deckend.node.model.User;
import io.deckend.node.utils.AuthEnum;

/**
 * @author Diogo Carleto - diogocarleto@gmail.com
 *
 */
public final class HttpUtils {

	private static final Logger logger = LoggerFactory.getLogger(HttpUtils.class);
	
	public static String fromHeader(final HttpServletRequest httpRequest, final AuthEnum authEnum) {
		return httpRequest.getHeader(authEnum.value);
	}
	
	public static void handleAuthenticatioTokenResponse(final HttpServletResponse httpResponse) {
		logger.info("Sending response token to client: {}", DeckendThreadLocal.getToken().getUser().getUsername());
		httpResponse.setStatus(HttpServletResponse.SC_OK);		
        httpResponse.addHeader("Content-Type", "application/json");
        httpResponse.addHeader(X_AUTH_TOKEN.value, DeckendThreadLocal.getToken().getValue());
	}
	
	public static User getLoggedUser() {
		User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		user.setPassword(null);		
		return user;
	}
}
