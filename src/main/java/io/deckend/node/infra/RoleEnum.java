package io.deckend.node.infra;

/**
 * @author Diogo Carleto - diogocarleto@gmail.com
 *
 */
public enum RoleEnum {
	
	ROLE_ANONYMOUS("ROLE_ANONYMOUS"),
	ROLE_USER("ROLE_USER"),
	ROLE_ADMIN("ROLE_ADMIN"),
	ROLE_APPADMIN("ROLE_APPADMIN"),
	ROLE_SYSADMIN("ROLE_SYSADMIN");
	
	public final String value;

	private RoleEnum(final String value) {
		this.value = value;
	}

	public static boolean isTypeAppAdminOrSysAdmin(final String role) {
		return isTypeSysAdmin(role) || ROLE_ADMIN.value.equals(role) || ROLE_APPADMIN.value.equals(role);
	}

	public static boolean isTypeSysAdmin(final String role) {
		return ROLE_SYSADMIN.value.equals(role);
	}
}
