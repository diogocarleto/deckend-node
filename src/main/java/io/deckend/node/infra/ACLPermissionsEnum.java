package io.deckend.node.infra;

import io.deckend.node.exception.DeckendException;
import static io.deckend.node.exception.ErrorMessageEnum.*;

/**
 * Enum that contains Access Control Lists type permissions
 * @author Diogo Carleto - diogocarleto@gmail.com
 *
 */
public enum ACLPermissionsEnum {

	READ("read"),
	WRITE("write");
	
	public final String aclValue;

	private ACLPermissionsEnum(final String value) {
		aclValue = value;
	}
	
	public static ACLPermissionsEnum getValue(String aclValue) {
		for(ACLPermissionsEnum aclPermissionEnum: ACLPermissionsEnum.values()) {
			if(aclValue.equals(aclPermissionEnum.aclValue)) {
				return aclPermissionEnum;
			}
		}
		
		throw new DeckendException(INVALID_ACL_PERMISSION, aclValue);
	}
}
