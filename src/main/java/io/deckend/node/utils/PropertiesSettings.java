package io.deckend.node.utils;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * Load custom properties from application.properties
 * @author Diogo Carleto - diogocarleto@gmail.com
 *
 */
@Component
@ConfigurationProperties("custom")
public class PropertiesSettings {
	
	private boolean development;

	public boolean isDevelopment() {
		return development;
	}

	public void setDevelopment(final boolean development) {
		this.development = development;
	}
}
