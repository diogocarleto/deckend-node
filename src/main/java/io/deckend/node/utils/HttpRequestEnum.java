package io.deckend.node.utils;

/**
 * @author Diogo Carleto - diogocarleto@gmail.com
 *
 */
public enum HttpRequestEnum {

	USER("user");
	
	private String value;

	private HttpRequestEnum(final String value) {
		this.value = value;
	}

	public String getValue() {
		return value;
	}
}
