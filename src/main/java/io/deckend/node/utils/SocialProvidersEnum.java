package io.deckend.node.utils;

/**
 * @author Diogo Carleto - diogocarleto@gmail.com
 *
 */
public enum SocialProvidersEnum {

	FACEBOOK("facebook", "fb"),
	TWITTER("twitter", "tw");
	
	public final String providerId;
	public final String prefix;

	private SocialProvidersEnum(final String providerId, final String prefix) {
		this.providerId = providerId;
		this.prefix = prefix;
	}

	public String getProviderId() {
		return providerId;
	}
	
	public String getPrefix() {
		return prefix;
	}

	public static boolean isSupported(final String providerId) {
		for(final SocialProvidersEnum spe: SocialProvidersEnum.values()) {
			if (spe.getProviderId().equalsIgnoreCase(providerId)) {
				return true;
			}
		}
		
		return false;
	}
	
}
