package io.deckend.node.utils;

import java.lang.reflect.Array;
import java.util.Collection;
import java.util.Map;

import io.deckend.node.exception.DeckendException;
import io.deckend.node.exception.ErrorMessageEnum;

/**
 * It seems like {@link org.springframework.util.Assert}, but throw a {@link io.deckend.node.exception.DeckendException}
 * @author Diogo Carleto - diogocarleto@gmail.com
 *
 */
public abstract class DeckendAssert {

	public static void isNull(final Object object, final ErrorMessageEnum errorEnum, final Object... params) {
		if(object != null) {
			throw new DeckendException(errorEnum, params);
		}
	}
		
	public static void isNotNull(final Object object, final ErrorMessageEnum errorEnum, final Object... params) {
		if(object == null) {
			throw new DeckendException(errorEnum, params);
		}
	}
	
	public static void isNotEmpty(final Object object, final ErrorMessageEnum errorMessageEnum, final Object... params) {
		if (isEmpty(object)) {
			throw new DeckendException(errorMessageEnum, params);
		}
	}
	
	@SuppressWarnings("rawtypes")
	private static boolean isEmpty(final Object object) {
		if (object == null)		
			return true;
		
		if (object.getClass().isArray()) {
			return Array.getLength(object) == 0;
		} else if (object.getClass().isEnum()) {
			return ((Enum) object).name() == null;
		} else if ((object instanceof Collection)) {
			return ((Collection)object ).size() == 0; 
		} else if ((object instanceof Map)) {
			return((Map) object ).size() == 0;
		} else {
			return ((String)object).length() == 0;
		}
	}
}
