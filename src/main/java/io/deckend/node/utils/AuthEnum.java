package io.deckend.node.utils;

/**
 * Contains header's keys.
 * @author Diogo Carleto - diogocarleto@gmail.com
 *
 */
public enum AuthEnum {
	X_AUTH_USERNAME("X-Auth-Username"),
	X_AUTH_PASSWORD("X-Auth-Password"),
	X_AUTH_TOKEN("X-Auth-Token"),
	X_AUTH_EXTERNAL_TOKEN("X-Auth-External-Token"),
	X_AUTH_APP_KEY("X-Auth-AppKey"),
	X_AUTH_APP_SYSTEM("X-Auth-AppSystem");
	
	public final String value;

	private AuthEnum(final String value) {
		this.value = value;
	}
}
