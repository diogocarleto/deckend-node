package io.deckend.node.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import io.deckend.node.model.User;

/**
 * @author Diogo Carleto - diogocarleto@gmail.com
 *
 */
@Service
public class SocialService {

	private static final Logger logger = LoggerFactory.getLogger(SocialService.class);
	
//	@Inject
//	@SocialProvider(SocialProvidersEnum.FACEBOOK)
//	private SocialTokenAuthenticatorProvider facebookTokenAuthenticator;
//	
//	@Inject
//	@SocialProvider(SocialProvidersEnum.TWITTER)
//	private SocialTokenAuthenticatorProvider twitterTokenAuthenticator;
	
	public User authenticateBy(final String providerId, final String accessToken) {
//		logger.info("trying to authenticate by {}", providerId);
//		SocialProvidersEnum spe = SocialProvidersEnum.valueOf(providerId.toUpperCase());
//		User user = null;
//		Authentication resultOfAuthentication = null;
//		
//		switch (spe) {
//		case FACEBOOK:
//			resultOfAuthentication = facebookTokenAuthenticator
//						.authenticate(new SocialPreAuthenticatedAuthenticationToken(spe, accessToken));
//			break;
//		case TWITTER:
//			resultOfAuthentication = twitterTokenAuthenticator
//				.authenticate(new SocialPreAuthenticatedAuthenticationToken(spe, accessToken));
//			break;
//		}
//		
//		SecurityContextHolder.getContext().setAuthentication(resultOfAuthentication);
//		return (User) resultOfAuthentication.getPrincipal();
		return null;
	}
}
