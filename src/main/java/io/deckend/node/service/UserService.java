package io.deckend.node.service;

import static io.deckend.node.infra.RoleEnum.ROLE_USER;
import static io.deckend.node.utils.DeckendAssert.isNotEmpty;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import io.deckend.node.config.settings.UserAdministratorSettings;
import io.deckend.node.exception.DeckendException;
import io.deckend.node.exception.ErrorMessageEnum;
import io.deckend.node.infra.RoleEnum;
import io.deckend.node.infra.controller.HttpUtils;
import io.deckend.node.model.User;
import io.deckend.node.repository.UserRepository;
import io.deckend.node.security.authentication.SocialPreAuthenticatedAuthToken;
import io.deckend.node.utils.SocialProvidersEnum;

/**
 * @author Diogo Carleto - diogocarleto@gmail.com
 *
 */
@Service
public class UserService {
	
	private static Logger logger = LoggerFactory.getLogger(UserService.class);
	
	@Resource
	private UserRepository userRepository;
	
	@Resource
	private TokenService tokenService;
	
	@Resource
	private SocialService socialService;
	
	@Resource
	private AuthenticationManager authenticationManager;
	
	@Resource
	private PasswordEncoder passwordEncoder;
	
	@Resource
	private UserAdministratorSettings userAdministratorSettings;
	
	@Resource
	private HttpServletResponse httpResponse;
	
	public void createAdminUser() {
		userRepository.save(User.user(userAdministratorSettings.getName(), "", 
										userAdministratorSettings.getUsername(), 
										passwordEncoder.encode(userAdministratorSettings.getPassword()), 
										userAdministratorSettings.getRole(), 
										userAdministratorSettings.getEmail()));
	}
	
	public User signUp(final String providerId, final String accessToken) {
		logger.info("signUp to providerId: {}", providerId);
		if(!SocialProvidersEnum.isSupported(providerId)) {
			throw new DeckendException(ErrorMessageEnum.PROVIDER_ID_NOT_SUPPORTED, providerId);
		}
		
		authenticationManager.authenticate(new SocialPreAuthenticatedAuthToken(SocialProvidersEnum.valueOf(providerId.toUpperCase()), accessToken));
		HttpUtils.handleAuthenticatioTokenResponse(httpResponse);
		return getLoggedUser();
	}
	
	//TODO - mover para filtro
	public User signUp(final User user) {
		isNotEmpty(user.getUsername(), ErrorMessageEnum.INVALID_USERNAME, user.getUsername());
		isNotEmpty(user.getEmail(), ErrorMessageEnum.INVALID_EMAIL, user.getEmail());
		isNotEmpty(user.getPassword(), ErrorMessageEnum.INVALID_PASSWORD, user.getEmail());
		
		user.setRole(ROLE_USER.value);//every signUp set role to ROLE_USER
		
		String decryptedPassword = user.getPassword();
		user.setPassword(passwordEncoder.encode(user.getPassword()));
		User savedUser = userRepository.save(user);
		authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(savedUser.getUsername(), decryptedPassword));
		HttpUtils.handleAuthenticatioTokenResponse(httpResponse);//TODO ver como chamar a autenticação do AuthenticationFilter aqui.
		return getLoggedUser();
	}
	
	/**
	 * TODO future feature
	 * Used to signIn systemUser, who can administrate any app.
	 * @param username
	 * @param password
	 * @param appKey
	 * @param sysAppSystem
	 * @return
	 */
//	public User signIn(final String username, final String password, final String sysAppSystem) {
//		SystemUser sysUser = sysUserRepository.findByUsername(username);
//		
//		if(sysUser==null || !passwordEncoder.matches(password, sysUser.getPassword())) {//passwordEncoder.matches used to verify authenticy of password
//			throw new DeckendException(ErrorMessageEnum.INVALID_USERNAME_PASSWORD);
//		}
//		
//		User mockedAppUser = user(sysUser.getName(), "", username, password, sysUser.getRole())
//								.app(DeckendThreadLocal.getApp());
//		
//		tokenService.buildToken(mockedAppUser);		
//		return mockedAppUser;
//	}
	
	/**
	 * Verify if the role is valid at the moment of adding user.
	 * Only the ROLE_ADMIN or ROLE_SYSADMIN can set another role different than ROLE_USER.
	 * {@link dc.infra.secutiry.utils.RoleEnum}
	 * @param user
	 * @return
	 */
	private boolean isRolePermittedToAddUser(final User user) {
		if(user.getRole().equals(ROLE_USER.value)) 
			return true;
		
		if(!(SecurityContextHolder.getContext().getAuthentication().getPrincipal() instanceof User)) 
			return false;
		
		User loggedUser = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		if(!RoleEnum.isTypeAppAdminOrSysAdmin(loggedUser.getRole())) 
			return false;
		
		return true;
	}
	
	public void changePassword(final String password) {
		isNotEmpty(password, ErrorMessageEnum.INVALID_PASSWORD);
		
		User userDb = getLoggedUser();
		userDb.setPassword(passwordEncoder.encode(password));
		userRepository.save(userDb);
		tokenService.removeToken();
	}

	//TODO - avaliar se o APPADMIN pode alterar
	public User update(final User user) {
		if(!user.equals(getLoggedUser())) {
			throw new DeckendException(ErrorMessageEnum.CANNOT_UPDATE_DIFERENT_USER);
		}
		
		User savedUser = userRepository.save(getLoggedUser().mergeToUpdate(user));
		tokenService.updateTokenContent(savedUser);
		
		return getLoggedUser();
	}

	/**
	 * Unused yet, will be used to app administrator remove users
	 * @param username
	 */
	public void delete(final String username) {
		User user = userRepository.findByUsername(username);
		if(!getLoggedUser().equals(user) && !RoleEnum.isTypeSysAdmin(getLoggedUser().getRole())) {
			throw new DeckendException(ErrorMessageEnum.INSUFFICIENT_CREDENTIALS_TO_REMOVE_USER, username);
		}
		
		userRepository.delete(user);
	}
	
	public void deleteLoggedUser() {
		User user = userRepository.findByUsername(getLoggedUser().getUsername());
		userRepository.delete(user);
	}
	
	private User getLoggedUser() {
		User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		return user;
	}
}
