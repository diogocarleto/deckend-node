package io.deckend.node.service;

import static io.deckend.node.model.Token.token;

import java.time.LocalDateTime;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import com.hazelcast.spring.cache.HazelcastCacheManager;

import io.deckend.node.config.HazelcastConfiguration;
import io.deckend.node.config.settings.TokenSettings;
import io.deckend.node.infra.controller.DeckendThreadLocal;
import io.deckend.node.model.Token;
import io.deckend.node.model.User;

/**
 * TODO - see if is necessary implement evictTokens.
 * @author Diogo Carleto - diogocarleto@gmail.com
 *
 */
@Service
public class TokenService {

	@Autowired
	private TokenSettings tokenSettings;
	
	@Autowired
	private HazelcastCacheManager hazelcastcacheManager;
	
	public Token buildToken(final User user) {
		Token token = token(user, calcTimeout());		
		hazelcastcacheManager.getCache(HazelcastConfiguration.AUTHENTICATION_TOKENS_CACHE).put(token.getValue(), token);
		DeckendThreadLocal.setToken(token);
		return token;
	}
	
	private LocalDateTime calcTimeout() {
		return LocalDateTime.now().plusMinutes(tokenSettings.getTimeout());
	}

	public void removeToken() {
		hazelcastcacheManager.getCache(HazelcastConfiguration.AUTHENTICATION_TOKENS_CACHE).evict(DeckendThreadLocal.getToken().getValue());
	}
	
	@Cacheable(HazelcastConfiguration.AUTHENTICATION_TOKENS_CACHE)
	public Token findByValue(final String value) {
		return (Token) hazelcastcacheManager.getCache(HazelcastConfiguration.AUTHENTICATION_TOKENS_CACHE).get(value);
	}
	
	/**
	 * Update values when user update happens.
	 * @param user
	 */
	public void updateTokenContent(final User user) {
		DeckendThreadLocal.getToken().setUser(user);
	}
}
