package io.deckend.node.service;

import static io.deckend.node.exception.ErrorMessageEnum.INVALID_COLLECTION;
import static io.deckend.node.exception.ErrorMessageEnum.INVALID_ID;
import static io.deckend.node.exception.ErrorMessageEnum.INVALID_USERNAME;
import static io.deckend.node.exception.ErrorMessageEnum.PERMISSION_GRANTED_SUCCESSFULLY;
import static io.deckend.node.exception.ErrorMessageEnum.PERMISSION_REVOKED_SUCCESSFULLY;
import static io.deckend.node.exception.ErrorMessageInfo.messageInfo;
import static io.deckend.node.infra.ACLPermissionsEnum.READ;
import static io.deckend.node.infra.ACLPermissionsEnum.WRITE;
import static io.deckend.node.infra.model.DBObjectUtils.ACL;
import static io.deckend.node.utils.DeckendAssert.isNotEmpty;
import static io.deckend.node.utils.DeckendAssert.isNotNull;

import java.util.List;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.stereotype.Service;

import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;

import io.deckend.node.exception.ErrorMessageInfo;
import io.deckend.node.infra.ACLPermissionsEnum;
import io.deckend.node.infra.model.DBObjectUtils;
import io.deckend.node.model.User;
import io.deckend.node.repository.BaseMongoRepository;
import io.deckend.node.repository.UserRepository;

/**
 * @author Diogo Carleto - diogocarleto@gmail.com
 *
 */
@Service
public class DocumentService {
	
	private static final Logger logger = LoggerFactory.getLogger(DocumentService.class);

	@Resource
	private BaseMongoRepository baseRepository;
	
	@Resource
	private DBObjectUtils dbObjectUtils;
	
	@Resource
	private UserRepository userRepository;
	
	@Resource
	private MongoTemplate mongoTemplate;
	
	public DBObject createDocument(final String collectionName, final String json) {
		logger.info("trying to create Document: {}", json);
		return baseRepository.save(collectionName, json);
	}
	
	public ErrorMessageInfo delete(final String collectionName, final String id) {
		logger.info("trying to delete Document: {}", id);
		return baseRepository.delete(collectionName, id);
	}
	
	public DBObject update(final String collectionName, final String json) {
		logger.info("trying to update Document: {}", json);
		return baseRepository.save(collectionName, json);
	}
		
	public DBObject updatePartial(final String collectionName, 
									final String id,
									final String json) {
		logger.info("trying to update partial Document: {}", json);
		return baseRepository.updatePartial(collectionName, id, json);
	}
	
	public List<DBObject> find(final String collectionName, final String query) {
		logger.info("trying to find Documents: {}", query);
		return baseRepository.find(collectionName, query);
	}
	
	public DBObject findById(final String collectionName, final String id) {
		logger.info("trying to find Document by id: {}", id);
		return baseRepository.findById(collectionName, id, READ, WRITE);
	}
	
	public ErrorMessageInfo grantPermissionToUser(final String collectionName, 
										final String id,
										final String permission,
										final String username) {
		logger.info("trying to grant permissions to user: {}", username);
		
		checkCollectionNameAndId(collectionName, id);
		
		ACLPermissionsEnum aclPermission = ACLPermissionsEnum.getValue(permission);
		User user = userRepository.findByUsername(username);
		isNotNull(user, INVALID_USERNAME, username);
		
		addACL(collectionName, id, username, aclPermission);
		return messageInfo(PERMISSION_GRANTED_SUCCESSFULLY);
	}
	
	public ErrorMessageInfo grantPermissionToRole(final String collectionName, 
										final String id,
										final String permission,
										final String role) {
		logger.info("trying to grant permissions to role: {}", role);
		
		checkCollectionNameAndId(collectionName, id);
		ACLPermissionsEnum aclPermission = ACLPermissionsEnum.getValue(permission);
		
		addACL(collectionName, id, role, aclPermission);
		return messageInfo(PERMISSION_GRANTED_SUCCESSFULLY);
	}
	
	public ErrorMessageInfo grantPermissionToPublic(final String collectionName, 
											final String id,
											final String permission) {
			logger.info("trying to grant permissions to public");
			
			checkCollectionNameAndId(collectionName, id);
			ACLPermissionsEnum aclPermission = ACLPermissionsEnum.getValue(permission);
			
			addACL(collectionName, id, "public", aclPermission);
			return messageInfo(PERMISSION_GRANTED_SUCCESSFULLY);
	}
	
	public ErrorMessageInfo revokePermission(final String collectionName, 
			final String id,
			final String permission,
			final String typePermission) {
		logger.info("trying to revoke permission to typePermission: {}", typePermission);
		
		checkCollectionNameAndId(collectionName, id);
		ACLPermissionsEnum aclPermission = ACLPermissionsEnum.getValue(permission);
		
		removeACL(collectionName, id, typePermission, aclPermission);
		return messageInfo(PERMISSION_REVOKED_SUCCESSFULLY);
	}
	
	/**
	 * 
	 * @param collection
	 * @param id
	 * @param typePermission - username,role or public.
	 * @param aclPermission
	 */
	private void addACL(final String collection, final String id, String typePermission, ACLPermissionsEnum aclPermission) {
		DBObject objToSave = findById(collection, id);
		DBObject acl = new BasicDBObject(typePermission, aclPermission.aclValue);
		objToSave.put(ACL, acl);
		
		mongoTemplate.getCollection(collection).save(objToSave);
	}
	
	private void removeACL(final String collection, final String id, String typePermission, ACLPermissionsEnum aclPermission) {
		DBObject objToSave = findById(collection, id);
		if(objToSave.get(ACL) != null) {
			BasicDBObject acl = (BasicDBObject) objToSave.get(ACL);
			if(acl.containsField(typePermission) && acl.containsValue(aclPermission.aclValue)) {
				acl.remove(typePermission);
				mongoTemplate.getCollection(collection).save(objToSave);
			}
		}
	}
	
	private void checkCollectionNameAndId(final String collectionName, final String id) {
		isNotEmpty(id, INVALID_ID, id);
		isNotEmpty(collectionName, INVALID_COLLECTION, collectionName);
	}
}
