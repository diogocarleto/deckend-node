package io.deckend.node.profile;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.context.request.RequestContextListener;

import io.deckend.node.infra.RoleEnum;
import io.deckend.node.model.User;
import io.deckend.node.repository.UserRepository;
import io.deckend.node.service.UserService;

//TODO - criar um superusuário, usado tanto para testes quanto para produção.
/**
 * @author Diogo Carleto - diogocarleto@gmail.com
 *
 */
@Configuration
@Profile("dev")
public class DevConfiguration {
	
	private final Logger logger = LoggerFactory.getLogger(DevConfiguration.class);
		
	@Bean
	@ConditionalOnMissingBean
	public RequestContextListener requestContextListener() {
		return new RequestContextListener();
	}
		
	@Resource
	private UserRepository userRepository;
	
	@Resource
	private UserService userService;
	
	@Resource
    private PasswordEncoder passwordEncoder;
	
	@Resource
	private MongoOperations mongoOperations;
	
	@PostConstruct
	private void initDatabase() {
//		cleanDatabase();// commented because this remove all indexes of @Document
		userRepository.deleteAll();
		userService.createAdminUser();
		userRepository.save(User.user("dtest", "", 
				"dtest", 
				passwordEncoder.encode("123"), 
				RoleEnum.ROLE_USER.value, 
				"dtest@gmail.com"));
		
		userRepository.save(User.user("dtest1", "", 
				"dtest1", 
				passwordEncoder.encode("123"), 
				RoleEnum.ROLE_USER.value, 
				"dtest1@gmail.com"));
	}
	
	@PreDestroy
	private void cleanDatabase() {
		mongoOperations.getCollectionNames().stream()
			.forEach((collection) -> {
				if(!collection.equals("system.indexes"))
					mongoOperations.dropCollection(collection);
		});
		
		mongoOperations.dropCollection(User.class);
		userRepository.deleteAll();
		logger.info("Database cleaned");
	}
}
