package io.deckend.node.filter;

import static io.deckend.node.exception.ErrorMessageEnum.INVALID_PROXY;
import static io.deckend.node.exception.ErrorMessageEnum.PROXY_NOT_FOUND;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.time.LocalDateTime;
import java.util.Enumeration;

import javax.annotation.Resource;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.tomcat.util.http.fileupload.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpMethod;
import org.springframework.http.client.ClientHttpRequest;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.http.client.SimpleClientHttpRequestFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RequestCallback;
import org.springframework.web.client.ResponseExtractor;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.filter.OncePerRequestFilter;
import org.springframework.web.util.UriComponentsBuilder;

import io.deckend.node.exception.DeckendException;
import io.deckend.node.model.Proxy;
import io.deckend.node.repository.ProxyRepository;

/**
 * @author Diogo Carleto - diogocarleto@gmail.com
 *
 */
@Component
public class ProxyFilter extends OncePerRequestFilter {

	private static final Logger logger = LoggerFactory.getLogger(ProxyFilter.class);

	@Resource
	ProxyRepository proxyRepository;

	private void validateProxyName(String url) {
		String[] parts = url.split("/proxies/");
		if(parts.length < 2) {
			logger.error("Invalid proxy name: {}", url);
			throw new DeckendException(INVALID_PROXY);
		}
	}

	/**
	 * Return an array with proxyName, urlParameters.
	 * @param url
	 * @return
	 */
	private String[] getProxyNameAndParameters(String url) {
		validateProxyName(url);

		String proxies = "/proxies/";
		String cleanUrl = url.substring(url.indexOf(proxies)+proxies.length());

		if(cleanUrl.contains("/")) {
			int position = cleanUrl.indexOf("/");
			return new String[] {cleanUrl.substring(0, position), cleanUrl.substring(position)};
		}

		return new String[] {cleanUrl, ""};
	}

	@Override
	protected void doFilterInternal(final HttpServletRequest request, final HttpServletResponse response, final FilterChain filterChain)
			throws ServletException, IOException {
		logger.info("Calling to: {}, time: {}",  request.getRequestURI().toString(), LocalDateTime.now());

		String[] proxyNameAndParameters = getProxyNameAndParameters(request.getRequestURI().toString());

		Proxy proxy = proxyRepository.findByName(proxyNameAndParameters[0]);
		if(proxy==null) {
			logger.error("Proxy not found: {}", request.getRequestURI().toString());
			throw new DeckendException(PROXY_NOT_FOUND);
		}


		SimpleClientHttpRequestFactory requestFactory = new SimpleClientHttpRequestFactory();
		boolean isBufferRequestBody = HttpMethod.valueOf(request.getMethod()).equals(HttpMethod.POST) ? false : true;
		requestFactory.setBufferRequestBody(isBufferRequestBody);
		RestTemplate restTemplate = new RestTemplate(requestFactory);

		RequestCallback requestCallback = (final ClientHttpRequest clientHttpRequest) -> {
			logger.info("copying headers");
			for(Enumeration<String> e = request.getHeaderNames(); e.hasMoreElements();) {
				String headerName = e.nextElement();
				clientHttpRequest.getHeaders().add(headerName, request.getHeader(headerName));
			}
			clientHttpRequest.getHeaders().add("AAAAAAAAAAAAAAAAAA", "AAAAAAAAAAAAAAAAA");

			logger.info("Copying body");
			IOUtils.copyLarge(request.getInputStream(), clientHttpRequest.getBody());
		};

		ResponseExtractor<ClientHttpResponse> responseExtractor = (final ClientHttpResponse clientHttpResponse) -> {
			logger.info("Writing response back to client");
			response.setStatus(clientHttpResponse.getStatusCode().value());

			BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(clientHttpResponse.getBody()));
			while(bufferedReader.ready()) {
				response.getWriter().write(bufferedReader.readLine()+"\n");
			}
			response.flushBuffer();
			logger.info("Response back to client finished");
			return clientHttpResponse;
		};

		restTemplate.execute(normalizeProxyUrl(proxy.getUrl(), proxyNameAndParameters[1], request),
								HttpMethod.valueOf(request.getMethod()),
								requestCallback,
								responseExtractor);

		logger.info("Called to: {}, time: {}",  request.getRequestURI().toString(), LocalDateTime.now());
		return;
	}

	private String normalizeProxyUrl(String url, String parameters, HttpServletRequest request) {
		UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(url+parameters.replaceAll("//", "/"));

		for(Enumeration<String> e = request.getParameterNames(); e.hasMoreElements();) {
			String parameterName = e.nextElement();
			builder.queryParam(parameterName, request.getParameter(parameterName));
		}

		String encodedUrl = builder.build().encode().toUriString();

		logger.info(encodedUrl);
		return encodedUrl;
	}
}
