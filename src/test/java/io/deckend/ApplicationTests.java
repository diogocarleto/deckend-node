package io.deckend;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.boot.test.WebIntegrationTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import io.deckend.node.Application;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@WebIntegrationTest
/**
 * 
 * @author Diogo Carleto - diogocarleto@gmail.com
 *
 */
public class ApplicationTests {

	@Test
	public void contextLoads() {
	}

}
