package io.deckend.service;

import static io.deckend.node.utils.SocialProvidersEnum.FACEBOOK;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.boot.test.WebIntegrationTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import io.deckend.controller.AbstractRestTests;
import io.deckend.node.Application;
import io.deckend.node.exception.DeckendException;
import io.deckend.node.exception.ErrorMessageEnum;
import io.deckend.node.service.SocialService;

/**
 * @author Diogo Carleto - diogocarleto@gmail.com
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@WebIntegrationTest
public class SocialServiceIntegrationTest extends AbstractRestTests {

	@Autowired
	private SocialService socialService;

	@Test
	public void testAuthenticateInvalidAccessToken() {
		try {
			socialService.authenticateBy(FACEBOOK.getProviderId(), "123");
		} catch (final DeckendException ex) {
			assertEquals(ex.getErrorEnum(), ErrorMessageEnum.NOT_AUTHORIZED_SOCIAL_NETWORK);
		}
	}

	@Test
	public void testAuthenticate() {
		final String accessToken = "CAAL0ypjBVykBAO1uryb3fqu5n06BVEhv3yKt21SMefXGVfnAysZCBjSZCZBVJwuFXIHzqPFKZAsnZCG4EmNSH8hKMk4zCSjFZCbruZC5ZBqlVTp4zDdZACCC6uH5eG4vsXs5KDGeZAU3Js06DShAGwqxCr6qtPjfNc4fJRiS4m5B5OPTySSUu7JJOOx1wGu5jtAsJhQFPCiOADsHRTB1Y68LUr";
    	assertNotNull(socialService.authenticateBy(FACEBOOK.getProviderId(), accessToken));
	}
}
