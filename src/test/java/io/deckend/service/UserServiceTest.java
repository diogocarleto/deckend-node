package io.deckend.service;

import static io.deckend.node.model.User.user;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.boot.test.WebIntegrationTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import io.deckend.node.Application;
import io.deckend.node.infra.RoleEnum;
import io.deckend.node.service.UserService;

/**
 * @author Diogo Carleto - diogocarleto@gmail.com
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@WebIntegrationTest
public class UserServiceTest {

	@Autowired
	private UserService userService;
	
	@Test(expected=Exception.class)
	public void testSignUpUserInvalidUsername() {
		userService.signUp(user("John", "Silva", "", "", RoleEnum.ROLE_USER.value, "johnsilva@gmail.com"));
	}
	
	@Test(expected=Exception.class)
	public void testSignUpInvalidEmail() {
		userService.signUp(user("John", "Silva", "john.silva", "", RoleEnum.ROLE_USER.value, ""));
	}
	
	@Test(expected=Exception.class)
	public void testSignUpWithouPassword() {
		userService.signUp(user("John", "Silva", "john.silva", "", RoleEnum.ROLE_USER.value, "johnsilva@gmail.com"));
	}
}
