package io.deckend.controller;

import static io.deckend.node.utils.AuthEnum.X_AUTH_APP_KEY;
import static io.deckend.node.utils.AuthEnum.X_AUTH_TOKEN;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.springframework.http.HttpMethod.POST;

import java.net.URI;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.boot.test.WebIntegrationTest;
import org.springframework.core.io.ClassPathResource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import com.mongodb.DBObject;
import com.mongodb.util.JSON;

import io.deckend.node.Application;
import io.deckend.node.exception.ErrorMessageEnum;
import io.deckend.node.exception.ErrorMessageInfo;

//import static org.springframework.test.web.servlet.setup.MockMvcBuilders.*;
//import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
//import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * @author Diogo Carleto - diogocarleto@gmail.com
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@WebIntegrationTest
public class FileControllerTest extends AbstractRestTests {

	private String accessToken;
	private final String COLLECTION_NAME = "mycollection";

	@Before
	public void before() {
		accessToken = authenticateRootAndGetToken();
	}

	@Test
	public void uploadFileWithMetadata() throws Exception {
        assertNotNull(uploadFileUtil("application.properties"));
	}

	@Test
	public void downloadFile() {
        String id = uploadFileUtil("application.properties");
        assertNotNull(id);

        RequestEntity<?> request = RequestEntity.get(URI.create(getBaseFileUrl()+"/"+id))
								.header(X_AUTH_APP_KEY.value, appSettings.getKey())
								.header(X_AUTH_TOKEN.value, accessToken)
								.accept(MediaType.APPLICATION_JSON).build();

		ResponseEntity<String> response = new RestTemplate().exchange(request, String.class);
        assertNotNull(response.getBody());
	}

	@Test
	public void downloadUsingChuncks() {
		String id = uploadFileUtil("floripa_089.jpg");
        assertNotNull(id);

        RequestEntity<?> request = RequestEntity.get(URI.create(getBaseFileUrl()+"/"+id))
								.header(X_AUTH_APP_KEY.value, appSettings.getKey())
								.header(X_AUTH_TOKEN.value, accessToken)
								.accept(MediaType.APPLICATION_JSON).build();

		ResponseEntity<String> response = new RestTemplate().exchange(request, String.class);
        assertNotNull(response.getBody());

	}

	@Test(expected=Exception.class)
	public void findInexistentFile() {
        String id = "123";
        RequestEntity<?> request = RequestEntity.get(URI.create(getBaseFileUrl()+"/"+id))
								.header(X_AUTH_APP_KEY.value, appSettings.getKey())
								.header(X_AUTH_TOKEN.value, accessToken)
								.accept(MediaType.APPLICATION_JSON).build();

		new RestTemplate().exchange(request, String.class);
	}

	@Test
	public void uploadFileWithoutMetadata(){
		HttpHeaders headers = new HttpHeaders();
		headers.add(X_AUTH_APP_KEY.value, appSettings.getKey());
		headers.add(X_AUTH_TOKEN.value, accessToken);
		headers.setContentType(MediaType.MULTIPART_FORM_DATA);


		MultiValueMap<String, Object> map = new LinkedMultiValueMap<String, Object>();
        map.add("file", new ClassPathResource("application.properties"));
        map.add("collectionName", COLLECTION_NAME);

        HttpEntity<MultiValueMap<String, Object>> imageEntity = new HttpEntity<MultiValueMap<String, Object>>(map, headers);
        ResponseEntity<String> response = new RestTemplate().exchange(getBaseFileUrl(), POST, imageEntity, String.class);

        DBObject savedFile = (DBObject) JSON.parse(response.getBody());
        assertNotNull(savedFile.get("_id"));
	}

	@Test
	public void deleteFile(){
		String id = uploadFileUtil("application.properties");
        assertNotNull(id);

        RequestEntity<?> request = RequestEntity.delete(URI.create(getBaseFileUrl()+"/"+id))
				.header(X_AUTH_APP_KEY.value, appSettings.getKey())
				.header(X_AUTH_TOKEN.value, accessToken)
				.accept(MediaType.APPLICATION_JSON).build();

        ResponseEntity<ErrorMessageInfo> response = new RestTemplate().exchange(request, ErrorMessageInfo.class);
        assertEquals(response.getBody().code, ErrorMessageEnum.FILE_DELETED_SUCCESFULLY.getCode());
	}

	private String uploadFileUtil(final String file) {
		HttpHeaders headers = new HttpHeaders();
		headers.add(X_AUTH_APP_KEY.value, appSettings.getKey());
		headers.add(X_AUTH_TOKEN.value, accessToken);
		headers.setContentType(MediaType.MULTIPART_FORM_DATA);


		MultiValueMap<String, Object> map = new LinkedMultiValueMap<String, Object>();
        map.add("file", new ClassPathResource(file));
        map.add("collectionName", COLLECTION_NAME);
        map.add("metadata", "{'data': 'test'}");

        HttpEntity<MultiValueMap<String, Object>> imageEntity = new HttpEntity<MultiValueMap<String, Object>>(map, headers);
        ResponseEntity<String> response = new RestTemplate().exchange(getBaseFileUrl(), POST, imageEntity, String.class);

        DBObject savedFile = (DBObject) JSON.parse(response.getBody());
        return (String) savedFile.get("_id");
	}

	/*
	 * TODO - Another way to run upload tests, but scape filters
	 * @Resource
		private WebApplicationContext wac;
	 *
	 * HttpHeaders headers = new HttpHeaders();
		headers.add(X_AUTH_APP_KEY.value, appSettings.getKey());
		headers.add(X_AUTH_TOKEN.value, accessToken);

		MockMvc mockMvc = webAppContextSetup(wac).build();

		mockMvc.perform(fileUpload(getBaseFileUrl())
					.file(mockMultipartFile).headers(headers)
					.param("metadata", "{'data': 'test'}"))
							.andExpect(status().isOk());
	 */
}
