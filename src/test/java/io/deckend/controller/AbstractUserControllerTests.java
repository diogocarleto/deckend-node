package io.deckend.controller;

import java.net.URI;

/**
 * @author Diogo Carleto - diogocarleto@gmail.com
 *
 */
public abstract class AbstractUserControllerTests extends AbstractRestTests {

	protected URI getSignUpURI(final String providerId) {
		StringBuilder url = new StringBuilder(getBaseUserUrl())
								.append("/signUp");
		if(providerId!=null) {
			url.append("/"+providerId);
		}		
		return URI.create(url.toString());
	}
	
	protected URI getSignInURI(final String providerId) {
		StringBuilder url = new StringBuilder(getBaseUserUrl())
								.append("/signIn");
		if(providerId!=null) {
			url.append("/"+providerId);
		}		
		return URI.create(url.toString());
	}
	
	protected URI getUpdateURI() {
		return URI.create(getBaseUserUrl());
	}	
}
