package io.deckend.controller;

import static io.deckend.node.exception.ErrorMessageEnum.DUPLICATED_KEY_ERROR;
import static io.deckend.node.exception.ErrorMessageEnum.INVALID_PROXY;
import static io.deckend.node.exception.ErrorMessageEnum.INVALID_PROXY_ID;
import static io.deckend.node.exception.ErrorMessageEnum.PROXY_DELETED_SUCCESSFULLY;
import static io.deckend.node.model.Proxy.proxy;
import static io.deckend.node.utils.AuthEnum.X_AUTH_APP_KEY;
import static io.deckend.node.utils.AuthEnum.X_AUTH_TOKEN;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.junit.Assert.assertThat;

import java.net.URI;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.boot.test.WebIntegrationTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import io.deckend.node.Application;
import io.deckend.node.exception.ErrorMessageInfo;
import io.deckend.node.model.Proxy;

/**
 * @author Diogo Carleto - diogocarleto@gmail.com
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@WebIntegrationTest
public class ProxyControllerTests extends AbstractRestTests {

	protected String accessToken;
	
	@Before
	public void before() {
		accessToken = authenticateRootAndGetToken();
	}
	
	@Test
	public void createInvalidProxy() {
		ErrorMessageInfo errorMessageInfo = null;
		try {
			createProxy(accessToken, proxy(false, "", ""));
		} catch(HttpClientErrorException ex) {
			errorMessageInfo = extractDetailsException(ex);
		}
		assertThat(errorMessageInfo.code, is(INVALID_PROXY.code));
	}
	
	@Test
	public void create() {
		Proxy proxy = createProxy(accessToken, proxy(false, "external_app", "http://external_app.com")).getBody();
		assertThat(proxy.getId(), notNullValue());
	}
	
	@Test
	public void createWithoutPermissions(){
		accessToken = authenticate("dtest", "123", appSettings.getKey());
		try {
			createProxy(accessToken, proxy(false, "external_app1", "http://external_app1.com"));
		} catch(HttpClientErrorException ex) {
			assertThat(ex.getStatusCode(), is(HttpStatus.FORBIDDEN));
		}
	}
	
	@Test
	public void createDuplicatedProxy() {
		ErrorMessageInfo errorMessageInfo = null;
		try {
			createProxy(accessToken, proxy(false, "external_app3", "http://external_app3.com"));
			createProxy(accessToken, proxy(false, "external_app3", "http://external_app3.com"));
		} catch(HttpClientErrorException ex) {
			errorMessageInfo = extractDetailsException(ex);
		}
		assertThat(errorMessageInfo.code, is(DUPLICATED_KEY_ERROR.code));
	}
	
	@Test
	public void update() {
		Proxy proxy = createProxy(accessToken, proxy(false, "external_app4", "http://external_app4.com")).getBody();
		proxy.setName("external_app4_");
		proxy = updateProxy(accessToken, proxy).getBody();
		
		assertThat(proxy.getName(), is("external_app4_"));
	}
	
	@Test
	public void updateInexistentProxy() {
		ErrorMessageInfo errorMessageInfo = null;
		try {
			updateProxy(accessToken, proxy(false, "external_app4", "http://external_app4.com"));
		} catch(HttpClientErrorException ex) {
			errorMessageInfo = extractDetailsException(ex);
		}
		assertThat(errorMessageInfo.code, is(INVALID_PROXY_ID.code));
	}
	
	@Test
	public void delete() { 
		Proxy proxy = createProxy(accessToken, proxy(false, "external_app5", "http://external_app5.com")).getBody();
		ErrorMessageInfo errorMessageInfo = deleteProxy(accessToken, proxy.getId()).getBody();
		assertThat(errorMessageInfo.code, is(PROXY_DELETED_SUCCESSFULLY.code));
	}
	
	protected ResponseEntity<Proxy> createProxy(final String accessToken, final Proxy proxy) {
		RequestEntity<Proxy> request = RequestEntity.post(getBaseProxyURI())
				.header(X_AUTH_APP_KEY.value, appSettings.getKey())
				.header(X_AUTH_TOKEN.value, accessToken)
				.accept(MediaType.APPLICATION_JSON)
				.body(proxy);
		return new RestTemplate().exchange(request, Proxy.class);
	}
	
	protected ResponseEntity<Proxy> updateProxy(final String accessToken, final Proxy proxy) {
		RequestEntity<Proxy> request = RequestEntity.put(getBaseProxyURI())
				.header(X_AUTH_APP_KEY.value, appSettings.getKey())
				.header(X_AUTH_TOKEN.value, accessToken)
				.accept(MediaType.APPLICATION_JSON)
				.body(proxy);
		return new RestTemplate().exchange(request, Proxy.class);
	}
	
	protected ResponseEntity<ErrorMessageInfo> deleteProxy(final String accessToken, final String name) {
		RequestEntity<?> request = RequestEntity.delete(getProxyURIToDelete(name))
				.header(X_AUTH_APP_KEY.value, appSettings.getKey())
				.header(X_AUTH_TOKEN.value, accessToken)
				.accept(MediaType.APPLICATION_JSON)
				.build();
		return new RestTemplate().exchange(request, ErrorMessageInfo.class);
	}
	
	private URI getProxyURIToDelete(final String name) {
		return URI.create(getBaseUrl()+"/proxy/"+name);
	}
	
	private URI getBaseProxyURI() {
		return URI.create(getBaseUrl()+"/proxy");
	}
}
