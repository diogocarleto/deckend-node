package io.deckend.controller;

import static io.deckend.node.infra.RoleEnum.ROLE_ADMIN;
import static io.deckend.node.infra.RoleEnum.ROLE_USER;
import static io.deckend.node.model.User.user;
import static io.deckend.node.utils.AuthEnum.X_AUTH_APP_KEY;
import static io.deckend.node.utils.AuthEnum.X_AUTH_TOKEN;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.net.URI;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.boot.test.WebIntegrationTest;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import io.deckend.node.Application;
import io.deckend.node.exception.ErrorMessageEnum;
import io.deckend.node.exception.ErrorMessageInfo;
import io.deckend.node.infra.RoleEnum;
import io.deckend.node.model.User;

/**
 * @author Diogo Carleto - diogocarleto@gmail.com
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@WebIntegrationTest
public class UserControllerTest extends AbstractUserControllerTests {
	
	@Test
	public void signUpInvalidAppKey() {
		try {
			final RequestEntity<User> request = RequestEntity.post(getSignUpURI("bazinga"))
					.accept(MediaType.APPLICATION_JSON).body(new User());
	
			new RestTemplate().exchange(request, User.class);
		} catch (final HttpClientErrorException ex) {
			assertEquals(ex.getStatusCode(), HttpStatus.UNAUTHORIZED);
		}
	}
	
	@Test
	public void signUpInvalidUsername() {
		ErrorMessageInfo errorInfo = null;
		try {
			User user = user("diogo", "carleto", null, "123", ROLE_USER.value, "diogocarleto1@gmail.com");
			
		    RequestEntity<User> request = RequestEntity.post(getSignUpURI(null))
		    							.header(X_AUTH_APP_KEY.value, appSettings.getKey())
		    							.accept(MediaType.APPLICATION_JSON).body(user);
		    
			new RestTemplate().exchange(request, User.class);
		} catch (final HttpClientErrorException ex) {
			errorInfo = extractDetailsException(ex);			
		}
		assertEquals(errorInfo.code, ErrorMessageEnum.INVALID_USERNAME.code);
	}
	
	@Test
	public void signUp() {
		User user = user("diogo", "carleto", "diogocarleto2", "123", ROLE_USER.value, "diogocarleto2@gmail.com");
		
	    RequestEntity<User> request = RequestEntity.post(getSignUpURI(null))
	    							.header(X_AUTH_APP_KEY.value, appSettings.getKey())
	    							.accept(MediaType.APPLICATION_JSON).body(user);
	    
		ResponseEntity<User> response = new RestTemplate().exchange(request, User.class);
		assertNotNull(response.getBody());
	}
	
	@Test
	public void signUpWithRoleAdminWithPermissions() {
		String token = authenticateRootAndGetToken();		
		User user = user("diogo", "carleto", "diogocarleto1", "123", ROLE_ADMIN.value, "diogocarleto1@gmail.com");
		
	    RequestEntity<User> request = RequestEntity.post(getSignUpURI(null))
	    							.header(X_AUTH_APP_KEY.value, appSettings.getKey())
	    							.header(X_AUTH_TOKEN.value, token)
	    							.accept(MediaType.APPLICATION_JSON).body(user);
	    
		ResponseEntity<User> response = new RestTemplate().exchange(request, User.class);
		assertNotNull(response.getBody());
	}	
	
	@Test
	public void deleteUser() {
		User user = user("Joao","Silva", "joao123", "123", RoleEnum.ROLE_USER.value, "joao123@gmail.com");
		
		RequestEntity<User> request = RequestEntity.post(getSignUpURI(""))
				.header(X_AUTH_APP_KEY.value, appSettings.getKey())
				.accept(MediaType.APPLICATION_JSON).body(user);		
		ResponseEntity<User> response = new RestTemplate().exchange(request, User.class);		
		String token = response.getHeaders().get(X_AUTH_TOKEN.value).get(0);
		assertNotNull(token);
		
		request = new RequestEntity<>(buildXAuthHeaders(appSettings.getKey(), token), 
				HttpMethod.DELETE, 
				URI.create(getBaseUserUrl()));	
		ResponseEntity<ErrorMessageInfo> responseEMI = new RestTemplate().exchange(request, ErrorMessageInfo.class);
		assertEquals(responseEMI.getBody().code, ErrorMessageEnum.USER_DELETED_SUCCESSFULLY.code);				
	}
	
	@Test
	public void signUpWithPhoneNumber() {
		User user = user("number", "555", "95555-5555", "123", ROLE_USER.value, "number555@gmail.com");
		
	    RequestEntity<User> request = RequestEntity.post(getSignUpURI(null))
	    							.header(X_AUTH_APP_KEY.value, appSettings.getKey())
	    							.accept(MediaType.APPLICATION_JSON).body(user);
	    
		ResponseEntity<User> response = new RestTemplate().exchange(request, User.class);
		assertNotNull(response.getBody());
	}
	
	@Test
	public void signUpDuplicatedPhoneNumber() {
		ErrorMessageInfo errorMessageInfo = null;
		try {
			String phoneNumber = "96555-5556";
			User user = user("number", "556", phoneNumber, "123", ROLE_USER.value, "number655@gmail.com");
			
		    RequestEntity<User> request = RequestEntity.post(getSignUpURI(null))
		    							.header(X_AUTH_APP_KEY.value, appSettings.getKey())
		    							.accept(MediaType.APPLICATION_JSON).body(user);
		    
			ResponseEntity<User> response = new RestTemplate().exchange(request, User.class);
			assertNotNull(response.getBody());
			
			user("number", "555", phoneNumber, "123", ROLE_USER.value, "number655@gmail.com");
		    request = RequestEntity.post(getSignUpURI(null))
		    							.header(X_AUTH_APP_KEY.value, appSettings.getKey())
		    							.accept(MediaType.APPLICATION_JSON).body(user);
		    
			new RestTemplate().exchange(request, User.class);
		} catch (HttpClientErrorException ex) {
			errorMessageInfo = extractDetailsException(ex);
		}
		
		assertEquals(errorMessageInfo.code, ErrorMessageEnum.DUPLICATED_KEY_ERROR.code); 
	}
	
	@Test
	public void getMe() {
		String token = authenticateRootAndGetToken();
		RequestEntity<?> request = RequestEntity.get(URI.create(getBaseUserUrl()))
				.header(X_AUTH_APP_KEY.value, appSettings.getKey())
				.header(X_AUTH_TOKEN.value, token)
				.accept(MediaType.APPLICATION_JSON).build();
		
		ResponseEntity<User> response = new RestTemplate().exchange(request, User.class);
		assertNotNull(response.getBody());
	}
}
