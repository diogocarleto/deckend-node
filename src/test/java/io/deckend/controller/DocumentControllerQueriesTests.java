package io.deckend.controller;

import static io.deckend.node.utils.AuthEnum.X_AUTH_APP_KEY;
import static io.deckend.node.utils.AuthEnum.X_AUTH_TOKEN;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.boot.test.WebIntegrationTest;
import org.springframework.http.MediaType;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.web.client.RestTemplate;

import com.mongodb.BasicDBList;
import com.mongodb.DBObject;
import com.mongodb.util.JSON;

import io.deckend.node.Application;

/**
 * @author Diogo Carleto - diogocarleto@gmail.com
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@WebIntegrationTest
public class DocumentControllerQueriesTests extends DocumentControllerBaseTests {

	@Before
	public void before() {
		accessToken = authenticateRootAndGetToken();
		RequestEntity<?> request = RequestEntity.post(getBaseCollectionURI("/"+COLLECTION_NAME))
					.header(X_AUTH_APP_KEY.value, appSettings.getKey())
					.header(X_AUTH_TOKEN.value, accessToken)
					.accept(MediaType.APPLICATION_JSON)
					.build();
		new RestTemplate().exchange(request, String.class);
	}

	@After
	public void after() {
		RequestEntity<?> request = RequestEntity.delete(getBaseCollectionURI("/"+COLLECTION_NAME))
				.header(X_AUTH_APP_KEY.value, appSettings.getKey())
				.header(X_AUTH_TOKEN.value, accessToken)
				.accept(MediaType.APPLICATION_JSON)
				.build();
		new RestTemplate().exchange(request, String.class);
	}

	@Test
	public void findAddedByUserRoleAdmin() {
		String json = "{ 'title': 'I am a title', 'body': 'I am a body'}";
		ResponseEntity<String> response = createDocument(COLLECTION_NAME, json);
		createDocument(COLLECTION_NAME, json);

		String query = "{'title': 'I am a title'}";
		response = findDocumentByQueryUtil(accessToken, query);

		BasicDBList dbObjects = (BasicDBList) JSON.parse(response.getBody());
		assertTrue(dbObjects.size() == 2);
	}

	@Test
	public void findAddedByRoleUser() {
		accessToken = authenticateDTest();

		String json = "{ 'title': 'I am a title', 'body': 'I am a body'}";
		ResponseEntity<String> response = createDocument(accessToken, COLLECTION_NAME, json);

		String query = "{'title': 'I am a title'}";
		response = findDocumentByQueryUtil(accessToken, query);
		BasicDBList dbObjects = (BasicDBList) JSON.parse(response.getBody());
		assertTrue(dbObjects.size() == 1);
	}

	@Test
	public void findWithComplexQueryAddedByUserRoleAdmin() {
		String json = "{ 'title': 'I am a title', 'body': 'I am a body', 'reviews': '15'}";
		createDocument(COLLECTION_NAME, json);

		String query = "{ 'reviews': { '$gt': '10' } }";
		ResponseEntity<String> response = findDocumentByQueryUtil(accessToken, query);

		BasicDBList dbObjects = (BasicDBList) JSON.parse(response.getBody());
		assertNotNull(((DBObject) dbObjects.get(0)).containsField("reviews"));
	}

	@Test
	public void findWithAndAndOperatorAddedByUserRoleAdmin() {
		String json = "{ 'title': 'I am a title', 'body': 'I am a body', 'reviews': '15'}";
		createDocument(COLLECTION_NAME, json);

		String query = "{ 'reviews': { '$gt': '10' }, 'body': 'I am a body'}";
		ResponseEntity<String> response = findDocumentByQueryUtil(accessToken, query);

		BasicDBList dbObjects = (BasicDBList) JSON.parse(response.getBody());
		assertNotNull(((DBObject) dbObjects.get(0)).containsField("reviews"));
	}

	@Test
	public void findWithOrOperatorAddedByUserRoleAdmin() {
		String json = "{ 'title': 'I am a title', 'body': 'I am a body', 'reviews': '15'}";
		createDocument(COLLECTION_NAME, json);

		String query = "{ '$or': [ {'reviews': '15'}, {'body': 'I am a body'} ] }";
		ResponseEntity<String> response = findDocumentByQueryUtil(accessToken, query);

		BasicDBList dbObjects = (BasicDBList) JSON.parse(response.getBody());
		assertNotNull(((DBObject) dbObjects.get(0)).containsField("reviews"));
	}

	@Test
	public void findWithOrOperatorFirstMatchAddedByUserRoleAdmin() {
		String json = "{ 'title': 'I am a title', 'body': 'I am a body', 'reviews': '15'}";
		createDocument(COLLECTION_NAME, json);

		String query = "{ '$or': [ {'reviews': '15'}, {'body': 'I am a body1'} ] }";
		ResponseEntity<String> response = findDocumentByQueryUtil(accessToken, query);

		BasicDBList dbObjects = (BasicDBList) JSON.parse(response.getBody());
		assertNotNull(((DBObject) dbObjects.get(0)).containsField("reviews"));
	}

	@Test
	public void findWithOrOperatorSecondMatchAddedByUserRoleAdmin() {
		String json = "{ 'title': 'I am a title', 'body': 'I am a body', 'reviews': '15'}";
		createDocument(COLLECTION_NAME, json);

		String query = "{ '$or': [ {'reviews': '10'}, {'body': 'I am a body'} ] }";
		ResponseEntity<String> response = findDocumentByQueryUtil(accessToken, query);

		BasicDBList dbObjects = (BasicDBList) JSON.parse(response.getBody());
		assertNotNull(((DBObject) dbObjects.get(0)).containsField("reviews"));
	}

	@Test
	public void findWithOrOperatorNoMatchAddedByUserRoleAdmin() {
		String json = "{ 'title': 'I am a title', 'body': 'I am a body', 'reviews': '15'}";
		createDocument(COLLECTION_NAME, json);

		String query = "{ '$or': [ {'reviews': '11'}, {'body': 'I am a body1'} ] }";
		ResponseEntity<String> response = findDocumentByQueryUtil(accessToken, query);

		BasicDBList dbObjects = (BasicDBList) JSON.parse(response.getBody());
		assertEquals(dbObjects.size(), 0);
	}

	@Test
	public void findWithOrOperatorFirstMatchGtAddedByUserRoleAdmin() {
		String json = "{ 'title': 'I am a title', 'body': 'I am a body', 'reviews': '15'}";
		createDocument(COLLECTION_NAME, json);

		String query = "{ '$or': [ {'reviews': { '$gt': '10' } }, {'body': 'I am a body1'} ] }";
		ResponseEntity<String> response = findDocumentByQueryUtil(accessToken, query);

		BasicDBList dbObjects = (BasicDBList) JSON.parse(response.getBody());
		assertNotNull(((DBObject) dbObjects.get(0)).containsField("reviews"));
	}

	@Test
	public void findByIdAddedByUserRoleAdmin() {
		String json = "{ 'title': 'I am a title', 'body': 'I am a body'}";
		ResponseEntity<String> response = createDocument(COLLECTION_NAME, json);
		DBObject savedObj = (DBObject) JSON.parse(response.getBody());

		response = findDocumentByIdUtil(accessToken, COLLECTION_NAME, (String) savedObj.get("_id"));
		assertNotNull(response.getBody());
	}

	@Test
	public void findByIdAddedByUserRoleUser() {
		accessToken = authenticateDTest();
		String json = "{ 'title': 'I am a title', 'body': 'I am a body'}";
		ResponseEntity<String> response = createDocument(COLLECTION_NAME, json);
		DBObject savedObj = (DBObject) JSON.parse(response.getBody());

		response = findDocumentByIdUtil(accessToken, COLLECTION_NAME, (String) savedObj.get("_id"));
		assertNotNull(response.getBody());
	}
}
