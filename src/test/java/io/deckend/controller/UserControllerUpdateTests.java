package io.deckend.controller;

import static io.deckend.node.model.User.user;
import static io.deckend.node.utils.AuthEnum.X_AUTH_APP_KEY;
import static io.deckend.node.utils.AuthEnum.X_AUTH_TOKEN;
import static org.junit.Assert.assertEquals;

import java.net.URI;
import java.util.Map;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.boot.test.WebIntegrationTest;
import org.springframework.http.MediaType;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import io.deckend.node.Application;
import io.deckend.node.exception.ErrorMessageEnum;
import io.deckend.node.exception.ErrorMessageInfo;
import io.deckend.node.infra.RoleEnum;
import io.deckend.node.model.User;

/**
 * @author Diogo Carleto - diogocarleto@gmail.com
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@WebIntegrationTest
public class UserControllerUpdateTests extends AbstractUserControllerTests {
	
	@Test
	public void changePassword() {
		String token = authenticateRootAndGetToken();	
		
	    RequestEntity<String> request = RequestEntity.post(URI.create(getBaseUserUrl()+"/changePassword?newPassword=456"))
	    							.header(X_AUTH_APP_KEY.value, appSettings.getKey())
	    							.header(X_AUTH_TOKEN.value, token)
	    							.accept(MediaType.APPLICATION_JSON).body("");
	    
		ResponseEntity<ErrorMessageInfo> response = new RestTemplate().exchange(request, ErrorMessageInfo.class);
		assertEquals(response.getBody().code, ErrorMessageEnum.PASSWORD_CHANGED_SUCCESSFULLY.getCode());
		
		token = authenticate("root","456","123");
		request = RequestEntity.post(URI.create(getBaseUserUrl()+"/changePassword?newPassword=123"))
				.header(X_AUTH_APP_KEY.value, appSettings.getKey())
				.header(X_AUTH_TOKEN.value, token)
				.accept(MediaType.APPLICATION_JSON).body("");
		new RestTemplate().exchange(request, ErrorMessageInfo.class);
	}	
	
	@Test
	public void updateUser() {
		Map<String, Object> mapResponse = authenticateRoot();
		
		User user = (User) mapResponse.get("USER"); 
		user.setLastName("lastNameUpdated");
		
		RequestEntity<User> request = RequestEntity.put(getUpdateURI())
				.header(X_AUTH_APP_KEY.value, appSettings.getKey())
				.header(X_AUTH_TOKEN.value, (String) mapResponse.get(X_AUTH_TOKEN.value))
				.accept(MediaType.APPLICATION_JSON).body(user);
	    
		ResponseEntity<User> response = new RestTemplate().exchange(request, User.class);
		assertEquals(response.getBody().getLastName(), user.getLastName());
	}	
	
	@Test
	public void updateUserLoggedAnotherUser() {
		ErrorMessageInfo errorInfo = null;
		try {
			
			Map<String, Object> mapResponse = authenticateRoot();
			User user = (User) mapResponse.get("USER");
			user.setVersion(2l);
			
			RequestEntity<User> request = RequestEntity.put(getUpdateURI())
					.header(X_AUTH_APP_KEY.value, appSettings.getKey())
					.header(X_AUTH_TOKEN.value, (String) mapResponse.get(X_AUTH_TOKEN.value))
					.accept(MediaType.APPLICATION_JSON).body(user);
		    
			new RestTemplate().exchange(request, User.class);
		} catch (final HttpClientErrorException ex) {
			errorInfo = extractDetailsException(ex);			
		}
		assertEquals(errorInfo.code, ErrorMessageEnum.CANNOT_UPDATE_DIFERENT_USER.getCode());
	}	
	
	@Test(expected=Exception.class)
	public void updateUserWithoutVersion() {
		String token = authenticateRootAndGetToken();		
		
		User user = user("Joao","Silva", "root", "123", RoleEnum.ROLE_SYSADMIN.value);		
		
		RequestEntity<User> request = RequestEntity.put(getUpdateURI())
				.header(X_AUTH_APP_KEY.value, appSettings.getKey())
				.header(X_AUTH_TOKEN.value, token)
				.accept(MediaType.APPLICATION_JSON).body(user);
	    
		new RestTemplate().exchange(request, User.class);		
	}	
	
	@Test
	public void shouldNotUpdateRole() {	}
}
