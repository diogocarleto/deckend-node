package io.deckend.controller;

import static com.eclipsesource.json.JsonObject.readFrom;
import static io.deckend.node.exception.ErrorMessageInfo.messageInfo;
import static io.deckend.node.utils.AuthEnum.X_AUTH_APP_KEY;
import static io.deckend.node.utils.AuthEnum.X_AUTH_APP_SYSTEM;
import static io.deckend.node.utils.AuthEnum.X_AUTH_PASSWORD;
import static io.deckend.node.utils.AuthEnum.X_AUTH_TOKEN;
import static io.deckend.node.utils.AuthEnum.X_AUTH_USERNAME;

import java.net.URI;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import io.deckend.node.config.settings.AppSettings;
import io.deckend.node.config.settings.UserAdministratorSettings;
import io.deckend.node.exception.ErrorMessageInfo;
import io.deckend.node.model.User;

/**
 * @author Diogo Carleto - diogocarleto@gmail.com
 *
 */
public abstract class AbstractRestTests {

	@Value("${server.address}")
	private String serverUrl;

	@Value("${server.port}")
	private String serverPort;

	@Resource
	protected UserAdministratorSettings userAdministratorSettings;

	@Resource
	protected AppSettings appSettings;

	protected HttpHeaders buildXAuthHeaders(final String username, final String password, final String appKey) {
		Map<String, String> headerMap = new HashMap<>();
		headerMap.put(X_AUTH_APP_KEY.value, Optional.ofNullable(appKey).get());
		headerMap.put(X_AUTH_USERNAME.value, Optional.ofNullable(username).get());
		headerMap.put(X_AUTH_PASSWORD.value, Optional.ofNullable(password).get());

		return buildHeaders(headerMap);
	}

	protected HttpHeaders buildXAuthHeaders(final String appKey, final String token) {
		Map<String, String> headerMap = new HashMap<>();
		headerMap.put(X_AUTH_APP_KEY.value, Optional.ofNullable(appKey).get());
		headerMap.put(X_AUTH_TOKEN.value, Optional.ofNullable(token).get());

		return buildHeaders(headerMap);
	}

	protected HttpHeaders buildHeaders(final Map<String, String> headerMap) {
		HttpHeaders headers = new HttpHeaders();
		headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
		headerMap.entrySet().stream().forEach((header) -> headers.add(header.getKey(), header.getValue()));
		return headers;
	}

	protected HttpHeaders buildXAuthHeaders(final String appKey) {
		final HttpHeaders headers = new HttpHeaders();
	    headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
	    headers.add(X_AUTH_APP_KEY.value, Optional.ofNullable(appKey).get());

		return headers;
	}

	protected String authenticateRootAndGetToken() {
		Map<String, Object> mapResponse = authenticateRoot();
		return (String) mapResponse.get(X_AUTH_TOKEN.value);
	}

	/**
	 * Return a map containing X_AUTH_TOKEN and user values.
	 * The keys are:
	 * X_AUTH_TOKEN
	 * USER
	 * @return
	 */
	protected Map<String, Object> authenticateRoot() {
		RequestEntity<User> request = RequestEntity.post(URI.create(getSignInUrl()))
				.header(X_AUTH_APP_KEY.value, appSettings.getKey())
				.header(X_AUTH_USERNAME.value, userAdministratorSettings.getUsername())
				.header(X_AUTH_PASSWORD.value, userAdministratorSettings.getPassword())
				.accept(MediaType.APPLICATION_JSON).body(new User());

		ResponseEntity<User> response = new RestTemplate().exchange(request, User.class);
		Map<String, Object> mapResponse = new HashMap<>();

		mapResponse.put(X_AUTH_TOKEN.value, response.getHeaders().get(X_AUTH_TOKEN.value).get(0));
		mapResponse.put("USER", response.getBody());
		return mapResponse;
	}

	protected String authenticateSysUserAndGetToken() {
		RequestEntity<User> request = RequestEntity.post(URI.create(getSignInUrl()))
				.header(X_AUTH_APP_KEY.value, "123")
				.header(X_AUTH_USERNAME.value, "sa")
				.header(X_AUTH_PASSWORD.value, "123")
				.header(X_AUTH_APP_SYSTEM.value, "xpto")
				.accept(MediaType.APPLICATION_JSON).body(new User());

		ResponseEntity<User> response = new RestTemplate().exchange(request, User.class);
		return response.getHeaders().get(X_AUTH_TOKEN.value).get(0);
	}

	protected String authenticate(final String username, final String password, final String appKey) {
		RestTemplate restTemplate = new RestTemplate();
		HttpEntity<String> entity = new HttpEntity<String>("parameters", buildXAuthHeaders(username, password, appKey));
		ResponseEntity<User> response = restTemplate.exchange(URI.create(getSignInUrl()), HttpMethod.POST, entity, User.class);

		return response.getHeaders().get(X_AUTH_TOKEN.value).get(0);
	}

	/**
	 * Convert the exception in an ErrorInfo.
	 * @param ex
	 * @return ErrorInfo
	 */
	protected ErrorMessageInfo extractDetailsException(final HttpClientErrorException ex) {
		String json = ex.getResponseBodyAsString();
		return messageInfo(readFrom(json).get("code").asInt(), readFrom(json).get("message").asString());
	}

	protected String getSignInUrl() {
		return new StringBuilder(getBaseUserUrl()).append("/signIn").toString();
	}

	protected String getBaseUrl() {
		return new StringBuilder("http://").append(serverUrl).append(":").append(serverPort).toString();
	}

	protected String getBaseUserUrl() {
		return getBaseUrl()+"/user";
	}

	protected URI getBaseCollectionURI(final String append) {
		return URI.create(getBaseUrl()+"/collection"+append);
	}

	public String getBaseFileUrl() {
		return getBaseUrl()+"/file";
	}

}
