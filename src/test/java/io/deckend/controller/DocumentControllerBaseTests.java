package io.deckend.controller;

import static io.deckend.node.utils.AuthEnum.X_AUTH_APP_KEY;
import static io.deckend.node.utils.AuthEnum.X_AUTH_TOKEN;

import java.net.URI;

import org.springframework.http.MediaType;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import io.deckend.node.exception.ErrorMessageInfo;

/**
 * @author Diogo Carleto - diogocarleto@gmail.com
 *
 */
public class DocumentControllerBaseTests extends AbstractRestTests {

	protected String accessToken;
	protected final String COLLECTION_NAME = "mycollection";

	protected ResponseEntity<String> createDocument(final String collectionName, final String json) {
		return createDocument(accessToken, collectionName, json);
	}

	protected ResponseEntity<String> createDocument(final String accessToken, final String collectionName, final String json) {
		RequestEntity<String> request = RequestEntity.post(getBaseDocumentURI(collectionName))
				.header(X_AUTH_APP_KEY.value, appSettings.getKey())
				.header(X_AUTH_TOKEN.value, accessToken)
				.accept(MediaType.APPLICATION_JSON)
				.body(json);
		return new RestTemplate().exchange(request, String.class);
	}

	protected ResponseEntity<String> updatePartialDocument(final String accessToken,
														final String collectionName,
														final String id,
														final String json) {
		RequestEntity<String> request = RequestEntity.put(URI.create(getBaseDocument(collectionName)+"/"+id))
				.header(X_AUTH_APP_KEY.value, appSettings.getKey())
				.header(X_AUTH_TOKEN.value, accessToken)
				.accept(MediaType.APPLICATION_JSON)
				.body(json);
		return new RestTemplate().exchange(request, String.class);
	}

	protected ResponseEntity<String> updateDocument(final String accessToken, final String collectionName, final String json) {
		RequestEntity<String> request = RequestEntity.put(getBaseDocumentURI(collectionName))
				.header(X_AUTH_APP_KEY.value, appSettings.getKey())
				.header(X_AUTH_TOKEN.value, accessToken)
				.accept(MediaType.APPLICATION_JSON)
				.body(json);
		return new RestTemplate().exchange(request, String.class);
	}

	protected ResponseEntity<ErrorMessageInfo> deleteDocument(final String collectionName, final String id) {
		RequestEntity<?> request = RequestEntity.delete(URI.create(getBaseDocument(collectionName)+"/"+id))
				.header(X_AUTH_APP_KEY.value, appSettings.getKey())
				.header(X_AUTH_TOKEN.value, accessToken)
				.accept(MediaType.APPLICATION_JSON)
				.build();
		return new RestTemplate().exchange(request, ErrorMessageInfo.class);
	}

	protected ResponseEntity<String> findDocumentByIdUtil(final String accessToken, String collectionName, String id) {
		RequestEntity<?> request = RequestEntity.get(URI.create(getBaseDocument(collectionName)+"/find"+"/"+id))
				.header(X_AUTH_APP_KEY.value, appSettings.getKey())
				.header(X_AUTH_TOKEN.value, accessToken)
				.accept(MediaType.APPLICATION_JSON)
				.build();

		return new RestTemplate().exchange(request, String.class);
	}

	protected ResponseEntity<String> findDocumentByQueryUtil(final String accessToken, final String query) {
		RequestEntity<String> request = RequestEntity.post(URI.create(getBaseFindDocumentUrl(COLLECTION_NAME)))
				.header(X_AUTH_APP_KEY.value, appSettings.getKey())
				.header(X_AUTH_TOKEN.value, accessToken)
				.accept(MediaType.APPLICATION_JSON)
				.body(query);

		return new RestTemplate().exchange(request, String.class);
	}

	protected String authenticateDTest() {
		return authenticate("dtest", "123", appSettings.getKey());
	}

	protected URI getBaseDocumentURI(final String collectionName) {
		return URI.create(getBaseDocument(collectionName));
	}

	protected String getBaseDocument(final String collectionName) {
		return getBaseUrl()+"/document/"+collectionName;
	}

	protected String getBaseFindDocumentUrl(final String collectionName) {
		return getBaseDocument(collectionName)+"/find";
	}
}
