package io.deckend.controller;

import static io.deckend.node.infra.RoleEnum.ROLE_USER;
import static io.deckend.node.model.User.user;
import static io.deckend.node.utils.AuthEnum.X_AUTH_APP_KEY;
import static io.deckend.node.utils.AuthEnum.X_AUTH_EXTERNAL_TOKEN;
import static io.deckend.node.utils.AuthEnum.X_AUTH_TOKEN;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.net.URI;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.boot.test.WebIntegrationTest;
import org.springframework.http.MediaType;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import io.deckend.node.Application;
import io.deckend.node.exception.ErrorMessageEnum;
import io.deckend.node.exception.ErrorMessageInfo;
import io.deckend.node.model.User;

/**
 * @author Diogo Carleto - diogocarleto@gmail.com
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@WebIntegrationTest
public class UserControllerSocialTests extends AbstractUserControllerTests {

	@Test
	public void signUpSocialWhitoutExternalToken() {
		ErrorMessageInfo errorInfo = null;
		try {
		    RequestEntity<User> request = RequestEntity.post(getSignUpURI("facebook"))
		    							.header(X_AUTH_APP_KEY.value, appSettings.getKey())
		    							.accept(MediaType.APPLICATION_JSON).body(new User());
		    
			new RestTemplate().exchange(request, User.class);
		} catch (final HttpClientErrorException ex) {
			errorInfo = extractDetailsException(ex);			
		}
		assertEquals(errorInfo.code, ErrorMessageEnum.INVALID_TOKEN.code);
	}
	
	@Test
	public void signUpSocialInvalidProviderId() {
		ErrorMessageInfo errorInfo = null;
		try {
		    RequestEntity<User> request = RequestEntity.post(getSignUpURI("bazinga"))
		    							.header(X_AUTH_APP_KEY.value, appSettings.getKey())
		    							.header(X_AUTH_EXTERNAL_TOKEN.value, "123")
		    							.accept(MediaType.APPLICATION_JSON).body(new User());
		    
			new RestTemplate().exchange(request, User.class);
		} catch (final HttpClientErrorException ex) {
			errorInfo = extractDetailsException(ex);			
		}
		assertEquals(errorInfo.code, ErrorMessageEnum.PROVIDER_ID_NOT_SUPPORTED.code);
	}
	
	//TODO - see how to implement
	public void signUpSocialExpiredExternalToken(){}
	
	@Test
	public void signUpSocialAndSignIn(){
		String accessToken = "CAAL0ypjBVykBAO1uryb3fqu5n06BVEhv3yKt21SMefXGVfnAysZCBjSZCZBVJwuFXIHzqPFKZAsnZCG4EmNSH8hKMk4zCSjFZCbruZC5ZBqlVTp4zDdZACCC6uH5eG4vsXs5KDGeZAU3Js06DShAGwqxCr6qtPjfNc4fJRiS4m5B5OPTySSUu7JJOOx1wGu5jtAsJhQFPCiOADsHRTB1Y68LUr";
    	
		RequestEntity<User> request = RequestEntity.post(getSignUpURI("facebook"))
				.header(X_AUTH_APP_KEY.value, appSettings.getKey())
				.header(X_AUTH_EXTERNAL_TOKEN.value, accessToken)
				.accept(MediaType.APPLICATION_JSON).body(new User());
		ResponseEntity<User> response = new RestTemplate().exchange(request, User.class);
		
		assertNotNull(response.getBody());
		assertNotNull(response.getHeaders().get(X_AUTH_TOKEN.value));
		
		request = RequestEntity.post(getSignInURI("facebook"))
				.header(X_AUTH_APP_KEY.value, appSettings.getKey())
				.header(X_AUTH_EXTERNAL_TOKEN.value, accessToken)
				.accept(MediaType.APPLICATION_JSON).body(new User());
		response = new RestTemplate().exchange(request, User.class);
		
		assertNotNull(response.getBody());
	}

	//TODO - see how to guarantee that this test runs after the one above
//		@Test
	public void signUpSocialUsernameAlreadyExists() {
		ErrorMessageInfo errorInfo = null;
		try {
			String accessToken = "CAAL0ypjBVykBAO1uryb3fqu5n06BVEhv3yKt21SMefXGVfnAysZCBjSZCZBVJwuFXIHzqPFKZAsnZCG4EmNSH8hKMk4zCSjFZCbruZC5ZBqlVTp4zDdZACCC6uH5eG4vsXs5KDGeZAU3Js06DShAGwqxCr6qtPjfNc4fJRiS4m5B5OPTySSUu7JJOOx1wGu5jtAsJhQFPCiOADsHRTB1Y68LUr";
	    	
			RequestEntity<User> request = RequestEntity.post(getSignUpURI("facebook"))
					.header(X_AUTH_APP_KEY.value, appSettings.getKey())
					.header(X_AUTH_EXTERNAL_TOKEN.value, accessToken)
					.accept(MediaType.APPLICATION_JSON).body(new User());
			new RestTemplate().exchange(request, User.class);
		} catch (final HttpClientErrorException ex) {
			errorInfo = extractDetailsException(ex);			
		}
		
		assertEquals(errorInfo.code, ErrorMessageEnum.DUPLICATED_KEY_ERROR.code);
	}
	
	@Test
	public void signUpSocialInvalidExternalToken() {
		ErrorMessageInfo errorInfo = null;
		try {
		    RequestEntity<User> request = RequestEntity.post(getSignUpURI("facebook"))
		    							.header(X_AUTH_APP_KEY.value, appSettings.getKey())
		    							.header(X_AUTH_EXTERNAL_TOKEN.value, "123")
		    							.accept(MediaType.APPLICATION_JSON).body(new User());
		    
			new RestTemplate().exchange(request, User.class);
		} catch (final HttpClientErrorException ex) {
			errorInfo = extractDetailsException(ex);		
		}
		assertEquals(errorInfo.code, ErrorMessageEnum.NOT_AUTHORIZED_SOCIAL_NETWORK.code);
	}
	
//	@Test //TODO - implementar
	public void sslinkFacebookUserToInternalUser() {
		//add a simple user
		User user = user("dc", "dc", "dc", "123", ROLE_USER.value, "dc@gmail.com");
		
	    RequestEntity<User> request = RequestEntity.post(getSignUpURI(null))
	    							.header(X_AUTH_APP_KEY.value, appSettings.getKey())
	    							.accept(MediaType.APPLICATION_JSON).body(user);
	    
		ResponseEntity<User> response = new RestTemplate().exchange(request, User.class);
		User userAdded = response.getBody();
		assertNotNull(userAdded);
		
		String token = response.getHeaders().get(X_AUTH_TOKEN.value).get(0);
		
		String accessToken = "CAAL0ypjBVykBAO1uryb3fqu5n06BVEhv3yKt21SMefXGVfnAysZCBjSZCZBVJwuFXIHzqPFKZAsnZCG4EmNSH8hKMk4zCSjFZCbruZC5ZBqlVTp4zDdZACCC6uH5eG4vsXs5KDGeZAU3Js06DShAGwqxCr6qtPjfNc4fJRiS4m5B5OPTySSUu7JJOOx1wGu5jtAsJhQFPCiOADsHRTB1Y68LUr";
    	request = RequestEntity.post(URI.create(getBaseUserUrl()+"/linkUser/facebook"))
				.header(X_AUTH_APP_KEY.value, appSettings.getKey())
				.header(X_AUTH_EXTERNAL_TOKEN.value, accessToken)
				.header(X_AUTH_TOKEN.value, token)
				.accept(MediaType.APPLICATION_JSON).body(new User());
    	
		response = new RestTemplate().exchange(request, User.class);
		User facebookUserAdded = response.getBody();
		assertNotNull(facebookUserAdded);
	}
	
	public void unlinkFacebookUserToInternaluser() {
		
	}
}
