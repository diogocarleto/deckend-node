package io.deckend.controller;

import static io.deckend.node.exception.ErrorMessageEnum.DOCUMENT_NOT_FOUND;
import static io.deckend.node.exception.ErrorMessageEnum.PERMISSION_GRANTED_SUCCESSFULLY;
import static io.deckend.node.exception.ErrorMessageEnum.PERMISSION_REVOKED_SUCCESSFULLY;
import static io.deckend.node.infra.ACLPermissionsEnum.READ;
import static io.deckend.node.infra.ACLPermissionsEnum.WRITE;
import static io.deckend.node.utils.AuthEnum.X_AUTH_APP_KEY;
import static io.deckend.node.utils.AuthEnum.X_AUTH_TOKEN;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.net.URI;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.boot.test.WebIntegrationTest;
import org.springframework.http.MediaType;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import com.mongodb.BasicDBList;
import com.mongodb.DBObject;
import com.mongodb.util.JSON;

import io.deckend.node.Application;
import io.deckend.node.exception.ErrorMessageInfo;
import io.deckend.node.infra.ACLPermissionsEnum;


/**
 * @author Diogo Carleto - diogocarleto@gmail.com
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@WebIntegrationTest
public class DocumentControllerPermissionsTests extends DocumentControllerBaseTests {

	@Before
	public void before() {
		accessToken = authenticateRootAndGetToken();
		RequestEntity<?> request = RequestEntity.post(getBaseCollectionURI("/"+COLLECTION_NAME))
					.header(X_AUTH_APP_KEY.value, appSettings.getKey())
					.header(X_AUTH_TOKEN.value, accessToken)
					.accept(MediaType.APPLICATION_JSON)
					.build();
		new RestTemplate().exchange(request, String.class);
	}

	@After
	public void after() {
		RequestEntity<?> request = RequestEntity.delete(getBaseCollectionURI("/"+COLLECTION_NAME))
				.header(X_AUTH_APP_KEY.value, appSettings.getKey())
				.header(X_AUTH_TOKEN.value, accessToken)
				.accept(MediaType.APPLICATION_JSON)
				.build();
		new RestTemplate().exchange(request, String.class);
	}

	@Test
	public void updateDocumentWithoutPermissions() {
		ErrorMessageInfo errorMessageInfo = null;
		try {
			DBObject document = createDefaultDocument();
			assertNotNull(document);

			document.put("note", "updated");

			accessToken = authenticateDTest();
			updateDocument(accessToken, COLLECTION_NAME, document.toString());
		} catch(HttpClientErrorException ex){
			errorMessageInfo = extractDetailsException(ex);
		}

		assertEquals(errorMessageInfo.code, DOCUMENT_NOT_FOUND.code);
	}

	@Test
	public void updateDocumentFieldWithoutPermitions() {
		DBObject document = createDefaultDocument();
		assertNotNull(document);

		String jsonUpdate = "{ 'title': 'I am a title updated'}";

		accessToken = authenticateDTest();
		ResponseEntity<String> response = updatePartialDocument(accessToken, COLLECTION_NAME, (String) document.get("_id"), jsonUpdate);
		assertNull(response.getBody());
	}

	@Test
	public void findDocumentWithoutPermissions() {
		DBObject document = createDefaultDocument();
		assertNotNull(document);

		//authenticate Dtest and trying to find a document of another user
		accessToken = authenticateDTest();
		String query = "{'title': 'I am a title'}";
		ResponseEntity<String> response = findDocumentByQueryUtil(accessToken, query);
		BasicDBList dbObjects = (BasicDBList) JSON.parse(response.getBody());
		assertTrue(dbObjects.isEmpty());
	}

	@Test
	public void findDocumentByIdWithoutPermissions() {
		DBObject document = createDefaultDocument();
		assertNotNull(document);

		//authenticate Dtest and trying to find a document of another user
		accessToken = authenticateDTest();
		RequestEntity<?> request = RequestEntity.get(URI.create(getBaseDocument(COLLECTION_NAME)+"/find"+"/"+document.get("_id")))
				.header(X_AUTH_APP_KEY.value, appSettings.getKey())
				.header(X_AUTH_TOKEN.value, accessToken)
				.accept(MediaType.APPLICATION_JSON)
				.build();

		ResponseEntity<String> response = new RestTemplate().exchange(request, String.class);
		assertNull(response.getBody());
	}

	@Test
	public void grantForUser() {
		DBObject document = createDefaultDocument();
		assertNotNull(document);

		URI uriToGrantUser = getURIToGrantRevokeUser(COLLECTION_NAME, document.get("_id").toString(), READ, "dtest");
		RequestEntity<?> request = RequestEntity.put(uriToGrantUser)
				.header(X_AUTH_APP_KEY.value, appSettings.getKey())
				.header(X_AUTH_TOKEN.value, accessToken)
				.accept(MediaType.APPLICATION_JSON)
				.build();

		ErrorMessageInfo errorMessageInfo = new RestTemplate().exchange(request, ErrorMessageInfo.class).getBody();
		Assert.assertThat(errorMessageInfo.code, is(PERMISSION_GRANTED_SUCCESSFULLY.code));
	}

	@Test
	public void grantForRole() {
		DBObject document = createDefaultDocument();
		assertNotNull(document);

		URI uriToGrantRole = getURIToGrantRevokeRole(COLLECTION_NAME, document.get("_id").toString(), WRITE, "role_user");
		RequestEntity<?> request = RequestEntity.put(uriToGrantRole)
				.header(X_AUTH_APP_KEY.value, appSettings.getKey())
				.header(X_AUTH_TOKEN.value, accessToken)
				.accept(MediaType.APPLICATION_JSON)
				.build();

		ErrorMessageInfo errorMessageInfo = new RestTemplate().exchange(request, ErrorMessageInfo.class).getBody();
		Assert.assertThat(errorMessageInfo.code, is(PERMISSION_GRANTED_SUCCESSFULLY.code));
	}


	@Test
	public void grantForPublic() {
		DBObject document = createDefaultDocument();
		assertNotNull(document);

		URI uriToGrantPublic = getURIToGrantRevokePublic(COLLECTION_NAME, document.get("_id").toString(), WRITE);
		RequestEntity<?> request = RequestEntity.put(uriToGrantPublic)
				.header(X_AUTH_APP_KEY.value, appSettings.getKey())
				.header(X_AUTH_TOKEN.value, accessToken)
				.accept(MediaType.APPLICATION_JSON)
				.build();

		ErrorMessageInfo errorMessageInfo = new RestTemplate().exchange(request, ErrorMessageInfo.class).getBody();
		Assert.assertThat(errorMessageInfo.code, is(PERMISSION_GRANTED_SUCCESSFULLY.code));
	}

	@Test
	public void revokeForUser() {
		DBObject document = createDefaultDocument();
		assertNotNull(document);

		URI uriToRevokeUser = getURIToGrantRevokeUser(COLLECTION_NAME, document.get("_id").toString(), READ, "dtest");
		RequestEntity<?> request = RequestEntity.put(uriToRevokeUser)
				.header(X_AUTH_APP_KEY.value, appSettings.getKey())
				.header(X_AUTH_TOKEN.value, accessToken)
				.accept(MediaType.APPLICATION_JSON)
				.build();

		ErrorMessageInfo errorMessageInfo = new RestTemplate().exchange(request, ErrorMessageInfo.class).getBody();
		Assert.assertThat(errorMessageInfo.code, is(PERMISSION_GRANTED_SUCCESSFULLY.code));

		request = RequestEntity.delete(uriToRevokeUser)
				.header(X_AUTH_APP_KEY.value, appSettings.getKey())
				.header(X_AUTH_TOKEN.value, accessToken)
				.accept(MediaType.APPLICATION_JSON)
				.build();

		errorMessageInfo = new RestTemplate().exchange(request, ErrorMessageInfo.class).getBody();
		Assert.assertThat(errorMessageInfo.code, is(PERMISSION_REVOKED_SUCCESSFULLY.code));

	}

	@Test
	public void revokeForRole() {
		DBObject document = createDefaultDocument();
		assertNotNull(document);

		URI uriToRevokeRole = getURIToGrantRevokeRole(COLLECTION_NAME, document.get("_id").toString(), READ, "role_user");
		RequestEntity<?> request = RequestEntity.put(uriToRevokeRole)
				.header(X_AUTH_APP_KEY.value, appSettings.getKey())
				.header(X_AUTH_TOKEN.value, accessToken)
				.accept(MediaType.APPLICATION_JSON)
				.build();

		ErrorMessageInfo errorMessageInfo = new RestTemplate().exchange(request, ErrorMessageInfo.class).getBody();
		Assert.assertThat(errorMessageInfo.code, is(PERMISSION_GRANTED_SUCCESSFULLY.code));

		request = RequestEntity.delete(uriToRevokeRole)
				.header(X_AUTH_APP_KEY.value, appSettings.getKey())
				.header(X_AUTH_TOKEN.value, accessToken)
				.accept(MediaType.APPLICATION_JSON)
				.build();

		errorMessageInfo = new RestTemplate().exchange(request, ErrorMessageInfo.class).getBody();
		Assert.assertThat(errorMessageInfo.code, is(PERMISSION_REVOKED_SUCCESSFULLY.code));

	}

	@Test
	public void revokeForPublic() {
		DBObject document = createDefaultDocument();
		assertNotNull(document);

		URI uriToRevokePublic = getURIToGrantRevokePublic(COLLECTION_NAME, document.get("_id").toString(), READ);
		RequestEntity<?> request = RequestEntity.put(uriToRevokePublic)
				.header(X_AUTH_APP_KEY.value, appSettings.getKey())
				.header(X_AUTH_TOKEN.value, accessToken)
				.accept(MediaType.APPLICATION_JSON)
				.build();

		ErrorMessageInfo errorMessageInfo = new RestTemplate().exchange(request, ErrorMessageInfo.class).getBody();
		Assert.assertThat(errorMessageInfo.code, is(PERMISSION_GRANTED_SUCCESSFULLY.code));

		request = RequestEntity.delete(uriToRevokePublic)
				.header(X_AUTH_APP_KEY.value, appSettings.getKey())
				.header(X_AUTH_TOKEN.value, accessToken)
				.accept(MediaType.APPLICATION_JSON)
				.build();

		errorMessageInfo = new RestTemplate().exchange(request, ErrorMessageInfo.class).getBody();
		Assert.assertThat(errorMessageInfo.code, is(PERMISSION_REVOKED_SUCCESSFULLY.code));
	}

	@Test
	public void grantAndReadForUser() {
		grantForUser();
		String json = "{ 'title': 'I am a title', 'body': 'I am a body'}";
		DBObject document = (DBObject) JSON.parse(createDocument(COLLECTION_NAME, json).getBody());

		URI uriToGrantUser = getURIToGrantRevokeUser(COLLECTION_NAME, document.get("_id").toString(), READ, "dtest");
		RequestEntity<?> request = RequestEntity.put(uriToGrantUser)
				.header(X_AUTH_APP_KEY.value, appSettings.getKey())
				.header(X_AUTH_TOKEN.value, accessToken)
				.accept(MediaType.APPLICATION_JSON)
				.build();

		ErrorMessageInfo errorMessageInfo = new RestTemplate().exchange(request, ErrorMessageInfo.class).getBody();
		Assert.assertThat(errorMessageInfo.code, is(PERMISSION_GRANTED_SUCCESSFULLY.code));

		accessToken = authenticateDTest();
		ResponseEntity<String> response = findDocumentByIdUtil(accessToken, COLLECTION_NAME, (String) document.get("_id"));
		assertNotNull(response.getBody());
	}

	@Test
	public void grantAndReadForUserWithoutPermission() {
		grantForUser();
		DBObject document = createDefaultDocument();
		assertNotNull(document);

		URI uriToGrantUser = getURIToGrantRevokeUser(COLLECTION_NAME, document.get("_id").toString(), READ, "dtest1");
		RequestEntity<?> request = RequestEntity.put(uriToGrantUser)
				.header(X_AUTH_APP_KEY.value, appSettings.getKey())
				.header(X_AUTH_TOKEN.value, accessToken)
				.accept(MediaType.APPLICATION_JSON)
				.build();

		ErrorMessageInfo errorMessageInfo = new RestTemplate().exchange(request, ErrorMessageInfo.class).getBody();
		Assert.assertThat(errorMessageInfo.code, is(PERMISSION_GRANTED_SUCCESSFULLY.code));

		accessToken = authenticateDTest();
		ResponseEntity<String> response = findDocumentByIdUtil(accessToken, COLLECTION_NAME, (String) document.get("_id"));
		assertNull(response.getBody());
	}

	@Test
	public void grantAndUpdateWithWritePermissions(){
		grantForUser();
		DBObject document = createDefaultDocument();
		assertNotNull(document);

		URI uriToGrantUser = getURIToGrantRevokeUser(COLLECTION_NAME, document.get("_id").toString(), WRITE, "dtest");
		RequestEntity<?> request = RequestEntity.put(uriToGrantUser)
				.header(X_AUTH_APP_KEY.value, appSettings.getKey())
				.header(X_AUTH_TOKEN.value, accessToken)
				.accept(MediaType.APPLICATION_JSON)
				.build();

		ErrorMessageInfo errorMessageInfo = new RestTemplate().exchange(request, ErrorMessageInfo.class).getBody();
		Assert.assertThat(errorMessageInfo.code, is(PERMISSION_GRANTED_SUCCESSFULLY.code));

		accessToken = authenticateDTest();

		document.put("note", "updated");
		accessToken = authenticateDTest();
		updateDocument(accessToken, COLLECTION_NAME, document.toString());

		ResponseEntity<String> response = findDocumentByIdUtil(accessToken, COLLECTION_NAME, (String) document.get("_id"));
		DBObject documentSaved = (DBObject) JSON.parse(response.getBody());
		assertTrue(documentSaved.containsField("note"));
	}

	@Test
	public void grantAndUpdateWithReadPermissions() {
		ErrorMessageInfo errorMessageInfo = null;
		try {
			grantForUser();
			DBObject document = createDefaultDocument();
			assertNotNull(document);

			URI uriToGrantUser = getURIToGrantRevokeUser(COLLECTION_NAME, document.get("_id").toString(), READ, "dtest");
			RequestEntity<?> request = RequestEntity.put(uriToGrantUser)
					.header(X_AUTH_APP_KEY.value, appSettings.getKey())
					.header(X_AUTH_TOKEN.value, accessToken)
					.accept(MediaType.APPLICATION_JSON)
					.build();

			ErrorMessageInfo errorMessageInfo1 = new RestTemplate().exchange(request, ErrorMessageInfo.class).getBody();
			Assert.assertThat(errorMessageInfo1.code, is(PERMISSION_GRANTED_SUCCESSFULLY.code));

			accessToken = authenticateDTest();

			document.put("note", "updated");
			accessToken = authenticateDTest();
			updateDocument(accessToken, COLLECTION_NAME, document.toString());
		} catch(HttpClientErrorException ex){
			errorMessageInfo = extractDetailsException(ex);
		}

		assertEquals(errorMessageInfo.code, DOCUMENT_NOT_FOUND.code);
	}

	private DBObject createDefaultDocument() {
		String json = "{ 'title': 'I am a title', 'body': 'I am a body'}";
		return (DBObject) JSON.parse(createDocument(COLLECTION_NAME, json).getBody());
	}

	private URI getURIToGrantRevokeUser(String collection, String id, ACLPermissionsEnum aclPermission, String username) {
		return URI.create(getBaseUrl()+"/document/"+collection+"/"+id+"/"+aclPermission.aclValue+"/user/"+username);
	}

	private URI getURIToGrantRevokeRole(String collection, String id, ACLPermissionsEnum aclPermission, String role) {
		return URI.create(getBaseUrl()+"/document/"+collection+"/"+id+"/"+aclPermission.aclValue+"/role/"+role);
	}

	private URI getURIToGrantRevokePublic(String collection, String id, ACLPermissionsEnum aclPermission) {
		return URI.create(getBaseUrl()+"/document/"+collection+"/"+id+"/"+aclPermission.aclValue);
	}
}
