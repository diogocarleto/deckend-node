package io.deckend.controller;

import static io.deckend.node.exception.ErrorMessageEnum.COLLECTION_NOT_FOUND;
import static io.deckend.node.exception.ErrorMessageEnum.DOCUMENT_NOT_FOUND;
import static io.deckend.node.exception.ErrorMessageEnum.INVALID_ID;
import static io.deckend.node.exception.ErrorMessageEnum.INVALID_JSON;
import static io.deckend.node.exception.ErrorMessageEnum.OPTIMISTIC_LOCK_EXCEPTION;
import static io.deckend.node.exception.ErrorMessageEnum.VERSION_NOT_FOUND_IN_OBJECT;
import static io.deckend.node.utils.AuthEnum.X_AUTH_APP_KEY;
import static io.deckend.node.utils.AuthEnum.X_AUTH_TOKEN;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.springframework.http.HttpMethod.POST;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.boot.test.WebIntegrationTest;
import org.springframework.core.io.ClassPathResource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import com.mongodb.DBRef;
import com.mongodb.util.JSON;

import io.deckend.node.Application;
import io.deckend.node.exception.ErrorMessageEnum;
import io.deckend.node.exception.ErrorMessageInfo;
import io.deckend.node.infra.model.DBObjectUtils;

/**
 * @author Diogo Carleto - diogocarleto@gmail.com
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@WebIntegrationTest
public class DocumentControllerCRUDTests extends DocumentControllerBaseTests {

	@Before
	public void before() {
		accessToken = authenticateRootAndGetToken();
		createCollection(COLLECTION_NAME);
	}

	@After
	public void after() {
		dropCollection(COLLECTION_NAME);
	}

	@Test
	public void createDocument() {
		String json = "{ 'title': 'I am a title', 'body': 'I am a body'}";
		ResponseEntity<String> response = createDocument(COLLECTION_NAME, json);
		assertNotNull(response.getBody());
	}

	@Test
	public void createDocumentInexistentCollection(){
		ErrorMessageInfo errorMessageInfo = null;
		try {
			String json = "{ 'title': 'I am a title', 'body': 'I am a body'}";
			createDocument("notFound", json);
		} catch(HttpClientErrorException ex){
			errorMessageInfo = extractDetailsException(ex);
		}

		assertEquals(errorMessageInfo.code, COLLECTION_NOT_FOUND.code);
	}

	@Test
	public void createInvalidDocument(){
		ErrorMessageInfo errorMessageInfo = null;
		try {
			String json = "{{ 'title': 'I am a title', 'body': 'I am a body'}";
			createDocument(COLLECTION_NAME, json);
		} catch(HttpClientErrorException ex){
			errorMessageInfo = extractDetailsException(ex);
		}

		assertEquals(errorMessageInfo.code, INVALID_JSON.code);
	}

	@Test
	public void deleteDocument(){
		String jsonString = createDocument(COLLECTION_NAME, "{ 'title': 'I am a title', 'body': 'I am a body'}").getBody();
		DBObject dbOject = (DBObject) JSON.parse(jsonString);
		ErrorMessageInfo errorMessageInfo = deleteDocument(COLLECTION_NAME, dbOject.get("_id").toString()).getBody();
		assertEquals(errorMessageInfo.code, ErrorMessageEnum.DOCUMENT_DELETED_SUCCESSFULLY.code) ;
	}

	@Test
	public void deleteInexistentDocument(){
		ErrorMessageInfo errorMessageInfo = null;
		try {
			deleteDocument(COLLECTION_NAME, "55bd328fd3d23322ab84fecc").getBody();
		} catch(HttpClientErrorException ex){
			errorMessageInfo = extractDetailsException(ex);
		}

		assertEquals(errorMessageInfo.code, DOCUMENT_NOT_FOUND.code);
	}

	@Test
	public void updateDocumentAddedByUserRoleAdmin(){
		String json = "{ 'title': 'I am a title', 'body': 'I am a body'}";
		ResponseEntity<String> response = createDocument(COLLECTION_NAME, json);
		String documentSavedString = response.getBody();
		assertNotNull(documentSavedString);

		DBObject documentSaved = (DBObject) JSON.parse(documentSavedString);
		documentSaved.put("note", "updated");

		response = updateDocument(accessToken, COLLECTION_NAME, documentSaved.toString());
		documentSaved = (DBObject) JSON.parse(response.getBody());
		assertTrue(documentSaved.containsField("note"));
	}

	@Test
	public void updateDocumentAddedByUserRoleUser(){
		accessToken = authenticateDTest();
		String json = "{ 'title': 'I am a title', 'body': 'I am a body'}";
		ResponseEntity<String> response = createDocument(COLLECTION_NAME, json);
		String documentSavedString = response.getBody();
		assertNotNull(documentSavedString);

		DBObject documentSaved = (DBObject) JSON.parse(documentSavedString);
		documentSaved.put("note", "updated");

		response = updateDocument(accessToken, COLLECTION_NAME, documentSaved.toString());
		documentSaved = (DBObject) JSON.parse(response.getBody());
		assertTrue(documentSaved.containsField("note"));
	}

	@Test
	public void updateInexistentDocument() {
		ErrorMessageInfo errorMessageInfo = null;
		try {
			String json = "{ '_id' : '55c65e6ff6a8f0a34128', 'title': 'I am a title', 'body': 'I am a body'}";
			updateDocument(accessToken, COLLECTION_NAME, json);
		} catch(HttpClientErrorException ex){
			errorMessageInfo = extractDetailsException(ex);
		}

		assertEquals(errorMessageInfo.code, INVALID_ID.code);
	}

	@Test
	public void updateInexistentDocumentWithoutVersion() {
		ErrorMessageInfo errorMessageInfo = null;
		try {
			String json = "{ '_id' : '55c65e6ffbeac6a8f0a34128', 'title': 'I am a title', 'body': 'I am a body'}";
			updateDocument(accessToken, COLLECTION_NAME, json);
		} catch(HttpClientErrorException ex){
			errorMessageInfo = extractDetailsException(ex);
		}

		assertEquals(errorMessageInfo.code, VERSION_NOT_FOUND_IN_OBJECT.code);
	}

	@Test
	public void updateOldDocument() {
		ErrorMessageInfo errorMessageInfo = null;
		try {
			String json = "{ 'title': 'I am a title', 'body': 'I am a body'}";
			ResponseEntity<String> response = createDocument(COLLECTION_NAME, json);
			String documentSavedString = response.getBody();
			assertNotNull(documentSavedString);

			DBObject documentSaved = (DBObject) JSON.parse(documentSavedString);
			documentSaved.put("note", "updated");

			response = updateDocument(accessToken, COLLECTION_NAME, documentSaved.toString());
			documentSaved = (DBObject) JSON.parse(response.getBody());
			documentSaved.put("version", 0);
			updateDocument(accessToken, COLLECTION_NAME, documentSaved.toString());
		} catch(HttpClientErrorException ex){
			errorMessageInfo = extractDetailsException(ex);
		}

		assertEquals(errorMessageInfo.code, OPTIMISTIC_LOCK_EXCEPTION.code);
	}

	@Test
	public void updatePartialDocumentAddedByUserRoleAdmin(){
		String json = "{ 'title': 'I am a title', 'body': 'I am a body'}";
		ResponseEntity<String> response = createDocument(COLLECTION_NAME, json);
		String documentSavedString = response.getBody();
		assertNotNull(documentSavedString);

		DBObject documentSaved = (DBObject) JSON.parse(documentSavedString);
		String jsonUpdate = "{ 'title': 'I am a title updated'}";

		response = updatePartialDocument(accessToken, COLLECTION_NAME, (String) documentSaved.get("_id"), jsonUpdate);
		documentSaved = (DBObject) JSON.parse(response.getBody());
		assertEquals(documentSaved.get("title"), "I am a title updated");
	}

	@Test
	public void updatePartialDocumentAddedByUserRoleUser(){
		accessToken = authenticateDTest();

		String json = "{ 'title': 'I am a title', 'body': 'I am a body'}";
		ResponseEntity<String> response = createDocument(accessToken, COLLECTION_NAME, json);
		String documentSavedString = response.getBody();
		assertNotNull(documentSavedString);

		DBObject documentSaved = (DBObject) JSON.parse(documentSavedString);
		String jsonUpdate = "{ 'title': 'I am a title updated'}";

		response = updatePartialDocument(accessToken, COLLECTION_NAME, (String) documentSaved.get("_id"), jsonUpdate);
		documentSaved = (DBObject) JSON.parse(response.getBody());
		assertEquals(documentSaved.get("title"), "I am a title updated");
	}

	/**
	 * Update the field title and insert another field.
	 */
	@Test
	public void updatePartialDocumentAddedByUserRoleUser2(){
		accessToken = authenticateDTest();

		String json = "{ 'title': 'I am a title', 'body': 'I am a body'}";
		ResponseEntity<String> response = createDocument(accessToken, COLLECTION_NAME, json);
		String documentSavedString = response.getBody();
		assertNotNull(documentSavedString);

		DBObject documentSaved = (DBObject) JSON.parse(documentSavedString);
		String jsonUpdate = "{ 'title': 'I am a title updated', 'note': 'a note added'}";

		response = updatePartialDocument(accessToken, COLLECTION_NAME, (String) documentSaved.get("_id"), jsonUpdate);
		documentSaved = (DBObject) JSON.parse(response.getBody());
		assertEquals(documentSaved.get("title"), "I am a title updated");
		assertEquals(documentSaved.get("note"), "a note added");
	}

	@Test
	public void addDocumentWithRelationshipToFile() {
		String json = "{ 'title': 'I am a title', 'body': 'I am a body'}";
		ResponseEntity<String> response = createDocument(COLLECTION_NAME, json);
		DBObject document = (DBObject) JSON.parse(response.getBody());
		assertNotNull(document);

		String fileId = uploadFileUtil("application.properties");
        assertNotNull(fileId);

        document.put("fileId", fileId);

        document = (DBObject) JSON.parse(updateDocument(accessToken, COLLECTION_NAME, document.toString()).getBody());
        assertThat(document.containsField("fileId") , notNullValue());
        assertThat(document.get(DBObjectUtils.VERSION), is(1));
	}

	@Test
	public void addDocumentWithRelationshipToAnotherDocument() {
		String json = "{ 'title': 'I am a title', 'body': 'I am a body'}";
		ResponseEntity<String> response = createDocument(COLLECTION_NAME, json);
		DBObject document1 = (DBObject) JSON.parse(response.getBody());
		assertNotNull(document1);

		String COLLECTION_NAME2 = "mycollection2";
		createCollection(COLLECTION_NAME2);
		String json2 = "{ 'title': 'I am a title', 'body': 'I am a body mycollection2'}";
		response = createDocument(COLLECTION_NAME2, json2);
		DBObject document2 = (DBObject) JSON.parse(response.getBody());
		assertNotNull(document2);

		document1.put("dbRef", new DBRef(COLLECTION_NAME2, document2.get(DBObjectUtils.ID_FIELD)));

        document1 = (DBObject) JSON.parse(updateDocument(accessToken, COLLECTION_NAME, document1.toString()).getBody());
        assertThat(document1.containsField("dbRef") , notNullValue());
        assertThat(document1.get(DBObjectUtils.VERSION), is(1));

        BasicDBObject dbRef = (BasicDBObject) document1.get("dbRef");
        DBObject document2FromDb = (DBObject) JSON.parse(findDocumentByIdUtil(accessToken, dbRef.getString("ref"), dbRef.getString("id")).getBody());

        assertThat(document2FromDb, notNullValue());
        dropCollection(COLLECTION_NAME2);
	}

	private String uploadFileUtil(final String file) {
		HttpHeaders headers = new HttpHeaders();
		headers.add(X_AUTH_APP_KEY.value, appSettings.getKey());
		headers.add(X_AUTH_TOKEN.value, accessToken);
		headers.setContentType(MediaType.MULTIPART_FORM_DATA);


		MultiValueMap<String, Object> map = new LinkedMultiValueMap<String, Object>();
        map.add("file", new ClassPathResource(file));
        map.add("collectionName", COLLECTION_NAME);
        map.add("metadata", "{'data': 'test'}");

        HttpEntity<MultiValueMap<String, Object>> imageEntity = new HttpEntity<MultiValueMap<String, Object>>(map, headers);
        ResponseEntity<String> response = new RestTemplate().exchange(getBaseFileUrl(), POST, imageEntity, String.class);

        DBObject savedFile = (DBObject) JSON.parse(response.getBody());
        return (String) savedFile.get("_id");
	}

	private void createCollection(String collectionName) {
		RequestEntity<?> request = RequestEntity.post(getBaseCollectionURI("/"+collectionName))
				.header(X_AUTH_APP_KEY.value, appSettings.getKey())
				.header(X_AUTH_TOKEN.value, accessToken)
				.accept(MediaType.APPLICATION_JSON)
				.build();
		new RestTemplate().exchange(request, String.class);
	}

	private void dropCollection(String collectionName) {
		RequestEntity<?> request = RequestEntity.delete(getBaseCollectionURI("/"+collectionName))
				.header(X_AUTH_APP_KEY.value, appSettings.getKey())
				.header(X_AUTH_TOKEN.value, accessToken)
				.accept(MediaType.APPLICATION_JSON)
				.build();
		new RestTemplate().exchange(request, String.class);
	}
}
