 package io.deckend.controller;

import static io.deckend.node.exception.ErrorMessageEnum.COLLECTION_ALREADY_EXISTS;
import static io.deckend.node.exception.ErrorMessageEnum.COLLECTION_NOT_FOUND;
import static io.deckend.node.exception.ErrorMessageEnum.INDEXES_CREATED_SUCCESSFULLY;
import static io.deckend.node.exception.ErrorMessageEnum.INDEXES_DROPPED_SUCCESSFULLY;
import static io.deckend.node.exception.ErrorMessageEnum.INVALID_INDEXES;
import static io.deckend.node.utils.AuthEnum.X_AUTH_APP_KEY;
import static io.deckend.node.utils.AuthEnum.X_AUTH_TOKEN;
import static org.hamcrest.Matchers.greaterThan;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.boot.test.WebIntegrationTest;
import org.springframework.http.MediaType;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import com.mongodb.BasicDBList;
import com.mongodb.util.JSON;

import io.deckend.node.Application;
import io.deckend.node.exception.ErrorMessageInfo;

/**
 * @author Diogo Carleto - diogocarleto@gmail.com
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@WebIntegrationTest
public class CollectionControllerTests extends AbstractRestTests {

	private String accessToken;
	
	@Before
	public void setup() {
		accessToken = authenticateRootAndGetToken();
	}
	
	@Test
	public void createCollection() {
		ResponseEntity<String> response = createCollection(accessToken, "teste");
		assertNotNull(response.getBody());
	}
	
	@Test
	public void createCollectionAlreadyExists() {
		ErrorMessageInfo errorMessageInfo = null;
		try {
			ResponseEntity<String> response = createCollection(accessToken, "teste1");
			assertNotNull(response.getBody());
			createCollection(accessToken, "teste1");
		} catch(HttpClientErrorException ex) {
			errorMessageInfo = extractDetailsException(ex);
		}
		assertEquals(errorMessageInfo.code, COLLECTION_ALREADY_EXISTS.code);
	}	
	
	@Test
	public void dropCollection() {
		String collectionName = "testDelete";
		createCollection(accessToken, collectionName);
		ResponseEntity<String> response = dropCollection(accessToken, collectionName);
		assertNotNull(response.getBody());
	}
	
	@Test
	public void dropInexistentCollection() {
		ErrorMessageInfo errorMessageInfo = null;
		try {
			ResponseEntity<String> response = dropCollection(accessToken, "testDelete1");
			assertNotNull(response.getBody());
			dropCollection(accessToken, "teste1");
		} catch(HttpClientErrorException ex) {
			errorMessageInfo = extractDetailsException(ex);
		}
		assertEquals(errorMessageInfo.code, COLLECTION_NOT_FOUND.code);
		
	}	
	
	//TODO - see a way to get created index
	@Test
	public void createIndex() {
		String collectionName = "collectionToIndex";
		ResponseEntity<String> response = createCollection(accessToken, collectionName);
		assertNotNull(response.getBody());
		ResponseEntity<ErrorMessageInfo> responseCreateIndex = createIndex(accessToken, collectionName, "{ a: 1 }, { unique: true }");
		assertEquals(responseCreateIndex.getBody().code, INDEXES_CREATED_SUCCESSFULLY.code);
	}
	
	@Test
	public void createInvalidIndex() {
		ErrorMessageInfo errorMessageInfo = null;
		try {
			String collectionName = "collectionToIndex1";
			ResponseEntity<String> response = createCollection(accessToken, collectionName);
			assertNotNull(response.getBody());
			createIndex(accessToken, collectionName, "{ a: a : 1 }, { unique: true }");
		} catch(HttpClientErrorException ex) {
			errorMessageInfo = extractDetailsException(ex);
		}
		assertEquals(errorMessageInfo.code, INVALID_INDEXES.code);
	}
	
	@Test
	public void dropIndex() {
		String collectionName = "collectionToIndex2";
		ResponseEntity<String> response = createCollection(accessToken, collectionName);
		assertNotNull(response.getBody());
		createIndex(accessToken, collectionName, "{ a: 1 }, { unique: true }");
		//TODO - do an implementation that enable drop index using a json.
		ResponseEntity<ErrorMessageInfo> responseDropIndex = dropIndex(accessToken, collectionName, "a_1");
		assertEquals(responseDropIndex.getBody().code, INDEXES_DROPPED_SUCCESSFULLY.code);
	}
	
	@Test
	public void dropInvalidIndex() {
		ErrorMessageInfo errorMessageInfo = null;
		try {
			String collectionName = "collectionToIndex3";
			ResponseEntity<String> response = createCollection(accessToken, collectionName);
			assertNotNull(response.getBody());
			dropIndex(accessToken, collectionName, "a:a");
		} catch(HttpClientErrorException ex) {
			errorMessageInfo = extractDetailsException(ex);
		}
		assertEquals(errorMessageInfo.code, INVALID_INDEXES.code);
	}
	
	@Test
	public void getIndexes() {
		String collectionName = "collectionToIndex4";
		ResponseEntity<String> response = createCollection(accessToken, collectionName);
		assertNotNull(response.getBody());
		
		createIndex(accessToken, collectionName, "{ a: 1 }, { unique: true }");
		
		RequestEntity<?> request = RequestEntity.get(getBaseCollectionURI("/"+collectionName+"/index"))
				.header(X_AUTH_APP_KEY.value, appSettings.getKey())
				.header(X_AUTH_TOKEN.value, accessToken)
				.accept(MediaType.APPLICATION_JSON)
				.build();
		response = new RestTemplate().exchange(request, String.class);
		BasicDBList dbObjects = (BasicDBList) JSON.parse(response.getBody());
		Assert.assertThat(dbObjects.size(), greaterThan(0));
	}
	
	private ResponseEntity<String> createCollection(final String accessToken, final String collectionName) {
		RequestEntity<?> request = RequestEntity.post(getBaseCollectionURI("/"+collectionName))
				.header(X_AUTH_APP_KEY.value, appSettings.getKey())
				.header(X_AUTH_TOKEN.value, accessToken)
				.accept(MediaType.APPLICATION_JSON)
				.build();
		return new RestTemplate().exchange(request, String.class);
	}
	
	private ResponseEntity<ErrorMessageInfo> createIndex(final String accessToken, final String collectionName, final String jsonIndex) {
		RequestEntity<?> request = RequestEntity.post(getBaseCollectionURI("/"+collectionName+"/index"))
				.header(X_AUTH_APP_KEY.value, appSettings.getKey())
				.header(X_AUTH_TOKEN.value, accessToken)
				.accept(MediaType.APPLICATION_JSON)
				.body(jsonIndex);
		return new RestTemplate().exchange(request, ErrorMessageInfo.class);
	}
	
	private ResponseEntity<ErrorMessageInfo> dropIndex(final String accessToken, final String collectionName, final String index) {
		RequestEntity<?> request = RequestEntity.delete(getBaseCollectionURI("/"+collectionName+"/index/"+index))
				.header(X_AUTH_APP_KEY.value, appSettings.getKey())
				.header(X_AUTH_TOKEN.value, accessToken)
				.accept(MediaType.APPLICATION_JSON)
				.build();
		return new RestTemplate().exchange(request, ErrorMessageInfo.class);
	}	
	
	private ResponseEntity<String> dropCollection(final String accessToken, final String collectionName) {
		RequestEntity<?> request = RequestEntity.delete(getBaseCollectionURI("/"+collectionName))
				.header(X_AUTH_APP_KEY.value, appSettings.getKey())
				.header(X_AUTH_TOKEN.value, accessToken)
				.accept(MediaType.APPLICATION_JSON)
				.build();
		return new RestTemplate().exchange(request, String.class);
	}
}
