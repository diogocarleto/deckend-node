package io.deckend.filter;

import static io.deckend.node.model.Proxy.proxy;
import static io.deckend.node.utils.AuthEnum.X_AUTH_APP_KEY;
import static io.deckend.node.utils.AuthEnum.X_AUTH_TOKEN;
import static org.hamcrest.Matchers.is;

import java.net.URI;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.boot.test.WebIntegrationTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.web.client.RestTemplate;

import io.deckend.controller.AbstractRestTests;
import io.deckend.node.Application;
import io.deckend.node.exception.ErrorMessageInfo;
import io.deckend.node.model.Proxy;

/**
 *TODO - add more tests
 * @author Diogo Carleto - diogocarleto@gmail.com
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@WebIntegrationTest
public class ProxyFilterTests extends AbstractRestTests {

	private static final Logger logger = LoggerFactory.getLogger(ProxyFilterTests.class);

	private final String proxyName = "jsonplaceholder";
	private String accessToken;

	@Before
	public void before() {
		accessToken = authenticateRootAndGetToken();
		createProxy(accessToken, proxy(true, proxyName, "http://jsonplaceholder.typicode.com/posts"));
	}

	@After
	public void after() {
		deleteProxy(accessToken, proxyName);
	}

	@Test
	public void postWithoutBody() {
		RequestEntity<?> request = RequestEntity.post(getBaseProxiesURI(proxyName))
				.header(X_AUTH_APP_KEY.value, appSettings.getKey())
				.header(X_AUTH_TOKEN.value, accessToken)
				.accept(MediaType.APPLICATION_JSON)
				.build();
		ResponseEntity<String> response = new RestTemplate().exchange(request, String.class);
		logger.info(response.getBody());
		Assert.assertThat(response.getStatusCode(), is(HttpStatus.CREATED));
	}

	@Test
	public void postWithBody() {
		String json = "{ 'title': 'I am a title', 'body': 'I am a body'}";
		RequestEntity<?> request = RequestEntity.post(URI.create(getBaseUrl()+"/proxy/"+proxyName+"/uiaaaaa"))//getBaseProxiesURI(proxyName))
				.header(X_AUTH_APP_KEY.value, appSettings.getKey())
				.header(X_AUTH_TOKEN.value, accessToken)
				.accept(MediaType.APPLICATION_JSON)
				.body(json);
		ResponseEntity<String> response = new RestTemplate().exchange(request, String.class);
		logger.info(response.getBody());
		Assert.assertThat(response.getStatusCode(), is(HttpStatus.CREATED));
	}

	public void postWithBodyAndToken() {}
	public void postProxyNotFound() {}
	public void getWithBody() {}

	@Test
	public void getWithoutBody() {
		RequestEntity<?> request = RequestEntity.get(getBaseProxiesURI(proxyName))
				.header(X_AUTH_APP_KEY.value, appSettings.getKey())
				.header(X_AUTH_TOKEN.value, accessToken)
				.accept(MediaType.APPLICATION_JSON)
				.build();
		ResponseEntity<String> response = new RestTemplate().exchange(request, String.class);
		logger.info(response.getBody());
		Assert.assertThat(response.getStatusCode(), is(HttpStatus.OK));
	}

	public void getWithBodyAndToken() {}

	private URI getBaseProxiesURI(final String proxyName) {
		return URI.create(getBaseUrl()+"/proxies/"+proxyName);
	}

	private URI getBaseProxyURI() {
		return URI.create(getBaseUrl()+"/proxy");
	}

	private URI getProxyURIToDelete(final String name) {
		return URI.create(getBaseUrl()+"/proxy/"+name);
	}

	protected ResponseEntity<Proxy> createProxy(final String accessToken, final Proxy proxy) {
		RequestEntity<Proxy> request = RequestEntity.post(getBaseProxyURI())
				.header(X_AUTH_APP_KEY.value, appSettings.getKey())
				.header(X_AUTH_TOKEN.value, accessToken)
				.accept(MediaType.APPLICATION_JSON)
				.body(proxy);
		return new RestTemplate().exchange(request, Proxy.class);
	}

	protected ResponseEntity<ErrorMessageInfo> deleteProxy(final String accessToken, final String name) {
		RequestEntity<?> request = RequestEntity.delete(getProxyURIToDelete(name))
				.header(X_AUTH_APP_KEY.value, appSettings.getKey())
				.header(X_AUTH_TOKEN.value, accessToken)
				.accept(MediaType.APPLICATION_JSON)
				.build();
		return new RestTemplate().exchange(request, ErrorMessageInfo.class);
	}
}
