package io.deckend.model;


import static io.deckend.node.infra.RoleEnum.ROLE_ADMIN;
import static io.deckend.node.model.User.user;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

import org.junit.Test;

import io.deckend.node.model.User;

/**
 * @author Diogo Carleto - diogocarleto@gmail.com
 *
 */
public class UserTests {

	private static final String ID = "12"; 
	
	@Test
	public void shouldNotMergeUsername() {		
		User mockUserToUpdate = user("Diogo1", "Carleto1", "usernameUpdated", "123456", ROLE_ADMIN.value, "diogocarleto1@gmail.com");
		mockUserToUpdate.setId(ID);
				
		User mockSavedUser = user("Diogo", "Carleto", "diogocarleto", "123456", "ROLE_ADMIN1", "diogocarleto@gmail.com");
		mockSavedUser.setId(ID);
		
		User mixedUser = mockSavedUser.mergeToUpdate(mockUserToUpdate);		
		assertNotEquals(mixedUser.getUsername(), mockUserToUpdate.getUsername());
	}
	
	@Test
	public void testAdjustData() {
		final String ID = "12";
		
		User mockUserToUpdate = user("Diogo1", "Carleto1", "diogocarleto", "123456", ROLE_ADMIN.value, "diogocarleto1@gmail.com");
		mockUserToUpdate.setId("11");
				
		User mockSavedUser = user("Diogo", "Carleto", "diogocarleto", "123456", "ROLE_ADMIN1", "diogocarleto@gmail.com");
		mockSavedUser.setId(ID);
		
		User mixedUser = mockSavedUser.mergeToUpdate(mockUserToUpdate);
		assertEquals(mixedUser.getRole(), ROLE_ADMIN.value);
		assertEquals(mixedUser.getId(), ID);
	}
	
	@Test
	public void testMantainsVersionFromUserToUpdate() {
		User mockUserToUpdate = user("Diogo1", "Carleto1", "diogocarleto", "123456", "ROLE_ADMIN1", "diogocarleto1@gmail.com");
		mockUserToUpdate.setId("11");
		mockUserToUpdate.setVersion(0l);
		
		User mockSavedUser = user("Diogo", "Carleto", "diogocarleto", "123456", "ROLE_ADMIN", "diogocarleto@gmail.com");
		mockSavedUser.setId("12");
		mockSavedUser.setVersion(1l);
		
		User mixedUser = mockSavedUser.mergeToUpdate(mockUserToUpdate);
		assertEquals(mixedUser.getVersion().longValue(), 0l);
	}	
}
